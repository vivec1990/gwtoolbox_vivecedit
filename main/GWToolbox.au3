#NoTrayIcon
#RequireAdmin

; Program icon and compiler instructions
#AutoIt3Wrapper_Icon=img/Monster_skill.ico
#AutoIt3Wrapper_Outfile=GWToolbox.exe
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Run_Obfuscator=n

; Add resource images
#AutoIt3Wrapper_Res_File_Add=img/title.jpg, rt_rcdata, TITLE_JPG
#AutoIt3Wrapper_Res_File_Add=img/balthspirit.jpg, rt_rcdata, BALTHSPIRIT_JPG
#AutoIt3Wrapper_Res_File_Add=img/lifebond.jpg, rt_rcdata, LIFEBOND_JPG
#AutoIt3Wrapper_Res_File_Add=img/protbond.jpg, rt_rcdata, PROTBOND_JPG

; Include autoit stuff
#include <Constants.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <ComboConstants.au3>
#include <Crypt.au3>
#include <GUITab.au3>
#include <Math.au3>
#include <EditConstants.au3>
#include <GuiComboBox.au3>
#include <Misc.au3>

; Include external Libraries
#include <lib/ColorPicker.au3>
#include <lib/ResourcesEx.au3>

; Include GWA2 and other GW stuff
#include <lib/Strings.au3>
#include <lib/GWA2.au3>
#include <lib/GwConstants.au3>
#include <lib/GwFuncs.au3>


; Set some global options
Opt("GUIOnEventMode", True)
Opt("TrayMenuMode", True)
Opt("MustDeclareVars", True)
Opt("GUICloseOnESC", False)

; Current Version
Global Const $currentVersion = "2.15"
Global Const $do_update = True

If Not FileExists($DataFolder) Then DirCreate($DataFolder)
If Not FileExists($keysIniFullPath) Then FileInstall(".\keys.ini", $keysIniFullPath)

; license and disclaimer if first run (ini file is not there)
Global Const $s_disclaimer = "DISCLAIMER:"&@CRLF& _
	'THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF ' & _
	'FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY ' & _
	'DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.' & @CRLF&@CRLF & _
	'By clicking the OK button you agree with all the above.'
If (Not FileExists($iniFileName) And Not FileExists($iniFullPath)) Then
	If MyGuiMsgBox(1, $GWToolbox, $s_disclaimer, 0, 400, 230) <> 1 Then exitProgram()
EndIf

; Initialize GW client
MyInitialize()

; Get window handle and PID
Global Const $gwHWND = GetWindowHandle()
Global Const $gwPID = WinGetProcess($gwHWND)

Global $timerGui, $timerGuiLabel
Global $healthGui, $healthGuiLabelHP, $healthGuiLabelMaxHP, $healthGuiLabelPerc, $healthGuiCurrentColor
Global $distanceGui, $distanceGuiLabelDist, $distanceGuiLabelPerc, $distanceGuiLabelText, $distanceGuiCurrentColor
Global $partyGui, $partyLabels[9]
Global $buffsGui, $buffsImages[9][3], $buffsStatus[9][3], $buffsImagesHidden[9][3], $buffsID[9][3]
Global Enum $BUFFS_VISIBLE, $BUFFS_HIDDEN, $BUFFS_UNKNOWN

Global $hDownload
If $do_update Then
	$hDownload = InetGet($Host&"GWToolbox_currentVersion.txt", $tmpFullPath, 1, 1)
Else
	$hDownload = 0
EndIf

#region colors
Global Const $COLOR_GREY = 0x222222
Global Const $COLOR_TIMER_DEFAULT = 0x20FF20

Global Const $COLOR_URGOZ_OPENING = 0xFF7575
Global Const $COLOR_URGOZ_OPEN = 0x20D020
Global Const $COLOR_URGOZ_CLOSING = 0x007500
Global Const $COLOR_URGOZ_CLOSED = 0xD02020

Global $COLOR_TIMER = IniRead($iniFullPath, $s_timer, $s_color, $COLOR_TIMER_DEFAULT)

Global $COLOR_HEALTH_HIGHT = IniRead($iniFullPath, $s_health, $s_color&"1", $COLOR_TIMER_DEFAULT)
Global $COLOR_HEALTH_MIDDLE = IniRead($iniFullPath, $s_health, $s_color&"2", $COLOR_YELLOW)
Global $COLOR_HEALTH_LOW = IniRead($iniFullPath, $s_health, $s_color&"3", $COLOR_RED)

Global $COLOR_PARTY = IniRead($iniFullPath, $s_party, $s_color, $COLOR_TIMER_DEFAULT)

Global $COLOR_DISTANCE = IniRead($iniFullPath, $s_distance, $s_color, $COLOR_TIMER_DEFAULT)
#endregion

;~ Remove default windows theme, so that custom colors can be used in the GUI
DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", 0)

#region READ INI FILE
Global $sfmacro = 	(IniRead($iniFullPath, $s_sfmacro, 		"active", False) == "True")
Global $stuck = 	(IniRead($iniFullPath, $s_stuck, 		"active", False) == "True")
Global $recall = 	(IniRead($iniFullPath, $s_recall, 		"active", False) == "True")
Global $ua = 		(IniRead($iniFullPath, $s_ua, 			"active", False) == "True")
Global $hidegw = 	(IniRead($iniFullPath, $s_hidegw, 		"active", False) == "True")
Global $resign = 	(IniRead($iniFullPath, $s_resign, 		"active", False) == "True")
Global $teamResign =(IniRead($iniFullPath, $s_teamresign,	"active", False) == "True")
Global $clicker = 	(IniRead($iniFullPath, $s_clicker, 		"active", False) == "True")
Global $res = 		(IniRead($iniFullPath, $s_res, 			"active", False) == "True")
Global $age = 		(IniRead($iniFullPath, $s_age, 			"active", False) == "True")
Global $agepm = 	(IniRead($iniFullPath, $s_agepm, 		"active", False) == "True")
Global $pstone = 	(IniRead($iniFullPath, $s_pstone, 		"active", False) == "True")
Global $focus = 	(IniRead($iniFullPath, $s_focus,		"active", False) == "True")
Global $looter = 	(IniRead($iniFullPath, $s_looter,		"active", False) == "True")
Global $identifier =(IniRead($iniFullPath, $s_identifier,	"active", False) == "True")
Global $ghostpop = 	(IniRead($iniFullPath, $s_ghostpop,		"active", False) == "True")
Global $ghosttarget=(IniRead($iniFullPath, $s_ghosttarget, 	"active", False) == "True")
Global $gstonepop = (IniRead($iniFullPath, $s_gstonepop, 	"active", False) == "True")
Global $legiopop = 	(IniRead($iniFullPath, $s_legiopop, 	"active", False) == "True")
Global $rainbowuse =(IniRead($iniFullPath, $s_rainbowuse, 	"active", False) == "True")

Global $pcons = 	(IniRead($iniFullPath, $s_pcons, 		"active", False) == "True")
Global $timer = 	(IniRead($iniFullPath, $s_timer, 		"active", False) == "True")
Global $health = 	(IniRead($iniFullPath, $s_health, 		"active", False) == "True")
Global $party = 	(IniRead($iniFullPath, $s_party, 		"active", False) == "True")
Global $buffs = 	(IniRead($iniFullPath, $s_buffs, 		"active", False) == "True")
Global $buffsDrop = (IniRead($iniFullPath, "buffsdrop", 	"active", False) == "True")
Global $distance = 	(IniRead($iniFullPath, $s_distance, 	"active", False) == "True")
Global $pconsHotkey=(IniRead($iniFullPath, $s_pcons, 	"hkActive", False) == "True")

Global $DialogHK1 =	(IniRead($iniFullPath, "DialogHK"&"1","active", False) == "True")
Global $DialogHK2 =	(IniRead($iniFullPath, "DialogHK"&"2","active", False) == "True")
Global $DialogHK3 =	(IniRead($iniFullPath, "DialogHK"&"3","active", False) == "True")
Global $DialogHK4 =	(IniRead($iniFullPath, "DialogHK"&"4","active", False) == "True")

Global $pconsConsActive =		(IniRead($iniFullPath, $s_pcons, $s_cons,		False) == "True")
Global $pconsEssenceActive =	(IniRead($iniFullPath, $s_pcons, "essence", 	False) == "True")
Global $pconsAlcoholActive = 	(IniRead($iniFullPath, $s_pcons, $s_alcohol, 	False) == "True")
Global $pconsRRCActive = 		(IniRead($iniFullPath, $s_pcons, $s_RRC, 		False) == "True")
Global $pconsBRCActive = 		(IniRead($iniFullPath, $s_pcons, $s_BRC, 		False) == "True")
Global $pconsGRCActive = 		(IniRead($iniFullPath, $s_pcons, "GRC", 		False) == "True")
Global $pconsPieActive = 		(IniRead($iniFullPath, $s_pcons, "pie", 		False) == "True")
Global $pconsCupcakeActive = 	(IniRead($iniFullPath, $s_pcons, "cupcake", 	False) == "True")
Global $pconsAppleActive = 		(IniRead($iniFullPath, $s_pcons, "apple", 		False) == "True")
Global $pconsCornActive = 		(IniRead($iniFullPath, $s_pcons, "corn", 		False) == "True")
Global $pconsEggActive = 		(IniRead($iniFullPath, $s_pcons, "egg", 		False) == "True")
Global $pconsKabobActive = 		(IniRead($iniFullPath, $s_pcons, "kabob", 		False) == "True")
Global $pconsWarSupplyActive =  (IniRead($iniFullPath, $s_pcons, "warsupply",	False) == "True")
Global $pconsLunarsActive = 	(IniRead($iniFullPath, $s_pcons, "lunars", 		False) == "True")
Global $pconsSkaleSoupActive = 	(IniRead($iniFullPath, $s_pcons, "skalesoup",	False) == "True")
Global $pconsMobstoppersActive =(IniRead($iniFullPath, $s_pcons, "mobstoppers",	False) == "True")
Global $pconsCityActive = 		(IniRead($iniFullPath, $s_pcons, "city",		False) == "True")
Global $pconsMoraleActive = 	(IniRead($iniFullPath, $s_pcons, "morale", 		False) == "True")

Global $stuckHotkey = 			IniRead($iniFullPath, $s_stuck, 		"hotkey", "00")
Global $recallHotkey = 			IniRead($iniFullPath, $s_recall, 		"hotkey", "00")
Global $uaHotkey = 				IniRead($iniFullPath, $s_ua, 			"hotkey", "00")
Global $hidegwHotkey = 			IniRead($iniFullPath, $s_hidegw, 		"hotkey", "00")
Global $clickerHotkey = 		IniRead($iniFullPath, $s_clicker, 		"hotkey", "00")
Global $resignHotkey = 			IniRead($iniFullPath, $s_resign, 		"hotkey", "00")
Global $teamResignHotkey = 		IniRead($iniFullPath, $s_teamresign, 	"hotkey", "00")
Global $resHotkey = 			IniRead($iniFullPath, $s_res, 			"hotkey", "00")
Global $ageHotkey = 			IniRead($iniFullPath, $s_age, 			"hotkey", "00")
Global $agepmHotkey = 			IniRead($iniFullPath, $s_agepm, 		"hotkey", "00")
Global $pstoneHotkey = 			IniRead($iniFullPath, $s_pstone, 		"hotkey", "00")
Global $focusHotkey = 			IniRead($iniFullPath, $s_focus, 		"hotkey", "00")
Global $looterHotkey = 			IniRead($iniFullPath, $s_looter, 		"hotkey", "00")
Global $identifierHotkey = 		IniRead($iniFullPath, $s_identifier, 	"hotkey", "00")
Global $pconsHotkeyHotkey = 	IniRead($iniFullPath, $s_pcons, 		"hotkey", "00")
Global $ghostpopHotkey = 		IniRead($iniFullPath, $s_ghostpop, 		"hotkey", "00")
Global $ghosttargetHotkey = 	IniRead($iniFullPath, $s_ghosttarget, 	"hotkey", "00")
Global $gstonepopHotkey = 		IniRead($iniFullPath, $s_gstonepop, 	"hotkey", "00")
Global $legiopopHotkey =		IniRead($iniFullPath, $s_legiopop, 		"hotkey", "00")
Global $rainbowuseHotkey = 		IniRead($iniFullPath, $s_rainbowuse, 	"hotkey", "00")

Global $DialogHK1Hotkey = IniRead($iniFullPath, "DialogHK"&"1", "hotkey", "00")
Global $DialogHK2Hotkey = IniRead($iniFullPath, "DialogHK"&"2", "hotkey", "00")
Global $DialogHK3Hotkey = IniRead($iniFullPath, "DialogHK"&"3", "hotkey", "00")
Global $DialogHK4Hotkey = IniRead($iniFullPath, "DialogHK"&"4", "hotkey", "00")

Global $DialogHK1Number = IniRead($iniFullPath, "DialogHK"&"1", "number", 0)
Global $DialogHK2Number = IniRead($iniFullPath, "DialogHK"&"2", "number", 0)
Global $DialogHK3Number = IniRead($iniFullPath, "DialogHK"&"3", "number", 0)
Global $DialogHK4Number = IniRead($iniFullPath, "DialogHK"&"4", "number", 0)

; GWToolbox also contains a very simple rupt bot, for rupting 1 skill, with 1 skill
; There is no interface, it must be configured in the .ini manually
Global $rupt = 		(IniRead($iniFullPath, $s_rupt, 		"active", False) == "True")
Global $ruptHotkey = IniRead($iniFullPath, $s_rupt, "hotkey", "00")
Global $ruptSkill = IniRead($iniFullPath, $s_rupt, "skill", 0)
Global $ruptSlot = IniRead($iniFullPath, $s_rupt, "slot", 0)
Global $ruptActive = False

; GWToolbox also contains a very simple movement tool, for moving the character at exactly the given coordinates
; There is no interface, it must be configured in the .ini manually
Global $movement 	   = (IniRead($iniFullPath, $s_movement, "active", False) == "True")
Global $movementHotkey = IniRead($iniFullPath, $s_movement, "hotkey", "00")
Global $movementXcoord = IniRead($iniFullPath, $s_movement, "x", 0)
Global $movementYcoord = IniRead($iniFullPath, $s_movement, "y", 0)

Global $Transparency = IniRead($iniFullPath, "display", "guitransparency", 200)
#endregion Inifile

#region GUI
#region mainGui
Global Const $GuiWidth = 400
Global Const $GuiHeight = 300
Global Const $dummyGui = GUICreate("")
Global $guiX = IniRead($iniFullPath, "display", "x", -1)
Global $guiY = IniRead($iniFullPath, "display", "y", -1)
If $guiX <> -1 And $guiX+$GuiWidth > @DesktopWidth Then $guiX = @DesktopWidth-$GuiWidth
If $guiX <> -1 And $guiX < 0 Then $guiX = 0
If $guiY <> -1 And $guiY+$GuiHeight > @DesktopHeight Then $guiY = @DesktopHeight-$GuiHeight
If $guiY <> -1 And $guiY < 0 Then $guiY = 0
Global Const $mainGui = GUICreate($GWToolbox, $GuiWidth, $GuiHeight, $guiX, $guiY, $WS_POPUP)
	_GuiRoundCorners($mainGui, 10)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)
	GUISetFont(10)
	GUISetOnEvent($GUI_EVENT_CLOSE, "exitProgram")
	GuiCtrlCreateRect(0, 23, $GuiWidth, 1)
Global Const $hTab = GUICtrlCreateTab(-100, -100, 1, 1)
	GUICtrlSetState(-1, $GUI_DISABLE)

Global Const $DummyInactive = GUICtrlCreateDummy()
Global Const $DummyLeft = GUICtrlCreateDummy()
	GUICtrlSetOnEvent($DummyLeft, "guiEventHandler")
Global Const $DummyRight = GUICtrlCreateDummy()
	GUICtrlSetOnEvent($DummyRight, "guiEventHandler")
Global $AccelKeys[2][2] = [["{LEFT}", $DummyLeft], ["{RIGHT}", $DummyRight]]
GUISetAccelerators($AccelKeys)

Global $tabButtons[7]
Global $tabX = 0
Global $tabWidth = 50
$tabButtons[0] = GUICtrlCreateLabel("General", 		$tabX, 				0, $tabWidth, 	23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
GuiCtrlCreateRect(									$tabX+$tabWidth,  	0, 1, 			23)
$tabX = $tabX+$tabWidth+1
$tabWidth = 58
$tabButtons[1] = GUICtrlCreateLabel("Hotkey1", 		$tabX,  			0, $tabWidth, 	23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
GuiCtrlCreateRect(									$tabX+$tabWidth, 	0, 1, 			23)
$tabX = $tabX+$tabWidth+1
$tabWidth = 58
$tabButtons[2] = GUICtrlCreateLabel("Hotkey2", 		$tabX,  			0, $tabWidth, 	23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
GuiCtrlCreateRect(									$tabX+$tabWidth, 	0, 1, 			23)
$tabX = $tabX+$tabWidth+1
$tabWidth = 38
$tabButtons[3] = GUICtrlCreateLabel("Cons", 		$tabX, 				0, $tabWidth, 	23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
GuiCtrlCreateRect(									$tabX+$tabWidth, 	0, 1, 			23)
$tabX = $tabX+$tabWidth+1
$tabWidth = 48
$tabButtons[4] = GUICtrlCreateLabel("Builds", 		$tabX, 				0, $tabWidth, 	23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
GuiCtrlCreateRect(									$tabX+$tabWidth, 	0, 1, 			23)
$tabX = $tabX+$tabWidth+1
$tabWidth = 52
$tabButtons[5] = GUICtrlCreateLabel("Dialogs", 		$tabX, 				0, $tabWidth, 	23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
GuiCtrlCreateRect(									$tabX+$tabWidth, 	0, 1, 			23)
$tabX = $tabX+$tabWidth+1
$tabWidth = 30
$tabButtons[6] = GUICtrlCreateLabel("Info", 		$tabX, 				0, $tabWidth, 	23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
GuiCtrlCreateRect(									$tabX+$tabWidth, 	0, 1, 			23)
GuiCtrlCreateRect(360, 0, 1, 23)
GuiCtrlCreateRect(380, 0, 1, 23)
Global Const $tabInfo = 6
GUICtrlCreateLabel("-", 341, 0, 19, 23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
	GUICtrlSetOnEvent(-1, "minimizeProgram")
Global Const $dragButton = GUICtrlCreateLabel("o", 361, 0, 19, 23, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
	GUICtrlSetTip(-1, "Drag to move")
	GUICtrlSetOnEvent(-1, "guiEventHandler")
GUICtrlCreateLabel("x", 381, 0, 19, 23, BitOR($SS_CENTER, $SS_CENTERIMAGE))
	GUICtrlSetOnEvent(-1, "exitProgram")
For $i=0 To UBound($tabButtons)-1
	GUICtrlSetOnEvent($tabButtons[$i], "tabClickHandler")
Next
GUICtrlSetBkColor($tabButtons[0], $COLOR_GREY)
#endregion
#region TabGeneral
GUICtrlCreateTabItem("General")
	Global Const $mainLabel = GUICtrlCreatePic("", 0, 25, $GuiWidth, 70, Default, $GUI_WS_EX_PARENTDRAG)
	If @Compiled Then
		_Resource_SetToCtrlID($mainLabel, "TITLE_JPG")
	Else
		GUICtrlSetImage($mainLabel, "img/title.jpg")
	EndIf

	GUICtrlCreateLabel("Version "&$currentVersion, 20, 70)
		GUICtrlSetColor(-1, 0xAAAAAA)

	Global Const $travelX = 5
	Global Const $travelY = 90
	Global Const $travelTo = 		GUICtrlCreateGroup("Fast Travel To", $travelX, $travelY, 190, 105)
	Global Const $travelToA = 		MyGuiCtrlCreateButton("ToA", 		$travelX+8, 	$travelY+20,  58, 25)
	Global Const $travelDoA = 		MyGuiCtrlCreateButton("DoA", 		$travelX+66, 	$travelY+20,  58, 25)
	Global Const $travelEmbark = 	MyGuiCtrlCreateButton("Embark", 	$travelX+8, 	$travelY+45,  58, 25)
	Global Const $travelKamadan = 	MyGuiCtrlCreateButton("Kamadan", 	$travelX+66, 	$travelY+45,  58, 25)
	Global Const $travelVlox = 		MyGuiCtrlCreateButton("Vlox's", 	$travelX+124, 	$travelY+20,  58, 25)
	Global Const $travelOther = 	MyGuiCtrlCreateButton("Other...", 		$travelX+124, 	$travelY+45,  58, 25)
	Global Const $travelDistrict = 	GUICtrlCreateCombo("Current District", $travelX+6, 	$travelY+76, 179, 25, $CBS_DROPDOWNLIST)
		GUICtrlSetData(-1, "International|American|American District 1|Europe English|Europe French|Europe German|Europe Italian|Europe Spanish|Europe Polish|Europe Russian|Asian Korean|Asia Chinese|Asia Japanese")
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetOnEvent($travelToA, "fastTravel")
		GUICtrlSetOnEvent($travelDoA, "fastTravel")
		GUICtrlSetOnEvent($travelEmbark, "fastTravel")
		GUICtrlSetOnEvent($travelKamadan, "fastTravel")
		GUICtrlSetOnEvent($travelVlox, "fastTravel")
		GUICtrlSetOnEvent($travelOther, "fastTravel")

	Global $sfmacroCheckBoxes[8]
	Global $sfmacroSkillsToUse[8]
	For $i = 0 To 7 Step 1
		$sfmacroSkillsToUse[$i] = (IniRead($iniFullPath, $s_sfmacro, $i + 1, False) == "True")
	Next

	GUICtrlCreateGroup("Skills Clicker", 205, 90, 190, 105)
	Global Const $sfmacroActive = GUICtrlCreateCheckbox("Active", 215, 110, 60, 17)
	Global Const $sfmacroEmoMode = GUICtrlCreateCheckbox("E/mo", 300, 110, 60, 17)
	MyGuiCtrlCreateButton("?", 379, 105, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will just use selected skills on recharge"&@CRLF&"Note: priority left to right"&@CRLF&@CRLF&"If E/mo is also enabled it will cast ER, spirit bond and burning speed."&@CRLF&"E/mo mode does NOT check for skill numbers below.")
	GUICtrlCreateLabel("Skills to use:", 215, 130, 80, 17)
	$sfmacroCheckBoxes[0] = GUICtrlCreateCheckbox("1", 215, 150, 25, 17)
	$sfmacroCheckBoxes[1] = GUICtrlCreateCheckbox("2", 260, 150, 25, 17)
	$sfmacroCheckBoxes[2] = GUICtrlCreateCheckbox("3", 305, 150, 25, 17)
	$sfmacroCheckBoxes[3] = GUICtrlCreateCheckbox("4", 350, 150, 25, 17)
	$sfmacroCheckBoxes[4] = GUICtrlCreateCheckbox("5", 215, 170, 25, 17)
	$sfmacroCheckBoxes[5] = GUICtrlCreateCheckbox("6", 260, 170, 25, 17)
	$sfmacroCheckBoxes[6] = GUICtrlCreateCheckbox("7", 305, 170, 25, 17)
	$sfmacroCheckBoxes[7] = GUICtrlCreateCheckbox("8", 350, 170, 25, 17)
		GUICtrlSetOnEvent($sfmacroActive, "toggleActive")
	If $sfmacro Then GUICtrlSetState($sfmacroActive, $GUI_CHECKED)
	For $i = 0 To 7 Step 1
		If $sfmacroSkillsToUse[$i] == True Then
			GUICtrlSetState($sfmacroCheckBoxes[$i], $GUI_CHECKED)
		EndIf
		GUICtrlSetOnEvent($sfmacroCheckBoxes[$i], "sfmacroToggleSkill")
	Next

	GUICtrlCreateGroup("Display", 5, 195, 190, 95)
		Global $IsOnTop = False
		Global Const $OnTopCheckbox = GUICtrlCreateCheckbox("Always on top", 15, 215)
			If (IniRead($iniFullPath, "display", "ontop", False) == "True") Then
				WinSetOnTop($mainGui, "", 1)
				GUICtrlSetState($OnTopCheckbox, $GUI_CHECKED)
				$IsOnTop = True
			EndIf
			GUICtrlSetOnEvent(-1, "guiEventHandler")
		WinSetTrans($mainGui, "", $Transparency)
		GUICtrlCreateLabel("Transparency:", 15, 240)
		Global Const $TransparencySlider = GUICtrlCreateSlider(15, 260, 170, 23)
			GUICtrlSetLimit(-1, 255, 40)
			GUICtrlSetData(-1, $Transparency)
			GUICtrlSetBkColor(-1, $COLOR_BLACK)
			GUICtrlSetOnEvent(-1, "guiEventHandler")

	MyGuiCtrlCreateButton("Materials Buyer...", 205, 205, 170, 25)
		GUICtrlSetOnEvent(-1, "MaterialsBuyerGui")
			MyGuiCtrlCreateButton("?", 380, 205, 10, 25)
				GUICtrlSetOnEvent(-1, "helpMaterialsBuyer")
				GUICtrlSetFont(-1, 9)
				GUICtrlSetTip(-1, "Click for help")


	GUICtrlCreateGroup("Max Zoom", 205, 240, 190, 50)
		MyGuiCtrlCreateButton("?", 379, 255, 10, 13)
			GUICtrlSetFont(-1, 9)
			GUICtrlSetTip(-1, "It will increase max camera zoom. Just move the slider and zoom out.")
		Global Const $zoomSlider = GUICtrlCreateSlider(210, 260, 165, 23)
			GUICtrlSetBkColor(-1, $COLOR_BLACK)
			GUICtrlSetLimit(-1, 20000, 750)
			GUICtrlSetOnEvent(-1, "guiEventHandler")
#endregion
#region TabHotkeys1
GUICtrlCreateTabItem("Hotkeys")
	Global $groupWidth = 80
	Global $groupHeight = 85
	Global $rowY = 25
	Global $rowX = 10
	Global $nameY = $rowY
	Global $activeY = $rowY+20
	Global $hotkeyY = $rowY+40
	Global $inputY = $rowY+55
	GUICtrlCreateGroup("/stuck", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will send /stuck to Guild Wars. Will also pm you /stuck in order to see that it really was used.")
	Global Const $stuckActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
	Global Const $stuckLabel = GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
	Global Const $stuckInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($stuckActive, "toggleActive")
		GUICtrlSetOnEvent($stuckInput, "setHotkey")
		If $stuck Then GUICtrlSetState($stuckActive, $GUI_CHECKED)
		GUICtrlSetData($stuckInput, IniRead($keysIniFullPath, "idToKey", $stuckHotkey, ""))
		GUICtrlSetFont($stuckActive, 9.5)
		GUICtrlSetFont($stuckLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup($s_recall, $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "If Recall is currently active on self, it will be cancelled, otherwise it will be cast on the current target.")
	Global Const $recallActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
	Global Const $recallLabel = GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
	Global Const $recallInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($recallActive, "toggleActive")
		GUICtrlSetOnEvent($recallInput, "setHotkey")
		If $recall Then GUICtrlSetState($recallActive, $GUI_CHECKED)
		GUICtrlSetData($recallInput, IniRead($keysIniFullPath, "idToKey", $recallHotkey, ""))
		GUICtrlSetFont($recallActive, 9.5)
		GUICtrlSetFont($recallLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup("UA", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "If UA is currently active on self it will be cancelled, otherwise it will be cast.")
	Global Const $uaActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
	Global Const $uaLabel = GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
	Global Const $uaInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($uaActive, "toggleActive")
		GUICtrlSetOnEvent($uaInput, "setHotkey")
		If $ua Then GUICtrlSetState($uaActive, $GUI_CHECKED)
		GUICtrlSetData($uaInput, IniRead($keysIniFullPath, "idToKey", $uaHotkey, ""))
		GUICtrlSetFont($uaActive, 9.5)
		GUICtrlSetFont($uaLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup("Hide GW", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will hide the Guild Wars window and Toolbox,"&@CRLF&"You will be able to click on the gwtoolbox icon in the tray area to restore")
	Global Const $hidegwActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
	Global Const $hidegwLabel = GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
	Global Const $hidegwInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($hidegwActive, "toggleActive")
		GUICtrlSetOnEvent($hidegwInput, "setHotkey")
		If $hidegw Then GUICtrlSetState($hidegwActive, $GUI_CHECKED)
		GUICtrlSetData($hidegwInput, IniRead($keysIniFullPath, "idToKey", $hidegwHotkey, ""))
		GUICtrlSetFont($hidegwActive, 9.5)
		GUICtrlSetFont($hidegwLabel, 9.5)

	$rowY = $rowY + $groupHeight + 5
	$nameY = $rowY
	$activeY = $rowY+20
	$hotkeyY = $rowY+40
	$inputY = $rowY+55
	$rowX = 10
	GUICtrlCreateGroup("/resign", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will send /resign to Guild Wars. Will also pm you /resign in order to see that it really was used.")
	Global Const $resignActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
	Global Const $resignLabel = GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
	Global Const $resignInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($resignActive, "toggleActive")
		GUICtrlSetOnEvent($resignInput, "setHotkey")
		If $resign Then GUICtrlSetState($resignActive, $GUI_CHECKED)
		GUICtrlSetData($resignInput, IniRead($keysIniFullPath, "idToKey", $resignHotkey, ""))
		GUICtrlSetFont($resignActive, 9.5)
		GUICtrlSetFont($resignLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup("[/resign;xx]", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-1, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will send [/resign;xx] to party chat")
	Global Const $resign2Active = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
	Global Const $resign2Label = GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
	Global Const $resign2Input = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($resign2Active, "toggleActive")
		GUICtrlSetOnEvent($resign2Input, "setHotkey")
		If $teamResign Then GUICtrlSetState($resign2Active, $GUI_CHECKED)
		GUICtrlSetData($resign2Input, IniRead($keysIniFullPath, "idToKey", $teamResignHotkey, ""))
		GUICtrlSetFont($resign2Active, 9.5)
		GUICtrlSetFont($resign2Label, 9.5)

	$rowX += 100
	Global $clickerToggle = False
	GUICtrlCreateGroup("Clicker", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "Pressing the hotkey will toggle the Clicker."&@CRLF&"When active, it will click very fast")
	Global Const $clickerActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
	Global Const $clickerLabel = GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
	Global Const $clickerInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($clickerActive, "toggleActive")
		GUICtrlSetOnEvent($clickerInput, "setHotkey")
		If $clicker Then GUICtrlSetState($clickerActive, $GUI_CHECKED)
		GUICtrlSetData($clickerInput, IniRead($keysIniFullPath, "idToKey", $clickerHotkey, ""))
		GUICtrlSetFont($clickerActive, 9.5)
		GUICtrlSetFont($clickerLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup("Res Scrolls", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-3, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will use a res scroll from your inventory")
	Global Const $resActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
	Global Const $resLabel = GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
	Global Const $resInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($resActive, "toggleActive")
		GUICtrlSetOnEvent($resInput, "setHotkey")
		If $res Then GUICtrlSetState($resActive, $GUI_CHECKED)
		GUICtrlSetData($resInput, IniRead($keysIniFullPath, "idToKey", $resHotkey, ""))
		GUICtrlSetFont($resActive, 9.5)
		GUICtrlSetFont($resLabel, 9.5)

	$rowY = $rowY + $groupHeight + 5
	$nameY = $rowY
	$activeY = $rowY+20
	$hotkeyY = $rowY+40
	$inputY = $rowY+55
	$rowX = 10
	GUICtrlCreateGroup("/age", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will send /age to Guild Wars")
	Global Const $ageActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
	Global Const $ageLabel = GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
	Global Const $ageInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($ageActive, "toggleActive")
		GUICtrlSetOnEvent($ageInput, "setHotkey")
		If $age Then GUICtrlSetState($ageActive, $GUI_CHECKED)
		GUICtrlSetData($ageInput, IniRead($keysIniFullPath, "idToKey", $ageHotkey, ""))
		GUICtrlSetFont($ageActive, 9.5)
		GUICtrlSetFont($ageLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup("Age pm", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will display an in-game message with the current time."&@CRLF& _
		"In Urgoz it will also display the status of the doors")
	Global Const $agepmActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
	Global Const $agepmLabel = GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
	Global Const $agepmInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($agepmActive, "toggleActive")
		GUICtrlSetOnEvent($agepmInput, "setHotkey")
		If $agepm Then GUICtrlSetState($agepmActive, $GUI_CHECKED)
		GUICtrlSetData($agepmInput, IniRead($keysIniFullPath, "idToKey", $agepmHotkey, ""))
		GUICtrlSetFont($agepmActive, 9.5)
		GUICtrlSetFont($agepmLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup("Pstones", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will use a powerstone.")
	Global Const $pstoneActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
	Global Const $pstoneLabel = GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
	Global Const $pstoneInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($pstoneActive, "toggleActive")
		GUICtrlSetOnEvent($pstoneInput, "setHotkey")
		If $pstone Then GUICtrlSetState($pstoneActive, $GUI_CHECKED)
		GUICtrlSetData($pstoneInput, IniRead($keysIniFullPath, "idToKey", $pstoneHotkey, ""))
		GUICtrlSetFont($pstoneActive, 9.5)
		GUICtrlSetFont($pstoneLabel, 9.5)

	$rowX += 100
	GUICtrlCreateGroup("Focus", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "By pressing this hotkey the toolbox will swap the focus between GW and the Toolbox itself."&@CRLF&"Very useful when playing with toolbox non always on top."&@CRLF&"If neither toolbox nor Gw are on top, GW will be given focus")
	Global Const $focusActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
	Global Const $focusLabel = GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
	Global Const $focusInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($focusActive, "toggleActive")
		GUICtrlSetOnEvent($focusInput, "setHotkey")
		If $focus Then GUICtrlSetState($focusActive, $GUI_CHECKED)
		GUICtrlSetData($focusInput, IniRead($keysIniFullPath, "idToKey", $focusHotkey, ""))
		GUICtrlSetFont($focusActive, 9.5)
		GUICtrlSetFont($focusLabel, 9.5)


#endregion
#region TabHotkeys2
GUICtrlCreateTabItem("Hotkeys2")
	Global $groupWidth = 80
	Global $groupHeight = 85
	Global $rowY = 25
	Global $rowX = 10
	Global $nameY = $rowY
	Global $activeY = $rowY+20
	Global $hotkeyY = $rowY+40
	Global $inputY = $rowY+55
	GUICtrlCreateGroup("Target Boo", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-3, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will target the closest Boo, the ghost created by ghost-in-a-box"&@CRLF&"When it's targeted you can normally use any skill that targets an ally")
	Global Const $ghosttargetActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
		GUICtrlSetFont(-1, 9.5)
	Global Const $ghosttargetInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($ghosttargetActive, "toggleActive")
		GUICtrlSetOnEvent($ghosttargetInput, "setHotkey")
		If $ghosttarget Then GUICtrlSetState($ghosttargetActive, $GUI_CHECKED)
		GUICtrlSetData($ghosttargetInput, IniRead($keysIniFullPath, "idToKey", $ghosttargetHotkey, ""))

	$rowX += 100
	GUICtrlCreateGroup("Pop Ghost", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-3, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will use a ghost-in-the-box from your inventory")
	Global Const $ghostpopActive = 	GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	Global Const $ghostpopInput = 	MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($ghostpopActive, "toggleActive")
		GUICtrlSetOnEvent($ghostpopInput, "setHotkey")
		If $ghostpop Then GUICtrlSetState($ghostpopActive, $GUI_CHECKED)
		GUICtrlSetData($ghostpopInput, IniRead($keysIniFullPath, "idToKey", $ghostpopHotkey, ""))

	$rowX += 100
	GUICtrlCreateGroup("Looter", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will pickup all assigned items in range 'nearby' of the player")
	Global Const $looterActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
		GUICtrlSetFont(-1, 9.5)
	Global Const $looterInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($looterActive, "toggleActive")
		GUICtrlSetOnEvent($looterInput, "setHotkey")
		If $looter Then GUICtrlSetState($looterActive, $GUI_CHECKED)
		GUICtrlSetData($looterInput, IniRead($keysIniFullPath, "idToKey", $looterHotkey, ""))

	$rowX += 100
	GUICtrlCreateGroup("Identifier", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will identify all items in inventory")
	Global Const $identifierActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
		GUICtrlSetFont(-1, 9.5)
	Global Const $identifierInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($identifierActive, "toggleActive")
		GUICtrlSetOnEvent($identifierInput, "setHotkey")
		If $identifier Then GUICtrlSetState($identifierActive, $GUI_CHECKED)
		GUICtrlSetData($identifierInput, IniRead($keysIniFullPath, "idToKey", $identifierHotkey, ""))

	$rowY = $rowY + $groupHeight + 5
	$nameY = $rowY
	$activeY = $rowY+20
	$hotkeyY = $rowY+40
	$inputY = $rowY+55
	$rowX = 10
	GUICtrlCreateGroup("Pop Ghastly", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-3, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will use a Ghastly Summoning Stone")
	Global Const $gstonepopActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
		GUICtrlSetFont(-1, 9.5)
	Global Const $gstonepopInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($gstonepopActive, "toggleActive")
		GUICtrlSetOnEvent($gstonepopInput, "setHotkey")
		If $gstonepop Then GUICtrlSetState($gstonepopActive, $GUI_CHECKED)
		GUICtrlSetData($gstonepopInput, IniRead($keysIniFullPath, "idToKey", $gstonepopHotkey, ""))

	$rowX += 100
	GUICtrlCreateGroup("Pop Legion.", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-3, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will use a Legionnaire Summoning Crystal")
	Global Const $legiopopActive = GUICtrlCreateCheckbox("Active", $rowX + 8, $activeY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	GUICtrlCreateLabel("Hotkey:", $rowX + 8, $hotkeyY, 41, 17)
		GUICtrlSetFont(-1, 9.5)
	Global Const $legiopopInput = MyGuiCtrlCreateButton("", $rowX + 8, $inputY, 64, 21)
		GUICtrlSetOnEvent($legiopopActive, "toggleActive")
		GUICtrlSetOnEvent($legiopopInput, "setHotkey")
		If $legiopop Then GUICtrlSetState($legiopopActive, $GUI_CHECKED)
		GUICtrlSetData($legiopopInput, IniRead($keysIniFullPath, "idToKey", $legiopopHotkey, ""))

	$rowX += 100
	GUICtrlCreateGroup("Use Rainbow", $rowX, $nameY, $groupWidth, $groupHeight)
	MyGuiCtrlCreateButton("?", $rowX+65, $activeY-7, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, "It will use a set of rainbow rocks (red, blue and green)"&@CRLF&"It will not use a rock if it's already active")
	Global Const $rainbowuseActive = GUICtrlCreateCheckbox("Active", $rowX+8, $activeY, 53, 17)
		GUICtrlSetFont(-1, 9.5)
	GUICtrlCreateLabel("Hotkey:", $rowX+8, $hotkeyY, 41, 17)
		GUICtrlSetFont(-1, 9.5)
	Global Const $rainbowuseInput = MyGuiCtrlCreateButton("", $rowX+8, $inputY, 64, 21)
		GUICtrlSetOnEvent($rainbowuseActive, "toggleActive")
		GUICtrlSetOnEvent($rainbowuseInput, "setHotkey")
		If $rainbowuse Then GUICtrlSetState($rainbowuseActive, $GUI_CHECKED)
		GUICtrlSetData($rainbowuseInput, IniRead($keysIniFullPath, "idToKey", $rainbowuseHotkey, ""))

	$rowX += 100

	GUICtrlCreateGroup("", $rowX, $nameY, $groupWidth, $groupHeight)


	$rowY = $rowY + $groupHeight + 5
	$nameY = $rowY
	$activeY = $rowY+20
	$hotkeyY = $rowY+40
	$inputY = $rowY+55
	$rowX = 10
	GUICtrlCreateGroup("", $rowX, $nameY, $groupWidth, $groupHeight)

	$rowX += 100
	GUICtrlCreateGroup("", $rowX, $nameY, $groupWidth, $groupHeight)

	$rowX += 100
	GUICtrlCreateGroup("", $rowX, $nameY, $groupWidth, $groupHeight)

	$rowX += 100
	GUICtrlCreateGroup("", $rowX, $nameY, $groupWidth, $groupHeight)


#endregion TabHotkeys2
#region TabCons
GUICtrlCreateTabItem("Cons")
	Global Enum $pconsConsArmor = 1, $pconsConsGrail, $pconsConsEssence, $pconsRedrock, $pconsBluerock, $pconsGreenrock, _
				$pconsPie, $pconsCupcake, $pconsApple, $pconsCorn, $pconsEgg, $pconsKabob, $pconsWarSupply, $pconsLunars, $pconsSkaleSoup, $pconsMobstoppers, $pconsPahnai
	Global $pconsEffects[18]
		$pconsEffects[0] = 17
		$pconsEffects[1] = $EFFECT_CONS_ARMOR
		$pconsEffects[2] = $EFFECT_CONS_GRAIL
		$pconsEffects[3] = $EFFECT_CONS_ESSENCE
		$pconsEffects[4] = $EFFECT_REDROCK
		$pconsEffects[5] = $EFFECT_BLUEROCK
		$pconsEffects[6] = $EFFECT_GREENROCK
		$pconsEffects[7] = $EFFECT_PIE
		$pconsEffects[8] = $EFFECT_CUPCAKE
		$pconsEffects[9] = $EFFECT_APPLE
		$pconsEffects[10] = $EFFECT_CORN
		$pconsEffects[11] = $EFFECT_EGG
		$pconsEffects[12] = $EFFECT_KABOBS
		$pconsEffects[13] = $EFFECT_WARSUPPLIES
		$pconsEffects[14] = $EFFECT_LUNARS
		$pconsEffects[15] = $EFFECT_SKALE_VIGOR
		$pconsEffects[16] = $EFFECT_WEAKENED_BY_DHUUM
		$pconsEffects[17] = $EFFECT_PAHNAI_SALAD

	Global $pconsCityEffects[5]
		$pconsCityEffects[0] = 4
		$pconsCityEffects[1] = $EFFECT_BLUE_DRINK
		$pconsCityEffects[2] = $EFFECT_CHOCOLATE_BUNNY
		$pconsCityEffects[3] = $EFFECT_CREME_BRULEE
		$pconsCityEffects[4] = $EFFECT_RED_BEAN_CAKE_FRUITCAKE

	Global $pconsCityModels[7]
		$pconsCityModels[0] = 6
		$pconsCityModels[1] = $ITEM_ID_SUGARY_BLUE_DRINK
		$pconsCityModels[2] = $ITEM_ID_CHOCOLATE_BUNNY
		$pconsCityModels[3] = $ITEM_ID_FRUITCAKE
		$pconsCityModels[4] = $ITEM_ID_CREME_BRULEE
		$pconsCityModels[5] = $ITEM_ID_RED_BEAN_CAKE
		$pconsCityModels[6] = $ITEM_ID_JAR_OF_HONEY

	Global $AlcoholUsageTimer = TimerInit()
	Global $AlcoholUsageCount = 0

	Global Const $pconsStatusGroup = GUICtrlCreateGroup("Status", 10, 25, 130, 50)
	Global Const $pconsStatusLabel = GUICtrlCreateLabel("Waiting...", 20, 40, 110, 30, BitOR($SS_CENTER, $SS_CENTERIMAGE))
		GUICtrlSetFont(-1, 15)
		GUICtrlSetData($pconsStatusLabel, ($pcons) ? "Active" : "Disabled")
		GUICtrlSetColor($pconsStatusLabel, ($pcons) ? $COLOR_GREEN : $COLOR_RED)
	Global Const $pconsToggle = MyGuiCtrlCreateButton("Toggle Active", 149, 34, 100, 40)
		GUICtrlSetOnEvent(-1, "toggleActive")

	Global Const $pconsHotkeyGroup = GUICtrlCreateGroup("Toggle Hotkey:", 258, 25, 130, 50)
	Global Const $pconsHotkeyActive = GUICtrlCreateCheckbox("Active", 258+10, 25+25, 50, 17)
		GUICtrlSetOnEvent(-1, "toggleActive")
		GUICtrlSetFont(-1, 9.5)
		GUICtrlSetTip(-1, "Activate/Disable the hotkey to enable/disable the pcons feature")
		If $pconsHotkey Then GUICtrlSetState($pconsHotkeyActive, $GUI_CHECKED)
	Global Const $pconsHotkeyInput = MyGuiCtrlCreateButton("", 258+65, 25+21, 57, 22)
		GUICtrlSetOnEvent($pconsHotkeyInput, "setHotkey")
		GUICtrlSetData($pconsHotkeyInput, IniRead($keysIniFullPath, "idToKey", $pconsHotkeyHotkey, ""))

	Global $pconsX = 15
	Global $pconsY = 80
	Global Const $pconsConsCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, $pconsY, 110, 18)
		GUICtrlSetTip($pconsConsCheckbox, _
			"Warning:"&@CRLF& _
			"Cons will be used when all players have loaded into the instance and are alive,"&@CRLF& _
			"they must be in radar range and this will only occur within the first 60 seconds."&@CRLF& _
			"Make sure the right party size is detected")

    Global Const $pconsEssenceCheckbox = 	GUICtrlCreateCheckbox("", $pconsX, $pconsY+20, 190, 18)
	    GUICtrlSetTip($pconsEssenceCheckbox, _
			"Essence will be used when all players have loaded into the instance and are alive,"&@CRLF& _
			"they must be in radar range and this will only occur within the first 60 seconds."&@CRLF& _
			"Make sure the right party size is detected")

	Global Const $pconsRRCCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+40, 190, 18)
	Global Const $pconsBRCCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+60, 190, 18)
	Global Const $pconsGRCCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+80, 190, 18)
	Global Const $pconsAlcoholCheckbox = 	GUICtrlCreateCheckbox("", $pconsX,	$pconsY+100, 190, 18)
		GUICtrlSetTip(-1, "The Toolbox will use most common kinds of alcohol"&@CRLF&"Note that it will use the first item found in inventory (starting from the backpack)")
	Global Const $pconsPieCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+120,190, 18)
	Global Const $pconsCupcakeCheckbox = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+140,190, 18)
	Global Const $pconsSkaleSoupCheckbox = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+160,190, 18)
		GUICtrlSetTip(-1, "(+1 Health regeneration for 10 minutes)")
	$pconsX = 215
	Global Const $pconsAppleCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY, 	190, 18)
	Global Const $pconsCornCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+20, 190, 18)
	Global Const $pconsEggCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+40, 190, 18)
	Global Const $pconsKabobCheckbox = 		GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+60, 190, 18)
		GUICtrlSetTip(-1, "(+5 Armor for 5 minutes)")
	Global Const $pconsWarSupplyCheckbox = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+80, 190, 18)
	Global Const $pconsLunarsCheckbox = 	GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+100,190, 18)
		GUICtrlSetTip(-1, "It will use Lunar Fortunes until the Lunar Blessing is obtained")
    Global Const $pconsMoraleCheckbox =		GUICtrlCreateCheckbox("", $pconsX,  $pconsY+120, 190, 18)
	    GUICtrlSetTip(-1, "It will use Pumpkin Cookies so you always have 10% Morale boost")
	Global Const $pconsMobstoppersCheckbox =GUICtrlCreateCheckbox("", $pconsX, 	$pconsY+140,190, 18)
		GUICtrlSetTip(-1, "It will use mobstopper when you have an alive skeleton of dhuum targeted and it's below 25% hp and it's in the area")
	Global Const $pconsCityCheckbox = 		GUICtrlCreateCheckbox("", $pconsX,	$pconsY+160,190, 18)
		GUICtrlSetTip(-1, "It will use a Sugary Blue Drink, Chocolate Bunny, Fruitcake, Creme Brulee, Red Bean Cake or Jar of Honey when moving in a town or outpost")
		If $pconsConsActive			Then GUICtrlSetState($pconsConsCheckbox, $GUI_CHECKED)
	    If $pconsEssenceActive		Then GUICtrlSetState($pconsEssenceCheckbox, $GUI_CHECKED)
		If $pconsAlcoholActive 		Then GUICtrlSetState($pconsAlcoholCheckbox, $GUI_CHECKED)
		If $pconsRRCActive 			Then GUICtrlSetState($pconsRRCCheckbox, $GUI_CHECKED)
		If $pconsBRCActive 			Then GUICtrlSetState($pconsBRCCheckbox, $GUI_CHECKED)
		If $pconsGRCActive 			Then GUICtrlSetState($pconsGRCCheckbox, $GUI_CHECKED)
		If $pconsPieActive 			Then GUICtrlSetState($pconsPieCheckbox, $GUI_CHECKED)
		If $pconsCupcakeActive 		Then GUICtrlSetState($pconsCupcakeCheckbox, $GUI_CHECKED)
		If $pconsAppleActive 		Then GUICtrlSetState($pconsAppleCheckbox, $GUI_CHECKED)
		If $pconsCornActive			Then GUICtrlSetState($pconsCornCheckbox, $GUI_CHECKED)
		If $pconsEggActive 			Then GUICtrlSetState($pconsEggCheckbox, $GUI_CHECKED)
		If $pconsKabobActive 		Then GUICtrlSetState($pconsKabobCheckbox, $GUI_CHECKED)
		If $pconsWarSupplyActive 	Then GUICtrlSetState($pconsWarSupplyCheckbox, $GUI_CHECKED)
		If $pconsLunarsActive 		Then GUICtrlSetState($pconsLunarsCheckbox, $GUI_CHECKED)
		If $pconsSkaleSoupActive 	Then GUICtrlSetState($pconsSkaleSoupCheckbox, $GUI_CHECKED)
		If $pconsMobstoppersActive 	Then GUICtrlSetState($pconsMobstoppersCheckbox, $GUI_CHECKED)
		If $pconsCityActive			Then GUICtrlSetState($pconsCityActive, $GUI_CHECKED)
	    If $pconsMoraleActive		Then GUICtrlSetState($pconsMoraleActive, $GUI_CHECKED)
		GUICtrlSetOnEvent($pconsConsCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsEssenceCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsAlcoholCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsRRCCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsBRCCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsGRCCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsPieCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsCupcakeCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsAppleCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsCornCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsEggCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsKabobCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsWarSupplyCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsLunarsCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsSkaleSoupCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsMobstoppersCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsCityCheckbox, "pconsToggle")
		GUICtrlSetOnEvent($pconsMoraleCheckbox, "pconsToggle")

	Global Const $pconsPresetList = GUICtrlCreateCombo("", 10, 259, 125, 20, $CBS_DROPDOWNLIST)
	Global Const $pconsPresetDefault = "Select Preset..."
		GUICtrlSetColor(-1, $COLOR_WHITE)
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetOnEvent(-1, "presetLoad")
		SetPresetCombo($pconsPresetList, $pconsPresetDefault)

	Global Const $pconsPresetNew = MyGuiCtrlCreateButton("Save...", 145, 260, 75, 22)
		GUICtrlSetOnEvent(-1, "presetSave")

	Global Const $pconsPresetDelete = MyGuiCtrlCreateButton("Delete...", 230, 260, 75, 22)
		GUICtrlSetOnEvent(-1, "presetDelete")

	Global Const $pconsScan = MyGuiCtrlCreateButton("Update", 315, 260, 75, 22)
		GUICtrlSetTip(-1, "Scan inventory")
		GUICtrlSetOnEvent(-1, "pconsScanInventory")

#endregion
#region TabBuilds
GUICtrlCreateTabItem("Builds")

	Global Const $buildsNumber = 16
	Global Const $buildsPartySize = 12
	Global $buildsX = 5

	Global $buildsEdit[$buildsNumber]
	Global $buildsLabel[$buildsNumber]

	Global $rowy = 5

	For $i = 0 To $buildsNumber-1
		If $i == $buildsNumber/2 Then
			$buildsX = 205
			$rowy = 5
		EndIf
		$rowy += 32
		$buildsEdit[$i] = MyGuiCtrlCreateButton("Edit", $buildsX, 		$rowy, 25, 20)
		$buildsLabel[$i] = GUICtrlCreateLabel("", 	$buildsX + 30,  $rowy+3, 160, 20)
			GUICtrlSetTip($buildsLabel[$i], "Click to send teambuild to chat")
			GUICtrlSetOnEvent($buildsEdit[$i], "customTeambuild")
			GUICtrlSetData($buildsLabel[$i], IniRead($iniFullPath, "builds"&($i+1), "buildname", "<No team build>"))
			GUICtrlSetOnEvent($buildsLabel[$i], "sendTeamBuilds")
	Next

#endregion TabBuilds
#region TabAttributes
#region TabDialogs
GUICtrlCreateTabItem("Dialogs")
;~ 	_GUICtrlTab_SetBkColor($mainGui, $hTab, $COLOR_BLACK)

	GUICtrlCreateGroup("Ferry & Co.", 10, 25, 95, 228)

	    Global Const $dialogsYes 			=	MyGuiCtrlCreateButton("0x84", 20, 46, 75, 23)
		Global Const $dialogsNo 			=	MyGuiCtrlCreateButton("0x85", 20, 75, 75, 23)
		Global Const $dialogsFerryConsulate	=  	MyGuiCtrlCreateButton("Consulate", 20, 104, 75, 23)
		Global Const $dialogsFerryKaineng 	= 	MyGuiCtrlCreateButton("Kaineng", 20, 133, 75, 23)
		Global Const $dialogsFerryLionsArch =	MyGuiCtrlCreateButton("Lions Arch", 20, 162, 75, 23)
		Global Const $dialogsFerryLionsGate = 	MyGuiCtrlCreateButton("Lions Gate", 20, 191, 75, 23)
			GUICtrlSetOnEvent($dialogsYes, "guiDialogsEventHandler")
			GUICtrlSetOnEvent($dialogsNo, "guiDialogsEventHandler")
			GUICtrlSetOnEvent($dialogsFerryConsulate, "guiDialogsEventHandler")
			GUICtrlSetOnEvent($dialogsFerryKaineng, "guiDialogsEventHandler")
			GUICtrlSetOnEvent($dialogsFerryLionsArch, "guiDialogsEventHandler")
			GUICtrlSetOnEvent($dialogsFerryLionsGate, "guiDialogsEventHandler")

	Global Const $dialogsCustom = MyGuiCtrlCreateButton("Custom "&@CR&"Dialog...", 12, 260, 92, 34, 0xFFFFFF, 0x222222, 1, $SS_CENTER)
		GUICtrlSetOnEvent(-1, "customDialogGui")

	Global Const $dialogsTake4H = 		MyGuiCtrlCreateButton("UW - Four Horseman", 110, 34, 135, 22)
	Global Const $dialogsTakeDemonAss = MyGuiCtrlCreateButton("UW - Demon Assassin", 110, 62, 135, 22)
	Global Const $dialogsTakeToS = 		MyGuiCtrlCreateButton("FoW - ToS", 250, 34, 135, 22)
	Global Const $dialogsTakeFury = 	MyGuiCtrlCreateButton("DoA - Foundry Reward", 250, 62, 135, 22)
		GUICtrlSetOnEvent($dialogsTake4H, "guiDialogsEventHandler")
		GUICtrlSetOnEvent($dialogsTakeDemonAss, "guiDialogsEventHandler")
		GUICtrlSetOnEvent($dialogsTakeToS, "guiDialogsEventHandler")
		GUICtrlSetOnEvent($dialogsTakeFury, "guiDialogsEventHandler")

	Local $lDialogs = ""
	For $i=1 To $DIALOGS_NAME[0]
		$lDialogs = $lDialogs & "|" & $DIALOGS_NAME[$i]
	Next
	$rowY = 82
	$rowX = 110
	$groupHeight = 45
	$groupWidth = 280
	Local $activeX = $rowX+8
	Local $hotkeyX = $activeX+55
	Local $comboX = $hotkeyX+60
	GUICtrlCreateGroup("", $rowX, $rowY, $groupWidth, $groupHeight)
	Global Const $DialogHK1Active = GUICtrlCreateCheckbox("Active", $activeX, $rowY+20, 53, 17)
	Global Const $DialogHK1Label = GUICtrlCreateLabel("Hotkey:", $hotkeyX, $rowY+10, 41, 17)
	Global Const $DialogHK1Input = MyGuiCtrlCreateButton("", $hotkeyX, $rowY+25, 55, 15)
	Global Const $DialogHK1Combo = GUICtrlCreateCombo("", $comboX, $rowY+15, 150, 24, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
		GUICtrlSetData(-1, $lDialogs)
		If $DialogHK1Number > 0 Then GUICtrlSetData(-1, $DIALOGS_NAME[$DialogHK1Number])
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetOnEvent($DialogHK1Active, "toggleActive")
		GUICtrlSetOnEvent($DialogHK1Input, "setHotkey")
		GUICtrlSetOnEvent($DialogHK1Combo, "setDialogID")
		If $DialogHK1 Then GUICtrlSetState($DialogHK1Active, $GUI_CHECKED)
		GUICtrlSetData($DialogHK1Input, IniRead($keysIniFullPath, "idToKey", $DialogHK1Hotkey, ""))
		GUICtrlSetFont($DialogHK1Active, 9.5)
		GUICtrlSetFont($DialogHK1Label, 9.5)

	$rowY += 42
	GUICtrlCreateGroup("", $rowX, $rowY, $groupWidth, $groupHeight)
	Global Const $DialogHK2Active = GUICtrlCreateCheckbox("Active", $activeX, $rowY+20, 53, 17)
	Global Const $DialogHK2Label = GUICtrlCreateLabel("Hotkey:", $hotkeyX, $rowY+10, 41, 17)
	Global Const $DialogHK2Input = MyGuiCtrlCreateButton("", $hotkeyX, $rowY+25, 55, 15)
	Global Const $DialogHK2Combo = GUICtrlCreateCombo("", $comboX, $rowY+15, 150, 24, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
		GUICtrlSetData(-1, $lDialogs)
		If $DialogHK2Number > 0 Then GUICtrlSetData(-1, $DIALOGS_NAME[$DialogHK2Number])
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetOnEvent($DialogHK2Active, "toggleActive")
		GUICtrlSetOnEvent($DialogHK2Input, "setHotkey")
		GUICtrlSetOnEvent($DialogHK2Combo, "setDialogID")
		If $DialogHK2 Then GUICtrlSetState($DialogHK2Active, $GUI_CHECKED)
		GUICtrlSetData($DialogHK2Input, IniRead($keysIniFullPath, "idToKey", $DialogHK2Hotkey, ""))
		GUICtrlSetFont($DialogHK2Active, 9.5)
		GUICtrlSetFont($DialogHK2Label, 9.5)

	$rowY += 42
	GUICtrlCreateGroup("", $rowX, $rowY, $groupWidth, $groupHeight)
	Global Const $DialogHK3Active = GUICtrlCreateCheckbox("Active", $activeX, $rowY+20, 53, 17)
	Global Const $DialogHK3Label = GUICtrlCreateLabel("Hotkey:", $hotkeyX, $rowY+10, 41, 17)
	Global Const $DialogHK3Input = MyGuiCtrlCreateButton("", $hotkeyX, $rowY+25, 55, 15)
	Global Const $DialogHK3Combo = GUICtrlCreateCombo("", $comboX, $rowY+15, 150, 24, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
		GUICtrlSetData(-1, $lDialogs)
		If $DialogHK3Number > 0 Then GUICtrlSetData(-1, $DIALOGS_NAME[$DialogHK3Number])
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetOnEvent($DialogHK3Active, "toggleActive")
		GUICtrlSetOnEvent($DialogHK3Input, "setHotkey")
		GUICtrlSetOnEvent($DialogHK3Combo, "setDialogID")
		If $DialogHK3 Then GUICtrlSetState($DialogHK3Active, $GUI_CHECKED)
		GUICtrlSetData($DialogHK3Input, IniRead($keysIniFullPath, "idToKey", $DialogHK3Hotkey, ""))
		GUICtrlSetFont($DialogHK3Active, 9.5)
		GUICtrlSetFont($DialogHK3Label, 9.5)

	$rowY += 42
	GUICtrlCreateGroup("", $rowX, $rowY, $groupWidth, $groupHeight)
	Global Const $DialogHK4Active = GUICtrlCreateCheckbox("Active", $activeX, $rowY+20, 53, 17)
	Global Const $DialogHK4Label = GUICtrlCreateLabel("Hotkey:", $hotkeyX, $rowY+10, 41, 17)
	Global Const $DialogHK4Input = MyGuiCtrlCreateButton("", $hotkeyX, $rowY+25, 55, 15)
	Global Const $DialogHK4Combo = GUICtrlCreateCombo("", $comboX, $rowY+15, 150, 24, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
		GUICtrlSetData(-1, $lDialogs)
		If $DialogHK4Number > 0 Then GUICtrlSetData(-1, $DIALOGS_NAME[$DialogHK4Number])
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetOnEvent($DialogHK4Active, "toggleActive")
		GUICtrlSetOnEvent($DialogHK4Input, "setHotkey")
		GUICtrlSetOnEvent($DialogHK4Combo, "setDialogID")
		If $DialogHK4 Then GUICtrlSetState($DialogHK4Active, $GUI_CHECKED)
		GUICtrlSetData($DialogHK4Input, IniRead($keysIniFullPath, "idToKey", $DialogHK4Hotkey, ""))
		GUICtrlSetFont($DialogHK4Active, 9.5)
		GUICtrlSetFont($DialogHK4Label, 9.5)

	$rowY += 42
	GUICtrlCreateGroup("", $rowX, $rowY, $groupWidth, $groupHeight)
	Global $DialogStartClassChangeBtn = MyGuiCtrlCreateButton("Go!", $activeX, $rowY+20, 100, 15)
	Global $DialogSelectClassToChangeToCombo = GUICtrlCreateCombo("", $comboX, $rowY+15, 150, 24, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
	GUICtrlSetBkColor(-1, $COLOR_BLACK)
	GUICtrlSetData($DialogSelectClassToChangeToCombo, "Assassin|Dervish|Elementalist|Mesmer|Monk|Necromancer|Paragon|Ranger|Ritualist|Warrior")
	GUICtrlSetOnEvent($DialogStartClassChangeBtn, "initiateClassChange")

#endregion
#region TabInfo
GUICtrlCreateTabItem("Info")
;~ 	_GUICtrlTab_SetBkColor($mainGui, $hTab, $COLOR_BLACK)
	GUICtrlCreateGroup("Timer", 5, 25, 140, 45)
	Global Const $infoTimerCheckbox = GUICtrlCreateCheckbox("Show", 15, 42)
		GUICtrlSetOnEvent(-1, "toggleTimer")
		If $timer Then GUICtrlSetState(-1, $GUI_CHECKED)
	Global Const $infoTimerColorPicker = _GUIColorPicker_Create("Color", 80, 40, 60, 25, $COLOR_TIMER, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Timer Color')
		GUICtrlSetColor(-1, $COLOR_BLACK)
		GUICtrlSetBkColor(-1, $COLOR_TIMER)
		GUICtrlSetOnEvent(-1, "guiEventHandler")

	GUICtrlCreateGroup("Target Health", 5, 80, 140, 80)
	Global Const $infoHealthChckbox = GUICtrlCreateCheckbox("Show", 15, 97)
		GUICtrlSetOnEvent(-1, "toggleHealth")
		If $health Then GUICtrlSetState(-1, $GUI_CHECKED)
	Global Const $infoHealthColorPicker1 = _GUIColorPicker_Create("Color 1", 80, 95, 60, 25, $COLOR_HEALTH_HIGHT, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Target Health Color 1')
		GUICtrlSetColor(-1, $COLOR_BLACK)
		GUICtrlSetBkColor(-1, $COLOR_HEALTH_HIGHT)
		GUICtrlSetOnEvent(-1, "guiEventHandler")
	Global Const $infoHealthColorPicker2 = _GUIColorPicker_Create("Color 2", 15, 125, 60, 25, $COLOR_HEALTH_MIDDLE, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Target Health Color 2')
		GUICtrlSetColor(-1, $COLOR_BLACK)
		GUICtrlSetBkColor(-1, $COLOR_HEALTH_MIDDLE)
		GUICtrlSetOnEvent(-1, "guiEventHandler")
	Global Const $infoHealthColorPicker3 = _GUIColorPicker_Create("Color 3", 80, 125, 60, 25, $COLOR_HEALTH_LOW, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Target Health Color 3')
		GUICtrlSetColor(-1, $COLOR_BLACK)
		GUICtrlSetBkColor(-1, $COLOR_HEALTH_LOW)
		GUICtrlSetOnEvent(-1, "guiEventHandler")

	GUICtrlCreateGroup("Party Danger", 5, 170, 140, 45)
	Global Const $infoPartyCheckbox = GUICtrlCreateCheckbox("Show", 15, 187)
		GUICtrlSetOnEvent(-1, "toggleParty")
		If $party Then GUICtrlSetState(-1, $GUI_CHECKED)
	Global Const $infoPartyColorPicker = _GUIColorPicker_Create("Color", 80, 185, 60, 25, $COLOR_PARTY, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Party Color')
		GUICtrlSetColor(-1, $COLOR_BLACK)
		GUICtrlSetBkColor(-1, $COLOR_PARTY)
		GUICtrlSetOnEvent(-1, "guiEventHandler")

	GUICtrlCreateGroup("Target Distance", 5, 220, 140, 45)
	Global Const $infoDistanceCheckbox = GUICtrlCreateCheckbox("Show", 15, 237)
		GUICtrlSetOnEvent(-1, "toggleDistance")
		If $distance Then GUICtrlSetState(-1, $GUI_CHECKED)
	Global Const $infoDistanceColorPicker = _GUIColorPicker_Create("Color", 80, 235, 60, 25, $COLOR_DISTANCE, BitOR($CP_FLAG_CHOOSERBUTTON, $CP_FLAG_MAGNIFICATION, $CP_FLAG_ARROWSTYLE), 0, -1, -1, 0, 'Distance Color')
		GUICtrlSetColor(-1, $COLOR_BLACK)
		GUICtrlSetBkColor(-1, $COLOR_DISTANCE)
		GUICtrlSetOnEvent(-1, "guiEventHandler")

	GUICtrlCreateGroup("E/mo Bonds Monitor", 160, 25, 140, 65)
	Global Const $infoBuffsCheckbox = GUICtrlCreateCheckbox("Show", 170, 42)
		GUICtrlSetOnEvent(-1, "toggleBuffs")
		If $buffs Then GUICtrlSetState(-1, $GUI_CHECKED)
	Global Const $infoBuffsDropCheckbox = GUICtrlCreateCheckbox("Drop on click", 170, 63)
		GUICtrlSetOnEvent(-1, "toggleActive")
		If $buffsDrop Then GUICtrlSetState(-1, $GUI_CHECKED)
	MyGuiCtrlCreateButton("?", 280, 45, 10, 13)
		GUICtrlSetFont(-1, 9)
		GUICtrlSetTip(-1, 	"It will show which bonds are used on each party member."&@CRLF& _
							"Place the monitor next to the party window, it will have the same height."&@CRLF& _
							@CRLF& _
							"Notes:"&@CRLF& _
							"- It will work properly only in parties with no heroes or henchmen"&@CRLF& _
							"- It will only show the bonds you are maintaining"&@CRLF& _
							"- If you tick 'Drop on click' you can drop bonds when clicking on the bond in the monitor")


	Global Const $settingsButton = MyGuiCtrlCreateButton("Open Settings Folder", 70, 280, 130, 15, 0xAAAAAA, 0x222222, 1)
		GUICtrlSetFont(-1, "8")
		GUICtrlSetOnEvent(-1, "guiEventHandler")
		GUICtrlSetTip(-1, "The location where the GWToolbox.ini file is saved with all your settings, you can delete it to reset toolbox or save it to backup and then restore later")

	Global Const $websiteButton = MyGuiCtrlCreateButton("Open GWToolbox Website", 210, 280, 130, 15, 0xAAAAAA, 0x222222, 1)
		GUICtrlSetFont(-1, "8")
		GUICtrlSetOnEvent(-1, "guiEventHandler")
		GUICtrlSetTip(-1, "The website where you can find the GWToolbox executable, source code, and full change history.")

#endregion TabInfo

#region setupGui
If $timer Then
	$timer = Not $timer
	toggleTimer()
EndIf

If $health Then
	$health = Not $health
	toggleHealth()
EndIf

If $party Then
	$party = Not $party
	toggleParty()
EndIf

If $distance Then
	$distance = Not $distance
	toggleDistance()
EndIf

If $buffs Then
	$buffs = Not $buffs
	toggleBuffs()
EndIf

GUICtrlSetState($mainlabel, $GUI_FOCUS)
GUISetState(@SW_SHOW, $mainGui)

#endregion setupGui
#endregion GUI
#region HelpMsgBoxes
Func helpMaterialsBuyer()
	Local $lTitle = "Help: Materials Buyer"
	Local $lText = 	"You have to talk to the materials trader before using the materials buyer. You can even close the materials trader window"&@CRLF&@CRLF& _
					"The Materials Buyer will stop working if you talk with another trader or crafter NPC (even cons crafters). At that point you have to zone or change district in order to make it work again"&@CRLF&@CRLF& _
					"The Materials Buyer will be unable to buy materials or estimate price if you move while it's doing such actions."&@CRLF&@CRLF& _
					"When opened, the Materials Buyer will try to estimate prices. You do not have to wait for it to end, but if you start buying before it is done then the estimation process will be cancelled, (buying will still work normally)"&@CRLF&@CRLF& _
					"Quantity specifies the number of trades OR the number of items to buy materials for. Ex.: You will get 50 bones if you put 5 and then buy bones; You will get materials for 1 conset if you put 1 and then buy mats for conset"
	MyGuiMsgBox(0, $lTitle, $lText, $mainGui, 500, 320, True)
EndFunc
#endregion

Func mainLoop()
	#region declarations
	Local $currentMap = -1
	Local $currentLoadingState = -1

	; make variables to store the state of keys (pressed/not pressed)
	; this is to ensure that a keypress trigger its function only once
	Local $pressedStuck = False
	Local $pressedRecall = False
	Local $pressedUa = False
	Local $pressedHidegw = False
	Local $pressedClicker = False
	Local $pressedResign = False
	Local $pressedTeamResign = False
	Local $pressedRupt = False
	Local $pressedMovement = False
	Local $pressedPcons = False
	Local $pressedRes = False
	Local $pressedAge = False
	Local $pressedAgePm = False
	Local $pressedPstone = False
	Local $pressedFocus = False
	Local $pressedLooter = False
	Local $pressedIdentifier = False
	Local $pressedGhostpop = False
	Local $pressedGhosttarget = False
	Local $pressedGstonepop = False
	Local $pressedLegiopop = False
	Local $pressedRainbowuse = False
	Local $pressedDialogHK1 = False
	Local $pressedDialogHK2 = False
	Local $pressedDialogHK3 = False
	Local $pressedDialogHK4 = False

	; variable for the pressed status
	Local $pressed = False

	; timers for the pcons (prevent more than 1 pcon being used every time)
	Local $timerCons = TimerInit()
	Local $timerEssence = TimerInit()
	Local $timerRRC = TimerInit()
	Local $timerBRC = TimerInit()
	Local $timerGRC = TimerInit()
	Local $timerPie = TimerInit()
	Local $timerCupcake = TimerInit()
	Local $timerApple = TimerInit()
	Local $timerEgg = TimerInit()
	Local $timerKabob = TimerInit()
	Local $timerWarsupply = TimerInit()
	Local $timerCorn = TimerInit()
	Local $timerLunars = TimerInit()
	Local $timerAlcohol = TimerInit()
	Local $timerSkaleSoup = TimerInit()
	Local $timerMobstoppers = TimerInit()
	Local $timerMorale = TimerInit()
	Local $timerCity = TimerInit()


	; open the DLL used for _isPressed(..)
	Local $hDLL = DllOpen("user32.dll")

	; stuff for info
	Local $lTgtHP, $lTgtMaxHP

	; variable for the timer
	Local $sec, $min, $currentTime
	Local $timerDefaultColor = True

	; variable for age
	Local $msg

	; Variables that needs to be recomputed everytime
	Global $pconsRetArray = GetHasEffects($pconsEffects)
	Global $pconsRetArrayVerify = GetHasEffects($pconsEffects)
	For $i=1 To $pconsRetArray[0]
		$pconsRetArray[$i] = _Max($pconsRetArray[$i], $pconsRetArrayVerify[$i])
	Next

	Global $pconsCityRetArray = GetHasEffects($pconsCityEffects)
	Global $pconsCityRetArrayVerify = GetHasEffects($pconsCityEffects)
	For $i=1 To $pconsCityRetArray[0]
		$pconsCityRetArray[$i] = _Max($pconsCityRetArray[$i], $pconsCityRetArrayVerify[$i])
	Next

	Local $lAgentArray = GetAgentArray()
	Local $lParty = GetParty($lAgentArray)
	Local $lPartyDanger = GetPartyDanger($lAgentArray, $lParty)
	Local $lMe = GetAgentByID(-2)
	Local $lTgt = GetAgentByID(-1)

	Local $lBuff, $lBuffTarget, $lBuffTargetNumber
	#endregion

	While True
		#region maploading for alcohol
		If GetMapLoading() <> $currentLoadingState Then
			$currentLoadingState = GetMapLoading()
			If $currentLoadingState == $INSTANCETYPE_EXPLORABLE Or $currentLoadingState == $INSTANCETYPE_OUTPOST Then
				$currentMap = GetMapID()
				pconsScanInventory()
				$AlcoholUsageCount = 0
			EndIf
		ElseIf GetMapID() <> $currentMap Then
			$currentMap = GetMapID()
			pconsScanInventory()
			$AlcoholUsageCount = 0
		EndIf
		#endregion

		#region close if needed
		If Not ProcessExists($gwPID) Then
			exitProgram()
		EndIf
		#endregion

		#region ontop
		If WinActive($gwHWND) Then
			If (Not $IsOnTop) And GUICtrlRead($OnTopCheckbox)==$GUI_CHECKED Then
				WinSetOnTop($mainGui, "", True)
				$IsOnTop = True
			EndIf
		Else
			If $IsOnTop And (Not WinActive($mainGui)) Then
				WinSetOnTop($mainGui, "", False)
				$IsOnTop = False
			EndIf
		EndIf
		#endregion

		#region GetStuff
		$lMe = GetAgentByID(-2)
		$lTgt = GetAgentByID(-1)
		Switch GetMapLoading()
			Case $INSTANCETYPE_EXPLORABLE
				If $pcons And (($pconsConsActive And GetInstanceUpTime() < 61*1000)) Or $party Then
					$lAgentArray = GetAgentArray()
					$lParty = GetParty($lAgentArray)
				EndIf
				If $pcons Then
					$pconsRetArray = GetHasEffects($pconsEffects)
					$pconsRetArrayVerify = GetHasEffects($pconsEffects)
					For $i=1 To $pconsRetArray[0]
						$pconsRetArray[$i] = _Max($pconsRetArray[$i], $pconsRetArrayVerify[$i])
					Next
				EndIf
			Case $INSTANCETYPE_OUTPOST
				If $pcons Then
					$pconsCityRetArray = GetHasEffects($pconsCityEffects)
					$pconsCityRetArrayVerify = GetHasEffects($pconsCityEffects)
					For $i=1 To $pconsCityRetArray[0]
						$pconsCityRetArray[$i] = _Max($pconsCityRetArray[$i], $pconsCityRetArrayVerify[$i])
					Next
				EndIf
			Case $INSTANCETYPE_LOADING
				; do nothing
		EndSwitch
		#endregion

		#region Transparency
		If GUICtrlRead($TransparencySlider) <> $Transparency Then
			$Transparency = GUICtrlRead($TransparencySlider)
			WinSetTrans($mainGui, "", $Transparency)
			If $timer Then WinSetTrans($timerGui, "", $Transparency)
			If $health Then WinSetTrans($healthGui, "", $Transparency)
			If $distance Then WinSetTrans($distanceGui, "", $Transparency)
		EndIf
		#endregion

		#region partydanger
		If $party Then
			$lPartyDanger = GetPartyDanger($lAgentArray, $lParty)
			If GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
				For $i=1 To _Min($lPartyDanger[0], 8)
					If DllStructGetData($lParty[$i], "PlayerNumber") <= 8 Then
						GuiCtrlUpdateData($partyLabels[DllStructGetData($lParty[$i], "PlayerNumber")], $lPartyDanger[$i]); &" - "&GetPlayerName($lParty[$i]))
					EndIf
				Next
			Else
				For $i=1 To 8
					GuiCtrlUpdateData($partyLabels[$i], "-")
				Next
			EndIf
		EndIf
		#endregion

		#region buffs
		If $buffs Then
			For $i=1 To 8
				For $k=0 To 2
					$buffsStatus[$i][$k] = $BUFFS_UNKNOWN
				Next
			Next
			For $buffIndex=1 To GetBuffCount()
				$lBuff = GetBuffByIndex($buffIndex)
				$lBuffTarget = GetAgentByID(DllStructGetData($lBuff, "TargetID"))
				$lBuffTargetNumber = DllStructGetData($lBuffTarget, "PlayerNumber")
				If $lBuffTargetNumber > 0 And $lBuffTargetNumber < 9 Then
					Switch DllStructGetData($lBuff, "SkillID")
						Case 242 ; balth
							buffsShow($lBuffTargetNumber, 0)
							$buffsID[$lBuffTargetNumber][0] = DllStructGetData($lBuff, "BuffID")
						Case 241 ; lifebond
							buffsShow($lBuffTargetNumber, 1)
							$buffsID[$lBuffTargetNumber][1] = DllStructGetData($lBuff, "BuffID")
						Case 263 ; prot
							buffsShow($lBuffTargetNumber, 2)
							$buffsID[$lBuffTargetNumber][2] = DllStructGetData($lBuff, "BuffID")
					EndSwitch
				EndIf
			Next
			For $i=1 To 8
				For $k=0 To 2
					If $buffsStatus[$i][$k] == $BUFFS_UNKNOWN Then
						buffsHide($i, $k)
					EndIf
				Next
			Next
		EndIf
		#endregion

		#region health
		If $health Then
			If DllStructGetData($lTgt, "Type")==0xDB Then
				$lTgtHP = DllStructGetData($lTgt, "HP")
				$lTgtMaxHP = DllStructGetData($lTgt, "MaxHP")
					GuiCtrlUpdateData($healthGuiLabelHP, StringFormat("%.1f", $lTgtHP*100))
					If $lTgtMaxHP <> 0 Then
						GuiCtrlUpdateData($healthGuiLabelMaxHP, Round($lTgtHP*$lTgtMaxHP)&" / "&$lTgtMaxHP)
					Else
						GuiCtrlUpdateData($healthGuiLabelMaxHP, "- / -")
					EndIf
					If $lTgtHP >= 0.9 Then
						healthUpdateColor($COLOR_HEALTH_HIGHT)
					ElseIf $lTgtHP >= 0.5 Then
						healthUpdateColor($COLOR_HEALTH_MIDDLE)
					Else
						healthUpdateColor($COLOR_HEALTH_LOW)
					EndIf
			Else
				GuiCtrlUpdateData($healthGuiLabelHP, "-")
				GuiCtrlUpdateData($healthGuiLabelMaxHP, "- / -")
				healthUpdateColor($COLOR_GREY)
			EndIf
		EndIf
		#endregion health

		#region distance
		If $distance Then
			If DllStructGetData($lTgt, "Type")==0xDB Then
				distanceUpdateColor($COLOR_DISTANCE)
				Local $lDistance = GetDistance($lMe, $lTgt)
				If $lDistance < $RANGE_ADJACENT Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_ADJACENT * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Adjacent")
				ElseIf $lDistance < $RANGE_NEARBY Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_NEARBY * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Nearby")
				ElseIf $lDistance < $RANGE_AREA Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_AREA * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "In the Area")
				ElseIf $lDistance < $RANGE_EARSHOT Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_EARSHOT * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Earshot")
				ElseIf $lDistance < $RANGE_SPELLCAST Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_SPELLCAST * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Cast Range")
				ElseIf $lDistance < $RANGE_SPIRIT Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_SPIRIT * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Spirit Range")
				ElseIf $lDistance < $RANGE_COMPASS Then
					GuiCtrlUpdateData($distanceGuiLabelDist, Round($lDistance / $RANGE_COMPASS * 100))
					GuiCtrlUpdateData($distanceGuiLabelText, "Compass")
				EndIf
			Else
				GuiCtrlUpdateData($distanceGuiLabelDist, "-")
				GuiCtrlUpdateData($distanceGuiLabelText, "-")
				distanceUpdateColor($COLOR_GREY)
			EndIf
		EndIf
		#endregion distance

		#region timer
		If $timer Then
			If GetMapLoading() <> $INSTANCETYPE_LOADING Then
				$sec = Int(GetInstanceUptime()/1000)
				If $sec <> $currentTime Then
					$currentTime = $sec
					$min = Int($sec/60)
					GUICtrlSetData($timerGuiLabel, Int($min/60)&":"&StringFormat("%02d", Mod($min, 60))&":"&StringFormat("%02d", Mod($sec, 60)))
					If GetMapID() == $MAP_ID_URGOZ And GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
						$timerDefaultColor = False
						Local $temp = Mod($sec, 25)
						If $temp < 1 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_OPENING)
						ElseIf $temp < 13 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_OPEN)
						ElseIf $temp < 16 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_CLOSING)
						ElseIf $temp < 23 Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_CLOSED)
						Else
							GUICtrlSetColor($timerGuiLabel, $COLOR_URGOZ_OPENING)
						EndIf
					Else
						If Not $timerDefaultColor Then
							GUICtrlSetColor($timerGuiLabel, $COLOR_TIMER)
							$timerDefaultColor = True
						EndIf
					EndIf
				EndIf
			Else
				GuiCtrlUpdateData($timerGuiLabel, "Loading")
			EndIf
		EndIf
		#endregion

		#region sf_macro
		If $sfmacro Then
			SkillsMacro()
		EndIf
		#endregion sf_macro

		#region stuck
		If $stuck Then
			$pressed = _IsPressed($stuckHotkey, $hDLL)
			If (Not $pressedStuck) And $pressed Then
				$pressedStuck = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) And GetMapLoading()<>$INSTANCETYPE_LOADING Then
					SendChat($s_stuck, "/")
					WriteChat("/stuck", $GWToolbox)
				EndIf
			ElseIf $pressedStuck And (Not $pressed) Then
				$pressedStuck = False
			EndIf
		EndIf
		#endregion stuck

		#region recall
		If $recall Then
			$pressed = _IsPressed($recallHotkey, $hDLL)
			If (Not $pressedRecall) And $pressed Then
				$pressedRecall = True
				sendRecall()
			ElseIf $pressedRecall And (Not $pressed) Then
				$pressedRecall = False
			EndIf
		EndIf
		#endregion recall

		#region ua
		If $ua Then
			$pressed = _IsPressed($uaHotkey, $hDLL)
			If (Not $pressedUa) And $pressed Then
				$pressedUa = True
				sendUa()
			ElseIf $pressedUa And (Not $pressed) Then
				$pressedUa = False
			EndIf
		EndIf
		#endregion ua

		#region hidegw
		If $hidegw Then
			$pressed = _IsPressed($hidegwHotkey, $hDLL)
			If (Not $pressedHidegw) And $pressed Then
				$pressedHidegw = True
				sendToggleHide()
			ElseIf $pressedHidegw And (Not $pressed) Then
				$pressedHidegw = False
			EndIf
		EndIf
		#endregion hidegw

		#region clicker
		If $clicker Then
			$pressed = _IsPressed($clickerHotkey, $hDLL)
			If (Not $pressedClicker) And $pressed Then
				$pressedClicker = True
				$clickerToggle = Not $clickerToggle
				If GetMapLoading()<>$INSTANCETYPE_LOADING Then
					If $clickerToggle Then
						WriteChat("Clicker Active", $GWToolbox)
					Else
						WriteChat("Clicker Inactive", $GWToolbox)
					EndIf
				EndIf
			ElseIf $pressedClicker And (Not $pressed) Then
				$pressedClicker = False
			EndIf
		EndIf

		If $clickerToggle Then
			For $i=1 To 10
				MouseClick("left")
			Next
		EndIf
		#endregion clicker

		#region resign
		If $resign Then
			$pressed = _IsPressed($resignHotkey, $hDLL)
			If (Not $pressedResign) And $pressed Then
				$pressedResign = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) And GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
					SendChat($s_resign, "/")
					WriteChat("/resign", $GWToolbox)
				EndIf
			ElseIf $pressedResign And (Not $pressed) Then
				$pressedResign = False
			EndIf
		EndIf
		#endregion resign

		#region teamresign
		If $teamResign Then
			$pressed = _IsPressed($teamResignHotkey, $hDLL)
			If (Not $pressedTeamResign) And $pressed Then
				$pressedTeamResign = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) And GetMapLoading()<>$INSTANCETYPE_LOADING Then
					SendChat("[/resign;xx]", "#")
				EndIf
			ElseIf $pressedTeamResign And (Not $pressed) Then
				$pressedTeamResign = False
			EndIf
		EndIf
		#endregion teamresign

		#region res_scrolls
		If $res Then
			$pressed = _IsPressed($resHotkey, $hDLL)
			If (Not $pressedRes) And $pressed Then
				$pressedRes = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) And GetMapLoading()<>$INSTANCETYPE_LOADING Then
					If Not UseItemByModelID($ITEM_ID_RES_SCROLLS) Then WriteChat("[WARNING] Res scroll not found!", $GWToolbox)
				EndIf
			ElseIf $pressedRes And (Not $pressed) Then
				$pressedRes = False
			EndIf
		EndIf
		#endregion res_scrolls

		#region age
		If $age Then
			$pressed = _IsPressed($ageHotkey, $hDLL)
			If (Not $pressedAge) And $pressed Then
				$pressedAge = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) And GetMapLoading()<>$INSTANCETYPE_LOADING Then
					SendChat($s_age, "/")
				EndIf
			ElseIf $pressedAge And (Not $pressed) Then
				$pressedAge = False
			EndIf
		EndIf
		#endregion

		#region agepm
		If $agepm Then
			$pressed = _IsPressed($agepmHotkey, $hDLL)
			If (Not $pressedAgePm) And $pressed Then
				$pressedAgePm = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) And GetMapLoading()<>$INSTANCETYPE_LOADING Then
					$sec = Int(GetInstanceUptime()/1000)
					$min = Int($sec/60)
					$msg = Int($min/60)&":"&StringFormat("%02d", Mod($min, 60))&":"&StringFormat("%02d", Mod($sec, 60))
					If GetMapID() == $MAP_ID_URGOZ And GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
						Local $temp = Mod($sec, 25)
						If $temp < 1 Then
							$msg &= " - Doors CLOSED" & " - opening in 0 sec(s)"
						ElseIf $temp < 16 Then
							$msg &= " - Doors OPEN" & " - closing in "&(15-$temp)&" sec(s)"
						Else
							$msg &= " - Doors CLOSED" & " - opening in "&(25-$temp)&" sec(s)"
						EndIf
					EndIf
					WriteChat($msg, $GWToolbox)
				EndIf
			ElseIf $pressedAgePm And (Not $pressed) Then
				$pressedAgePm = False
			EndIf
		EndIf
		#endregion

		#region pstone
		If $pstone Then
			$pressed = _IsPressed($pstoneHotkey, $hDLL)
			If (Not $pressedPstone) And $pressed Then
				$pressedPstone = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) And GetMapLoading()<>$INSTANCETYPE_LOADING Then
					If Not UseItemByModelID($ITEM_ID_POWERSTONE) Then WriteChat("[WARNING] Powerstone not found!", $GWToolbox)
				EndIf
			ElseIf $pressedPstone And (Not $pressed) Then
				$pressedPstone = False
			EndIf
		EndIf
		#endregion

		#region focus
		If $focus Then
			$pressed = _IsPressed($focusHotkey, $hDLL)
			If (Not $pressedFocus) And $pressed Then
				$pressedFocus = True
				If WinActive($gwHWND) Then
					WinActivate($mainGui)
				Else
					WinActivate($gwHWND)
				EndIf
			ElseIf $pressedFocus And (Not $pressed) Then
				$pressedFocus = False
			EndIf
		EndIf

		#endregion

		#region looter
		If $looter Then
			$pressed = _IsPressed($looterHotkey, $hDLL)
			If (Not $pressedLooter) And $pressed Then
				$pressedLooter = True
				SendLooter()
			ElseIf $pressedLooter And (Not $pressed) Then
				$pressedLooter = False
			EndIf
		EndIf
		#endregion

		#Region identifier
		If $identifier Then
			$pressed = _IsPressed($identifierHotkey, $hDLL)
			If (Not $pressedIdentifier) And $pressed Then
				$pressedIdentifier = True
				SendIdentifier()
			ElseIf $pressedIdentifier And (Not $identifier) Then
				$pressedIdentifier = False
			EndIf
		EndIf
		#EndRegion

		#region ghostpop
		If $ghostpop Then
			$pressed = _IsPressed($ghostpopHotkey, $hDLL)
			If (Not $pressedGhostpop) And $pressed Then
				$pressedGhostpop = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) Then
					If Not UseItemByModelID($MODELID_GHOST_IN_THE_BOX) Then WriteChat("[WARNING] Ghost-in-the-Box not found!", $GWToolbox)
				EndIf
			ElseIf $pressedGhostpop And (Not $pressed) Then
				$pressedGhostpop = False
			EndIf
		EndIf
		#endregion

		#region ghosttarget
		If $ghosttarget Then
			$pressed = _IsPressed($ghosttargetHotkey, $hDLL)
			If (Not $pressedGhosttarget) And $pressed Then
				$pressedGhosttarget = True
				TargetGhost()
			ElseIf $pressedGhosttarget And (Not $pressed) Then
				$pressedGhosttarget = False
			EndIf
		EndIf
		#endregion

		#region gstonepop
		If $gstonepop Then
			$pressed = _IsPressed($gstonepopHotkey, $hDLL)
			If (Not $pressedGstonepop) And $pressed Then
				$pressedGstonepop = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) Then
					If GetMapLoading() == $INSTANCETYPE_EXPLORABLE Then
						If Not UseItemByModelID($MODELID_GSTONE) Then
							WriteChat("[WARNING] Ghastly Summoning Stone not found!", $GWToolbox)
						EndIf
					EndIf
				EndIf
			ElseIf $pressedGstonepop And (Not $pressed) Then
				$pressedGstonepop = False
			EndIf
		EndIf
		#endregion

		#region legiopop
		If $legiopop Then
			$pressed = _IsPressed($legiopopHotkey, $hDLL)
			If (Not $pressedLegiopop) And $pressed Then
				$pressedLegiopop = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) Then
					If GetMapLoading() == $INSTANCETYPE_EXPLORABLE Then
						If Not UseItemByModelID($MODELID_LEGIONNAIRE_STONE) Then
							WriteChat("[WARNING] Legionnaire Summoning Crystal not found!", $GWToolbox)
						EndIf
					EndIf
				EndIf
			ElseIf $pressedLegiopop And (Not $pressed) Then
				$pressedLegiopop = False
			EndIf
		EndIf
		#endregion

		#region rainbowuse
		If $rainbowuse Then
			$pressed = _IsPressed($rainbowuseHotkey, $hDLL)
			If (Not $pressedRainbowuse) And $pressed Then
				$pressedRainbowuse = True
				If (WinActive($gwHWND) Or WinActive($mainGui) Or WinActive($dummyGui)) Then
					If GetMapLoading() == $INSTANCETYPE_EXPLORABLE Then

						Local $effectStructRed = GetEffect($EFFECT_REDROCK)
						If DllStructGetData($effectStructRed, "SkillID") == 0 Then
							If Not UseItemByModelID($ITEM_ID_RRC) Then
								WriteChat("[WARNING] Red Rock Candy not found!", $GWToolbox)
							EndIf
						EndIf

						Local $effectStructBlue = GetEffect($EFFECT_BLUEROCK)
						If DllStructGetData($effectStructBlue, "SkillID") == 0 Then
							If Not UseItemByModelID($ITEM_ID_BRC) Then
								WriteChat("[WARNING] Blue Rock Candy not found!", $GWToolbox)
							EndIf
						EndIf

						Local $effectStructGreen = GetEffect($EFFECT_GREENROCK)
						If DllStructGetData($effectStructGreen, "SkillID") == 0 Then
							If Not UseItemByModelID($ITEM_ID_GRC) Then
								WriteChat("[WARNING] Green Rock Candy not found!", $GWToolbox)
							EndIf
						EndIf

					EndIf
				EndIf
			ElseIf $pressedRainbowuse And (Not $pressed) Then
				$pressedRainbowuse = False
			EndIf
		EndIf
		#endregion

		#region DialogHK1
		If $DialogHK1 Then
			$pressed = _IsPressed($DialogHK1Hotkey, $hDLL)
			If (Not $pressedDialogHK1) And $pressed Then
				$pressedDialogHK1 = True
				Dialog($DIALOGS_ID[$DialogHK1Number])
			ElseIf $pressedDialogHK1 And (Not $pressed) Then
				$pressedDialogHK1 = False
			EndIf
		EndIf
		#endregion

		#region DialogHK2
		If $DialogHK2 Then
			$pressed = _IsPressed($DialogHK2Hotkey, $hDLL)
			If (Not $pressedDialogHK2) And $pressed Then
				$pressedDialogHK2 = True
				Dialog($DIALOGS_ID[$DialogHK2Number])
			ElseIf $pressedDialogHK2 And (Not $pressed) Then
				$pressedDialogHK2 = False
			EndIf
		EndIf
		#endregion

		#region DialogHK3
		If $DialogHK3 Then
			$pressed = _IsPressed($DialogHK3Hotkey, $hDLL)
			If (Not $pressedDialogHK3) And $pressed Then
				$pressedDialogHK3 = True
				Dialog($DIALOGS_ID[$DialogHK3Number])
			ElseIf $pressedDialogHK3 And (Not $pressed) Then
				$pressedDialogHK3 = False
			EndIf
		EndIf
		#endregion

		#region DialogHK3
		If $DialogHK4 Then
			$pressed = _IsPressed($DialogHK4Hotkey, $hDLL)
			If (Not $pressedDialogHK4) And $pressed Then
				$pressedDialogHK4 = True
				Dialog($DIALOGS_ID[$DialogHK4Number])
			ElseIf $pressedDialogHK4 And (Not $pressed) Then
				$pressedDialogHK4 = False
			EndIf
		EndIf
		#endregion

		#region pcons
		If $pconsHotkey Then
			$pressed = _IsPressed($pconsHotkeyHotkey, $hDLL)
			If (Not $pressedPcons) And $pressed Then
				$pressedPcons = True
				$pcons = Not $pcons
				GUICtrlSetData($pconsStatusLabel, $pcons ? "Active" : "Disabled")
				GUICtrlSetColor($pconsStatusLabel, $pcons ? $COLOR_GREEN : $COLOR_RED)
				WriteChat("Pcons are now " & ($pcons ? "Enabled" : "Disabled"), $GWToolbox)
			ElseIf $pressedPcons And (Not $pressed) Then
				$pressedPcons = False
			EndIf
		EndIf

		If $pcons Then
			If GetMapLoading()==$INSTANCETYPE_EXPLORABLE And (Not GetIsDead(-2)) And (DllStructGetData($lMe, "HP")>0) Then

				#region cons
				If $pconsConsActive And GetInstanceUptime() < (60*1000) Then
					If $pconsRetArray[$pconsConsEssence] < 1000 And $pconsRetArray[$pconsConsActive] < 1000 And $pconsRetArray[$pconsConsGrail] < 1000 Then
						If TimerDiff($timerCons) > 5000 Then				; if timer isnt bad
							Local $lSize = GetPartySize()
							If $lSize > 0 And $lParty[0] == $lSize Then ; if the whole party is in range (and loaded)
								Local $everybodyAliveAndLoaded = True
								For $i=1 To $lParty[0] Step 1
									If DllStructGetData($lParty[$i], "HP") <= 0 Then		; check if everyone is alive
										$everybodyAliveAndLoaded = False
										ExitLoop
									EndIf
								Next
								If $everybodyAliveAndLoaded Then
									If UseItemByModelID($ITEM_ID_CONS_ESSENCE) And UseItemByModelID($ITEM_ID_CONS_GRAIL) And UseItemByModelID($ITEM_ID_CONS_ARMOR) Then
										$timerCons = TimerInit()
									Else
										pconsScanInventory()
										If Not $pconsConsActive Then WriteChat("[WARNING] Cannot find cons", $GWToolbox)
									EndIf
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region essence
			    If $pconsEssenceActive And GetInstanceUptime() < (60*1000) Then
				   If $pconsRetArray[$pconsConsEssence] Then
					  If TimerDiff($timerEssence) > 5000 Then		; if timer isnt bad
						 Local $lSize = GetPartySize()
						 If $lSize > 0 And $lParty[0] == $lSize Then ; if the whole party is in range (and loaded)
							Local $everybodyAliveAndLoaded = True
							For $i=1 To $lParty[0] Step 1
							   If DllStructGetData($lParty[$i], "HP") <= 0 Then	; check if everyone is alive
								  $everybodyAliveAndLoaded = False
								  ExitLoop
							   EndIf
							Next
							If $everybodyAliveAndLoaded Then
							   If UseItemByModelID($ITEM_ID_CONS_ESSENCE) Then
								  $timerEssence = TimerInit()
							   Else
								  pconsScanInventory()
								  If Not $pconsEssenceActive Then WriteChat("[WARNING] Cannot find BU", $GWToolbox)
							   EndIf
							EndIf
						 EndIf
					  EndIf
				   EndIf
			    EndIf
				#endregion
				#region morale
				If $pconsMoraleActive Then
				   While GetMorale() < 10
					  If TimerDiff($timerMorale) > 250 Then
						 If UseItemByModelID($ITEM_ID_PUMPKIN_COOKIE) Then
							$timerMorale = TimerInit()
						 Else
						    pconsScanInventory()
							If Not $pconsMoraleActive Then WriteChat("[WARNING] Cannot find Pumpkin Cookie", $GWToolbox)
					     EndIf
					  EndIf
				   WEnd
				EndIf
				#endregion
				#region alcohol
				If $pconsAlcoholActive Then
					If GetAlcoholTimeRemaining() <= 60 Then
						If TimerDiff($timerAlcohol) > 5000 Then
							$AlcoholUsageCount += UseAlcohol()
							If GetAlcoholTimeRemaining() > 60 Then
								$timerAlcohol = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsAlcoholActive Then WriteChat("[WARNING] Cannot find Alcohol", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region Redrocks
				If $pconsRRCActive Then
					If $pconsRetArray[$pconsRedrock] < 1000 Then
						If TimerDiff($timerRRC) > 5000 Then
							If UseItemByModelID($ITEM_ID_RRC) Then
								$timerRRC = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsRRCActive Then WriteChat("[WARNING] Cannot find Red Rocks", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region Bluerock
				If $pconsBRCActive Then
					If $pconsRetArray[$pconsBluerock] < 1000 Then
						If TimerDiff($timerBRC) > 5000 Then
							If UseItemByModelID($ITEM_ID_BRC) Then
								$timerBRC = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsBRCActive Then WriteChat("[WARNING] Cannot find Blue Rocks", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region Greenrock
				If $pconsGRCActive Then
					If $pconsRetArray[$pconsGreenrock] < 1000 Then
						If TimerDiff($timerGRC) > 5000 Then
							If UseItemByModelID($ITEM_ID_GRC) Then
								$timerGRC = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsGRCActive Then WriteChat("[WARNING] Cannot find Green Rocks", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region pie
				If $pconsPieActive Then
					If $pconsRetArray[$pconsPie] < 1000 Then
						If TimerDiff($timerPie) > 5000 Then
							If UseItemByModelID($ITEM_ID_PIES) Then
								$timerPie = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsPieActive Then WriteChat("[WARNING] Cannot find Pumpkin Pies", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region cupcakes
				If $pconsCupcakeActive Then
					If $pconsRetArray[$pconsCupcake] < 1000 Then
						If TimerDiff($timerCupcake) > 5000 Then
							If UseItemByModelID($ITEM_ID_CUPCAKES) Then
								$timerCupcake = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsCupcakeActive Then WriteChat("[WARNING] Cannot find Cupcakes", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region apples
				If $pconsAppleActive Then
					If $pconsRetArray[$pconsApple] < 1000 Then
						If TimerDiff($timerApple) > 5000 Then
							If UseItemByModelID($ITEM_ID_APPLES) Then
								$timerApple = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsAppleActive Then WriteChat("[WARNING] Cannot find Candy Apples", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region corn
				If $pconsCornActive Then
					If $pconsRetArray[$pconsCorn] < 1000 Then
						If TimerDiff($timerCorn) > 5000 Then
							If UseItemByModelID($ITEM_ID_CORNS) Then
								$timerCorn = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsCornActive Then WriteChat("[WARNING] Cannot find Candy Corns", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region egg
				If $pconsEggActive Then
					If $pconsRetArray[$pconsEgg] < 1000 Then
						If TimerDiff($timerEgg) > 5000 Then
							If UseItemByModelID($ITEM_ID_EGGS) Then
								$timerEgg = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsEggActive Then WriteChat("[WARNING] Cannot find Golden Eggs", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region kabobs
				If $pconsKabobActive Then
					If $pconsRetArray[$pconsKabob] < 1000 Then
						If TimerDiff($timerKabob) > 5000 Then
							If UseItemByModelID($ITEM_ID_KABOBS) Then
								$timerKabob = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsKabobActive Then WriteChat("[WARNING] Cannot find Drake Kabobs", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region warsupplies
				If $pconsWarSupplyActive Then
					If $pconsRetArray[$pconsWarSupply] < 1000 Then
						If TimerDiff($timerWarsupply) > 5000 Then
							If UseItemByModelID($ITEM_ID_WARSUPPLIES) Then
								$timerWarsupply = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsWarSupplyActive Then WriteChat("[WARNING] Cannot find War Supplies", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region lunars
				If $pconsLunarsActive Then
					If $pconsRetArray[$pconsLunars] == 0 Then
						If TimerDiff($timerLunars) > GetPing()+500 Then
							If UseItemByModelID($ITEM_ID_LUNARS_DRAGON) Then
								$timerLunars = TimerInit()
							ElseIf UseItemByModelID($ITEM_ID_LUNARS_SNAKE) Then
								$timerLunars = TimerInit()
							ElseIf UseItemByModelID($ITEM_ID_LUNARS_HORSE) Then
								$timerLunars = TimerInit()
							ElseIf UseItemByModelID($ITEM_ID_LUNARS_RABBIT) Then
								$timerLunars = TimerInit()
							ElseIf UseItemByModelID($ITEM_ID_LUNARS_SHEEP) Then
								$timerLunars = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsLunarsActive Then WriteChat("[WARNING] Cannot find Lunars Fortunes", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region skalesoup
				If $pconsSkaleSoupActive Then
					If $pconsRetArray[$pconsSkaleSoup] < 1000 Then
						If TimerDiff($timerSkaleSoup) > 5000 Then
							If UseItemByModelID($ITEM_ID_SKALEFIN_SOUP) Then
								$timerSkaleSoup = TimerInit()
							Else
								pconsScanInventory()
								If Not $pconsSkaleSoupActive Then WriteChat("[WARNING] Cannot find Skalefin Soup", $GWToolbox)
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
				#region Mobstoppers
				If $pconsMobstoppersActive Then
					If DllStructGetData($lTgt, "PlayerNumber") == $MODELID_SKELETON_OF_DHUUM Then
						If DllStructGetData($lTgt, "HP") > 0 And DllStructGetData($lTgt, "HP") < 0.25 Then
							If GetDistance($lMe, $lTgt) < 400 Then
								If $pconsRetArray[$pconsMobstoppers] == 0 Then
									If TimerDiff($timerMobstoppers) > 5000 Then
										If UseItemByModelID($ITEM_ID_MOBSTOPPER) Then
											$timerMobstoppers = TimerInit()
										Else
											pconsScanInventory()
											If Not $pconsMobstoppersActive Then WriteChat("[WARNING] Cannot find Mobstoppers", $GWToolbox)
										EndIf
									EndIf
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
			EndIf
			If GetMapLoading()==$INSTANCETYPE_OUTPOST Then
				#region City Speedboost
				If $pconsCityActive Then
					If DllStructGetData($lMe, "MoveX") > 0 Or DllStructGetData($lMe, "MoveX") Then

						Local $lShouldUse = True
						For $i=1 To $pconsCityRetArray[0]
							If $pconsCityRetArray[$i] > 1000 Then $lShouldUse = False
						Next

						If $lShouldUse Then
							If TimerDiff($timerCity) > 5000 Then

								Local $lUsed = False
								For $i=1 To $pconsCityModels[0]
									If UseItemByModelID($pconsCityModels[$i]) Then
										$lUsed = True
										$timerCity = TimerInit()
										ExitLoop
									EndIf
								Next

								If Not $lUsed Then
									pconsScanInventory()
									If Not $pconsCityActive Then WriteChat("[WARNING] Cannot find city speedboosts", $GWToolbox)
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
				#endregion
			EndIf
		EndIf
		#endregion pcons

		#region rupt
		If $rupt Then
			$pressed = _IsPressed($ruptHotkey, $hDLL)
			If (Not $pressedRupt) And $pressed Then
				$pressedRupt = True
				$ruptActive = Not $ruptActive
				If $ruptActive Then
					WriteChat("Rupt macro is now active", $GWToolbox)
				Else
					WriteChat("Rupt macro is now inactive", $GWToolbox)
				EndIf
			ElseIf $pressedRupt And (Not $pressed) Then
				$pressedRupt = False
			EndIf
		EndIf

		If $ruptActive And GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then
			Local $skill = DllStructGetData($lTgt, "Skill")
			If $skill == $ruptSkill And (DllStructGetData(GetSkillBar(), "Recharge" & $ruptSlot)==0) Then
				UseSkill($ruptSlot, -1)
				pingSleep()
			EndIf
		EndIf
		#endregion rupt

		#region movement
		If $movement Then
			$pressed = _IsPressed($movementHotkey, $hDLL)
			If (Not $pressedMovement) And $pressed Then
				$pressedMovement = True
				Move($movementXcoord, $movementYcoord, 5)
				WriteChat("Movement macro activated", $GWToolbox)
			ElseIf $pressedMovement And (Not $pressed) Then
				$pressedMovement = False
			EndIf
		EndIf
		#endregion movement

		#region autoupdate
		If $hDownload <> 0 Then
			If InetGetInfo($hDownload, 2) Then
				If @WorkingDir <> @ScriptDir Then FileChangeDir(@ScriptDir)
				Local $hFile = FileOpen($tmpFullPath)
				Local $serverVersion = FileRead($hFile)
				If $currentVersion <> $serverVersion And $serverVersion <> "" And $serverVersion <> 0 Then
					Local $changeLog = BinaryToString(InetRead($Host&"changelog.txt", 1))
					Local $height = Int(StringLeft($changeLog, 4))
					Local $msg = StringRight($changeLog, StringLen($changeLog)-4)
					If MyGuiMsgBox(4,  "GWToolbox - update?", "A new version ("&$serverVersion&") is available, would you like to download it?" & @CRLF & @CRLF & @CRLF & _
														"Changelog:" & @CRLF & $msg, $mainGui, 350, IsInt($height) ? $height : 200) == 6 Then
						; get the new executable
						InetGet($Host&"GWToolbox.exe", @ScriptDir&"/tmp.exe", 1)
						; make batch file
						Local $hBat = FileOpen(@ScriptDir&"/update.bat", 2)
						If $hBat = -1 Then
							MyGuiMsgBox(0, "GWToolbox", "Error while updating, please manually download the new version")
							exitProgram()
						EndIf
						; 	:begin
;~ 						;	del "@scriptfullpath"
;~ 						;	If exist "@scriptfullpath" goto begin
;~ 						;	copy "@scriptdir\tmp.exe" "@scriptdir\GWToolbox.exe"
;~ 						;	del "@scriptdir\tmp.exe"
;~ 						;	start "Windows Command Prompt" "@scriptdir\GWToolbox.exe"
;~ 						;	del "@scriptdir\update.bat"
;~ 						FileWrite($hBat, _
;~ 							":begin" & @CRLF & _
;~ 							'del "' & @ScriptFullPath & '"' & @CRLF & _
;~ 							'If exist "' & @ScriptFullPath & '" goto begin' & @CRLF & _
;~ 							'copy "'&@ScriptDir&'\tmp.exe" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& _
;~ 							'del "' & @ScriptDir & '\tmp.exe"' & @CRLF & _
;~ 							'start "Windows Command Prompt" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& _
;~ 							'del "' & @ScriptDir & '\update.bat"')
						FileWrite($hBat, ":begin" & @CRLF & 'del "' & @ScriptFullPath & '"' & @CRLF & 'If exist "' & @ScriptFullPath & '" goto begin' & @CRLF & 'copy "'&@ScriptDir&'\tmp.exe" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& 'del "' & @ScriptDir & '\tmp.exe"' & @CRLF & 'start "Windows Command Prompt" "'&@ScriptDir&'\GWToolbox.exe"'&@CRLF& 'del "' & @ScriptDir & '\update.bat"')
						FileClose($hBat)
						FileClose($hFile)
						FileDelete($tmpFullPath)
						InetClose($hDownload)
						Sleep(50)
						Run(@ScriptDir&"\update.bat", @ScriptDir)
						exitProgram()
					EndIf
				EndIf
				FileClose($hFile)
				FileDelete($tmpFullPath)
				InetClose($hDownload)
				$hDownload = 0
			EndIf
		EndIf
		#endregion autoupdate

		If GetAlcoholTimeRemaining() < 0 Then
			$AlcoholUsageTimer = TimerInit()
			$AlcoholUsageCount = 0
		EndIf

		Sleep(5)
	WEnd
EndFunc   ;==>mainLoop

#region sendStuff
Func SkillsMacro()
	If Not GetMapLoading()==$INSTANCETYPE_EXPLORABLE Then Return
	Local $lMe = GetAgentByID(-2)
	If BitAND(DllStructGetData($lMe, 'Effects'), 0x0010) > 0 Then Return	; GetIsDead(-2)
	If DllStructGetData($lMe, 'Skill') <> 0 Then Return ; GetIsCasting(-2)
	Local $lSKillBar = GetSkillbar(0)
	If GUICtrlRead($sfmacroEmoMode) == $GUI_CHECKED Then
		Local $energyMissing = Int((1 - DllStructGetData($lMe, 'EnergyPercent')) * DllStructGetData($lMe, 'MaxEnergy'))
		If GetHeroProfession(0)==$PROF_ELEMENTALIST And $energyMissing > 10 Then
			Local $haveER = DllStructGetData(GetEffect($SKILL_ID_ETHER_RENEWAL), "SkillID") == $SKILL_ID_ETHER_RENEWAL
			Local $lSkillIDs[8]
			For $i=0 To 7
				$lSkillIDs[$i] = DllStructGetData($lSKillBar, "ID" & $i + 1)
			Next

			; Ether renewal
			For $i=0 To 7
				If $lSkillIDs[$i] == $SKILL_ID_ETHER_RENEWAL And DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
					UseSkill($i+1, -2)
					pingSleep()
					Return
				EndIf
			Next

			If $haveER Then
				; spirit bond
				For $i=0 To 7
					If $lSkillIDs[$i] == $SKILL_ID_SPIRIT_BOND And DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
						UseSkill($i+1, -2)
						pingSleep()
						Return
					EndIf
				Next

				; burning speed
				For $i=0 To 7
					If $lSkillIDs[$i] == $SKILL_ID_BURNING_SPEED And DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
						UseSkill($i+1, -2)
						pingSleep()
						Return
					EndIf
				Next
			EndIf
		EndIf
	Else
		For $i = 0 To 7 Step 1
			If $sfmacroSkillsToUse[$i] Then
				If DllStructGetData($lSKillBar, "Recharge" & $i + 1) == 0 Then
					useSkill($i + 1, -1)
					pingSleep()
					Return
				EndIf
			EndIf
		Next
	EndIf
EndFunc

Func sendRecall()
	If (Not (WinActive($gwHWND) Or WinActive($mainGui))) Or (Not GetMapLoading()==$INSTANCETYPE_EXPLORABLE) Then Return
	Local $hasRecall = GetIsTargetBuffed($SKILL_ID_RECALL, -2)
	If $hasRecall Then
		DropBuff($SKILL_ID_RECALL, -2)
	Else
		Local $skillBar = getSkillBar()
		Local $skillNo = getSkillPosition($SKILL_ID_RECALL, $skillBar)
		If $skillNo > 0 And DllStructGetData($skillBar, "Recharge" & $skillNo) == 0 Then
			useSkill($skillNo, -1)
		EndIf
	EndIf
EndFunc   ;==>sendRecall

Func sendUa()
	If (Not (WinActive($gwHWND) Or WinActive($mainGui))) Or (Not GetMapLoading()==$INSTANCETYPE_EXPLORABLE) Then Return
	Local $hasUA = GetIsTargetBuffed($SKILL_ID_UA, -2)
	If $hasUA Then
		DropBuff($SKILL_ID_UA, -2)
	Else
		Local $skillBar = getSkillBar()
		Local $skillNo = getSkillPosition($SKILL_ID_UA, $skillBar)
		If $skillNo > 0 And DllStructGetData($skillBar, "Recharge" & $skillNo) == 0 Then
			useSkill($skillNo, -1)
		EndIf
	EndIf
EndFunc   ;==>sendUa

Func sendToggleHide()
	WinSetState($gwHWND, "", @SW_HIDE)
	GUISetState(@SW_HIDE, $mainGui)
	GUISetState(@SW_HIDE, $dummyGui)
	Opt("TrayIconHide", False)
	Opt("GUIOnEventMode", False)

	Local $restore = TrayCreateItem("Restore")
	TrayCreateItem("")
	Local $close = TrayCreateItem("Close toolbox and guild wars")

	TraySetState()

	Local $hDLL = DllOpen("user32.dll")

	While 1
		Local $msg = TrayGetMsg()
		Local $pressed = _IsPressed($hidegwHotkey, $hDLL)
		If ($hidegw And $pressed) Or $msg = $restore Then
			If $pressed Then
				While _IsPressed($hidegwHotkey, $hDLL)
					Sleep(10)
				WEnd
			EndIf

			WinSetState($gwHWND, "", @SW_SHOW)
			GUISetState(@SW_SHOW, $mainGui)
			GUISetState(@SW_SHOW, $dummyGui)
			Opt("TrayIconHide", True)
			Opt("GUIOnEventMode", True)
			Return

		ElseIf $msg = $close Then
			WinClose($gwHWND)
			exitProgram()
		EndIf
	WEnd
EndFunc   ;==>sendToggleHide

Func SendLooter()
	If (Not (WinActive($gwHWND) Or WinActive($mainGui))) Or (Not GetMapLoading()==$INSTANCETYPE_EXPLORABLE) Then Return True

	Local $lMe = GetAgentByID(-2)
	Local $lItemArray[1] = [0]
	Local $lAgentArray = GetAgentArray(0x400)

	For $i=1 To $lAgentArray[0]
		If DllStructGetData($lAgentArray[$i], "Owner")==0 Or DllStructGetData($lAgentArray[$i], "Owner")==GetMyID() Then
			If GetDistance($lAgentArray[$i], $lMe) < $RANGE_AREA Then
				$lItemArray[0] += 1
				ReDim $lItemArray[$lItemArray[0]+1]
				$lItemArray[$lItemArray[0]] = $lAgentArray[$i]
			EndIf
		EndIf
	Next

	Local $lPseudoDistance, $lOtherPseudoDistance
	Local $lClosestItemIndex
	Local $lClosestItem
	Local $lItemID
	Local $lDeadlock
	While ($lItemArray[0] > 0)

;~ 		1. Choose the closest item
		$lMe = GetAgentByID(-2)
		$lPseudoDistance = $RANGE_AREA ^ 2
		For $i=1 To $lItemArray[0]
			$lOtherPseudoDistance = GetPseudoDistance($lItemArray[$i], $lMe)
			If $lOtherPseudoDistance < $lPseudoDistance Then
				$lPseudoDistance = $lOtherPseudoDistance
				$lClosestItemIndex = $i
			EndIf
		Next

;~ 		2. remove item from list
		$lClosestItem = __ArrayDelete($lItemArray, $lClosestItemIndex)

;~ 		2. Pick up the item
		$lItemID = DllStructGetData($lClosestItem, "ID")
		If GetAgentExists($lItemID) Then SendPacket(0xC, 0x38, $lItemID, 0)

;~ 		3. Wait until the item has been picked up.
		$lDeadlock = TimerInit()
		While GetAgentExists($lItemID)
			Sleep(50)
			If TimerDiff($lDeadlock) > 2500 Then Return
		WEnd
	WEnd
EndFunc

Func SendIdentifier()
	Local $lBag, $lItem

	For $lBagNo = 1 To 4
		$lBag = GetBag($lBagNo)
		For $i = 1 To DllStructGetData($lBag, 'Slots')
			$lItem = GetItemBySlot($lBagNo, $i)
			If DllStructGetData($lItem, 'ModelID') == 0 Then ContinueLoop
			If DllStructGetData($lItem, 'ID') == 0 Then ContinueLoop

			If FindIDKit() == 0 Then
				WriteChat("Could not find an Identification Kit", $GWToolbox)
				Return
			EndIf

			IdentifyItem($lItem)
			Sleep(GetPing())
		Next
		Sleep(50)
	Next
EndFunc

Func __ArrayDelete(ByRef $aArray, $iElement)
	If $iElement < 1 Then Return
	If $iElement > $aArray[0] Then Return
	Local $ret = $aArray[$iElement]
	For $i = $iElement To $aArray[0]-1
		$aArray[$i] = $aArray[$i+1]
	Next
	ReDim $aArray[$aArray[0]]
	$aArray[0] -= 1
	Return $ret
EndFunc
#endregion sendStuff

#region Ghosts
Func TargetGhost()
	If GetMapLoading() == $INSTANCETYPE_LOADING Then Return
	Local $lMe = GetAgentByID(-2)
	Local $lDistance = 10000000
	Local $lClosest = -1
	Local $lArr = GetAgentArray(0xDB)
	Local $lTmp
	For $i=1 To $lArr[0]
		If DllStructGetData($lArr[$i], 'PlayerNumber') == $MODELID_BOO And DllStructGetData($lArr[$i], 'HP') > 0 Then
			$lTmp = GetPseudoDistance($lMe, $lArr[$i])
			If $lTmp < $lDistance Then
				$lClosest = $i
				$lDistance = $lTmp
			EndIf
		EndIf
	Next
	If $lClosest > 0 Then ChangeTarget($lArr[$lClosest])
EndFunc
#endregion

#region builds
Func customTeambuild()
	GUISetState(@SW_DISABLE, $mainGui)
	Opt("GUIOnEventMode", False)

	; get from which build it is
	Local $buildsNo
	For $i = 0 To $buildsNumber-1
		If @GUI_CtrlId == $buildsEdit[$i] Then
			$buildsNo = $i+1
			ExitLoop
		EndIf
	Next

	; build the interface
	Local $teamBuildGui = GUICreate("Edit Team Build", 470, 508, Default, Default, $WS_POPUP, Default, $mainGui)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_WHITE)
		WinSetTrans($teamBuildGui, "", $Transparency)
	GUICtrlCreateLabel("Edit Team Build", 0, 0, 420, 40, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	GUICtrlCreateLabel("Build Name:", 36, 44)
	Local $buildName = 	GUICtrlCreateInput(IniRead($iniFullPath, "builds" & $buildsNo, "buildname", ""), 110, 42, 190, 21)

	GUICtrlCreateLabel("Names:", 50, 70, 110, 21)
	GUICtrlCreateLabel("Templates:", 190, 70, 170, 21)
	$rowY = 88
	Local $name[$buildsPartySize]
	Local $template[$buildsPartySize]
	Local $sendButton[$buildsPartySize]
	For $i = 0 To $buildsPartySize-1
		GUICtrlCreateLabel("#"&($i+1), 16, $rowY+2,  20, 21)
		$name[$i] = 	GUICtrlCreateInput(IniRead($iniFullPath, "builds" & $buildsNo, "name"&($i+1),     ""),  45, $rowY, 130, 21)
		$template[$i] =	GUICtrlCreateInput(IniRead($iniFullPath, "builds" & $buildsNo, "template"&($i+1), ""), 185, $rowY, 220, 21)
		$sendButton[$i] = MyGuiCtrlCreateButton("Send", 410, $rowY+1, 40, 19)

		$rowY += 32
	Next

	Local $showNumbers = GUICtrlCreateCheckbox("Show Build Numbers", 16, 468, 150, 25)
		If (IniRead($iniFullPath, "builds" & $buildsNo, "showNumbers", "True")=="True") Then GUICtrlSetState($showNumbers, $GUI_CHECKED)
	Local $cancelButton = MyGuiCtrlCreateButton("Cancel", 185, 468, 100, 25)
	Local $okButton = MyGuiCtrlCreateButton("Ok", 305, 468, 100, 25)

	Local $iENTER = GUICtrlCreateDummy()
	Local $iESC = GUICtrlCreateDummy()
	Local $AccelKeys[2][2] = [["{ENTER}", $iENTER], ["{ESC}", $iESC]]; Set accelerators
	GUISetAccelerators($AccelKeys)

	GUISetState(@SW_SHOW)

	While 1
		Local $msg = GUIGetMsg()

		For $i = 0 To $buildsPartySize-1
			If $msg == $sendButton[$i] Then
				sendBuild(GUICtrlRead($name[$i]), GUICtrlRead($template[$i]), $i+1, GUICtrlRead($showNumbers)==$GUI_CHECKED)
			EndIf
		Next

		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $cancelButton, $iESC
				ExitLoop

			Case $okButton, $iENTER
				IniWrite($iniFullPath, "builds" & $buildsNo, "buildname", GUICtrlRead($buildName))

				For $i = 0 To $buildsPartySize-1
					IniWrite($iniFullPath, "builds" & $buildsNo, "name"&($i+1), GUICtrlRead($name[$i]))
					IniWrite($iniFullPath, "builds" & $buildsNo, "template"&($i+1), GUICtrlRead($template[$i]))
				Next
				IniWrite($iniFullPath, "builds" & $buildsNo, "showNumbers", GUICtrlRead($showNumbers)==$GUI_CHECKED)

				GUICtrlSetData($buildsLabel[$buildsNo-1], GUICtrlRead($buildName))
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($teamBuildGui)
	GUISetState(@SW_ENABLE, $mainGui)
	WinActivate($mainGui)
	Opt("GUIOnEventMode", True)
EndFunc   ;==>customTeambuild

; interface button callback
Func sendTeamBuilds()
	For $i = 0 To $buildsNumber
		If @GUI_CtrlId == $buildsLabel[$i] Then Return sendTeamBuild($i+1)
	Next
EndFunc   ;==>sendBuilds

; send a specific teambuild
Func sendTeamBuild($buildsNo, $chat = "#", $delay = 600)
	Local $lName = IniRead($iniFullPath, "builds" & $buildsNo, "buildname", "")
	Local $lShowNumbers = IniRead($iniFullPath, "builds" & $buildsNo, "showNumbers", True)=="True"
	Local $lTemplate
	If ($lName <> "") Then SendChat($lName, $chat)
	Sleep($delay)
	For $i = 1 To $buildsPartySize
		sendBuildIni($buildsNo, $i, $lShowNumbers)
	Next
EndFunc   ;==>sendBuild

; send a specific build (read from ini)
Func sendBuildIni($buildsNo, $partyMember, $showNumbers = True, $chat = "#", $delay = 600)
	Local $lName = IniRead($iniFullPath, "builds" & $buildsNo, "name" & $partyMember, "")
	Local $lTemplate = IniRead($iniFullPath, "builds" & $buildsNo, "template" & $partyMember, "")
	sendBuild($lName, $lTemplate, $partyMember, $showNumbers, $chat, $delay)
EndFunc

; Send a specific build (given)
Func sendBuild($name, $template, $partyMember = 0, $showNumbers = False, $chat = "#", $delay = 600)
	If ($name <> "") Or ($template <> "") Then
		SendChat("[" & ($showNumbers ? ($partyMember & " - ") : "") & $name & ";" & $template & "]", $chat)
		Sleep($delay)
	EndIf
EndFunc
#endregion builds

#region timer
Func toggleTimer()
	$timer = Not $timer
	If $timer Then
		Local $x = IniRead($iniFullPath, $s_timer, "x", -1)
		Local $y = IniRead($iniFullPath, $s_timer, "y", -1)
		$timerGui = GUICreate("", 150, 40, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($timerGui, 10)
		$timerGuiLabel = GUICtrlCreateLabel("", 0, 0, 150, 40, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont($timerGuiLabel, 26)
		GUICtrlSetColor($timerGuiLabel, $COLOR_TIMER)
		GUISetBkColor($COLOR_BLACK)
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "timerMoved")
		WinSetTrans($timerGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($timerGui)
	EndIf
	IniWrite($iniFullPath, $s_timer, "Active", $timer)
EndFunc

Func timerMoved()
	If $timer Then
		Local $pos = WinGetPos($timerGui)
		IniWrite($iniFullPath, $s_timer, "x", $pos[0])
		IniWrite($iniFullPath, $s_timer, "y", $pos[1])
	EndIf
EndFunc
#endregion timer

#region health
Func toggleHealth()
	$health = Not $health
	If $health Then
		Local $x = IniRead($iniFullPath, $s_health, "x", -1)
		Local $y = IniRead($iniFullPath, $s_health, "y", -1)
		$healthGui = GUICreate("", 105, 60, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($healthGui, 10)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_HEALTH_HIGHT)
		$healthGuiLabelHP = 	GUICtrlCreateLabel("", 3, 	0, 	85, 40, $SS_RIGHT, $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 26, 500)
		$healthGuiLabelPerc = 	GUICtrlCreateLabel("%",90, 	0, 	15, 40, BitOR($SS_LEFT, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 12)
		$healthGuiLabelMaxHP = 	GUICtrlCreateLabel("", 3, 	40, 105,20, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 12)
		$healthGuiCurrentColor = $COLOR_HEALTH_HIGHT
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "healthMoved")
		WinSetTrans($healthGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($healthGui)
	EndIf
	IniWrite($iniFullPath, $s_health, "Active", $health)
EndFunc

Func healthMoved()
	If $health Then
		Local $pos = WinGetPos($healthGui)
		IniWrite($iniFullPath, $s_health, "x", $pos[0])
		IniWrite($iniFullPath, $s_health, "y", $pos[1])
	EndIf
EndFunc

Func healthUpdateColor($color)
	If $healthGuiCurrentColor <> $color Then
		GUICtrlSetColor($healthGuiLabelHP, $color)
		GUICtrlSetColor($healthGuiLabelMaxHP, $color)
		GUICtrlSetColor($healthGuiLabelPerc, $color)
		$healthGuiCurrentColor = $color
	EndIf
EndFunc
#endregion

#region distance
Func toggledistance()
	$distance = Not $distance
	If $distance Then
		Local $x = IniRead($iniFullPath, $s_distance, "x", -1)
		Local $y = IniRead($iniFullPath, $s_distance, "y", -1)
		$distanceGui = GUICreate("", 85, 50, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($distanceGui, 10)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_DISTANCE)
		$distanceGuiLabelDist = GUICtrlCreateLabel("", 0, 0, 80, 30, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 24)
		$distanceGuiLabelPerc = GUICtrlCreateLabel("%", 70, 0, 15, 30, BitOR($SS_LEFT, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 10)
		$distanceGuiLabelText = GUICtrlCreateLabel("", 0, 30, 85, 20, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetFont(-1, 10)
		$distanceGuiCurrentColor = $COLOR_DISTANCE
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "distanceMoved")
		WinSetTrans($distanceGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($distanceGui)
	EndIf
	IniWrite($iniFullPath, $s_distance, "Active", $distance)
EndFunc

Func distanceUpdateColor($color)
	If $distanceGuiCurrentColor <> $color Then
		GUICtrlSetColor($distanceGuiLabelDist, $color)
		GUICtrlSetColor($distanceGuiLabelPerc, $color)
		GUICtrlSetColor($distanceGuiLabelText, $color)
		$distanceGuiCurrentColor = $color
	EndIf
EndFunc

Func distanceMoved()
	If $distance Then
		Local $pos = WinGetPos($distanceGui)
		IniWrite($iniFullPath, $s_distance, "x", $pos[0])
		IniWrite($iniFullPath, $s_distance, "y", $pos[1])
	EndIf
EndFunc
#endregion distance

#region party
Func toggleParty()
	$party = Not $party
	If $party Then
		Local $lWidth = 30
		Local $lHeight = 22
		Local $x = IniRead($iniFullPath, $s_party, "x", -1)
		Local $y = IniRead($iniFullPath, $s_party, "y", -1)
		$partyGui = GUICreate("", $lWidth, $lHeight*8, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		_GuiRoundCorners($partyGui, 10)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_PARTY)
		For $i=1 To 8
			$partyLabels[$i] = GUICtrlCreateLabel("-", 0, ($i-1)*$lHeight, $lWidth, $lHeight, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
				GUICtrlSetFont(-1, 12)
		Next
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "partyMoved")
		WinSetTrans($partyGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($partyGui)
	EndIf
	IniWrite($iniFullPath, $s_party, "Active", $party)
EndFunc

Func partyMoved()
	If $party Then
		Local $pos = WinGetPos($partyGui)
		IniWrite($iniFullPath, $s_party, "x", $pos[0])
		IniWrite($iniFullPath, $s_party, "y", $pos[1])
	EndIf
EndFunc
#endregion

#region buffs
Func toggleBuffs()
	$buffs = Not $buffs
	If $buffs Then
		Local $lSize = 22
		Local $x = IniRead($iniFullPath, $s_buffs, "x", -1)
		Local $y = IniRead($iniFullPath, $s_buffs, "y", -1)
		$buffsGui = GUICreate("", $lSize*3, $lSize*8, $x, $y, $WS_POPUP, $WS_EX_TOPMOST, $dummyGui)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlCreateLabel("", 0, 0, $lSize*3, $lSize*8, Default, $GUI_WS_EX_PARENTDRAG)
			GUICtrlSetOnEvent(-1, "buffsClicked")
		For $i=1 To 8
			$buffsImages[$i][0] = GUICtrlCreatePic("", 0*$lSize, ($i-1)*$lSize, $lSize, $lSize, $SS_NOTIFY, $GUI_WS_EX_PARENTDRAG)
			$buffsImages[$i][1] = GUICtrlCreatePic("", 1*$lSize, ($i-1)*$lSize, $lSize, $lSize, $SS_NOTIFY, $GUI_WS_EX_PARENTDRAG)
			$buffsImages[$i][2] = GUICtrlCreatePic("", 2*$lSize, ($i-1)*$lSize, $lSize, $lSize, $SS_NOTIFY, $GUI_WS_EX_PARENTDRAG)
			If @Compiled Then
				_Resource_SetToCtrlID($buffsImages[$i][0], "BALTHSPIRIT_JPG")
				_Resource_SetToCtrlID($buffsImages[$i][1], "LIFEBOND_JPG")
				_Resource_SetToCtrlID($buffsImages[$i][2], "PROTBOND_JPG")
			Else
				GUICtrlSetImage($buffsImages[$i][0], "img/balthspirit.jpg")
				GUICtrlSetImage($buffsImages[$i][1], "img/lifebond.jpg")
				GUICtrlSetImage($buffsImages[$i][2], "img/protbond.jpg")
			EndIf
		Next
		For $i=1 To 8
			For $k=0 To 2
				$buffsImagesHidden[$i][$k] = False
				buffsHide($i, $k)
			Next
		Next
		GUISetOnEvent($GUI_EVENT_PRIMARYUP, "buffsMoved")
		WinSetTrans($buffsGui, "", $Transparency)
		GUISetState()
		WinActivate($mainGui)
	Else
		GUIDelete($buffsGui)
	EndIf
	IniWrite($iniFullPath, $s_buffs, "Active", $buffs)
EndFunc

Func buffsClicked()
	If Not $buffsDrop Then Return
	Local $mousePos = GUIGetCursorInfo($buffsGui)
	Local $lBuffNo = Int(Floor($mousePos[0] / 22))
	Local $lPlayer = Int(Floor($mousePos[1] / 22)) + 1
	If $buffsStatus[$lPlayer][$lBuffNo] == $BUFFS_VISIBLE Then
;~ 		WriteChat("dropping buff # "&$buffsID[$lPlayer][$lBuffNo])
		SendPacket(0x8, 0x22, $buffsID[$lPlayer][$lBuffNo])
		Sleep(10)
	EndIf
EndFunc

Func buffsShow($aPlayer, $aBuffNo)
	If $buffsImagesHidden[$aPlayer][$aBuffNo] Then
		GUICtrlSetState($buffsImages[$aPlayer][$aBuffNo], $GUI_SHOW)
		$buffsImagesHidden[$aPlayer][$aBuffNo] = False
	EndIf
	$buffsStatus[$aPlayer][$aBuffNo] = $BUFFS_VISIBLE
EndFunc

Func buffsHide($aPlayer, $aBuffNo)
	If Not $buffsImagesHidden[$aPlayer][$aBuffNo] Then
		GUICtrlSetState($buffsImages[$aPlayer][$aBuffNo], $GUI_HIDE)
		$buffsImagesHidden[$aPlayer][$aBuffNo] = True
	EndIf
	$buffsStatus[$aPlayer][$aBuffNo] = $BUFFS_HIDDEN
EndFunc

Func buffsMoved()
	If $buffs Then
		Local $pos = WinGetPos($buffsGui)
		IniWrite($iniFullPath, $s_buffs, "x", $pos[0])
		IniWrite($iniFullPath, $s_buffs, "y", $pos[1])
	EndIf
EndFunc
#endregion buffs

#region pcons
Func GetAlcoholTimeRemaining()
	Return Round(60*$AlcoholUsageCount - TimerDiff($AlcoholUsageTimer)/1000)
EndFunc

Func UseAlcohol()
	Local $lItem
	For $lBag=1 To 4
		For $lSlot=1 To $BAG_SLOTS[$lBag]
			$lItem = GetItemBySlot($lBag, $lSlot)
			For $i=1 To $ITEM_ID_ALCOHOL_1[0]
				If DllStructGetData($lItem, "ModelID") == $ITEM_ID_ALCOHOL_1[$i] Then
					SendPacket(0x8, 0x77, DllStructGetData($lItem, "ID"))
					Return 1
				EndIf
			Next
			For $i=1 To $ITEM_ID_ALCOHOL_5[0]
				If DllStructGetData($lItem, "ModelID") == $ITEM_ID_ALCOHOL_5[$i] Then
					SendPacket(0x8, 0x77, DllStructGetData($lItem, "ID"))
					Return 4
				EndIf
			Next
		Next
	Next
	Return 0
EndFunc

Func pconsToggle()
	Local $aState = GUICtrlRead(@GUI_CtrlId) == $GUI_CHECKED
	Switch @GUI_CtrlId
		Case $pconsConsCheckbox
			If $aState Then
				If pconsFind($ITEM_ID_CONS_ESSENCE) And pconsFind($ITEM_ID_CONS_ARMOR) And pconsFind($ITEM_ID_CONS_GRAIL) Then
					$pconsConsActive = True
					IniWrite($iniFullPath, $s_pcons, $s_cons, True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsConsActive = False
				IniWrite($iniFullPath, $s_pcons, $s_cons, False)
			 EndIf
	    Case $pconsEssenceCheckbox
			If $aState Then
			   If pconsFind($ITEM_ID_CONS_ESSENCE) Then
					$pconsEssenceActive = True
					IniWrite($iniFullPath, $s_pcons, "essence", True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
			   $pconsEssenceActive = False
				IniWrite($iniFullPath, $s_pcons, "essence", False)
			EndIf
		Case $pconsAlcoholCheckbox
			If $aState Then
				Local $lFound = False
				For $i=1 To $ITEM_ID_ALCOHOL_1[0]
					If pconsFind($ITEM_ID_ALCOHOL_1[$i]) Then
						$lFound = True
						ExitLoop
					EndIf
				Next
				If Not $lFound Then
					For $i=1 To $ITEM_ID_ALCOHOL_5[0]
						If pconsFind($ITEM_ID_ALCOHOL_5[$i]) Then
							$lFound = True
							ExitLoop
						EndIf
					Next
				EndIf
				If $lFound Then
					$pconsAlcoholActive = True
					IniWrite($iniFullPath, $s_pcons, $s_alcohol, True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsAlcoholActive = False
				IniWrite($iniFullPath, $s_pcons, $s_alcohol, False)
			EndIf
		Case $pconsRRCCheckbox
			$pconsRRCActive = pconsSet($ITEM_ID_RRC, $aState, @GUI_CtrlId, $s_RRC)
		Case $pconsBRCCheckbox
			$pconsBRCActive = pconsSet($ITEM_ID_BRC, $aState, @GUI_CtrlId, $s_BRC)
		Case $pconsGRCCheckbox
			$pconsGRCActive = pconsSet($ITEM_ID_GRC, $aState, @GUI_CtrlId, "GRC")
		Case $pconsPieCheckbox
			$pconsPieActive = pconsSet($ITEM_ID_PIES, $aState, @GUI_CtrlId, "pie")
		Case $pconsCupcakeCheckbox
			$pconsCupcakeActive = pconsSet($ITEM_ID_CUPCAKES, $aState, @GUI_CtrlId, "cupcake")
		Case $pconsAppleCheckbox
			$pconsAppleActive = pconsSet($ITEM_ID_APPLES, $aState, @GUI_CtrlId, "apple")
		Case $pconsCornCheckbox
			$pconsCornActive = pconsSet($ITEM_ID_CORNS, $aState, @GUI_CtrlId, "corn")
		Case $pconsEggCheckbox
			$pconsEggActive = pconsSet($ITEM_ID_EGGS, $aState, @GUI_CtrlId, "egg")
		Case $pconsKabobCheckbox
			$pconsKabobActive = pconsSet($ITEM_ID_KABOBS, $aState, @GUI_CtrlId, "kabob")
		Case $pconsWarSupplyCheckbox
			$pconsWarSupplyActive = pconsSet($ITEM_ID_WARSUPPLIES, $aState, @GUI_CtrlId, "warsupply")
		Case $pconsLunarsCheckbox
			If $aState Then
				If pconsFind($ITEM_ID_LUNARS_DRAGON) Or pconsFind($ITEM_ID_LUNARS_SNAKE) Or pconsFind($ITEM_ID_LUNARS_HORSE) Or pconsFind($ITEM_ID_LUNARS_RABBIT) Or pconsFind($ITEM_ID_LUNARS_SHEEP) Then
					$pconsLunarsActive = True
					IniWrite($iniFullPath, $s_pcons, "lunars", True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsLunarsActive = False
				IniWrite($iniFullPath, $s_pcons, "lunars", False)
			EndIf
		Case $pconsSkaleSoupCheckbox
			$pconsSkaleSoupActive = pconsSet($ITEM_ID_SKALEFIN_SOUP, $aState, @GUI_CtrlId, "skalefinsoup")
		Case $pconsMobstoppersCheckbox
			$pconsMobstoppersActive = pconsSet($ITEM_ID_MOBSTOPPER, $aState, @GUI_CtrlId, "mobstoppers")
	    Case $pconsMoraleCheckbox
			$pconsMoraleActive = pconsSet($ITEM_ID_PUMPKIN_COOKIE, $aState, @GUI_CtrlId, "morale")
		Case $pconsCityCheckbox
			If $aState Then
				Local $lFound = False
				For $i=1 To $pconsCityModels[0]
					If pconsFind($pconsCityModels[$i]) Then
						$lFound = True
						ExitLoop
					EndIf
				Next
				If $lFound Then
					$pconsCityActive = True
					IniWrite($iniFullPath, $s_pcons, "city", True)
				Else
					GUICtrlSetState(@GUI_CtrlId, $GUI_UNCHECKED)
				EndIf
			Else
				$pconsCityActive = False
				IniWrite($iniFullPath, $s_pcons, "city", False)
			EndIf
	EndSwitch
EndFunc

Func pconsSet($aModelID, $aState, $aGuiCtrlID, $aIniKey)
	If $aState Then
		If pconsFind($aModelID) Then
			IniWrite($iniFullPath, $s_pcons, $aIniKey, True)
			If GUICtrlRead($aGuiCtrlID) == $GUI_UNCHECKED Then GUICtrlSetState($aGuiCtrlID, $GUI_CHECKED)
			Return True
		Else
			If GUICtrlRead($aGuiCtrlID) == $GUI_CHECKED Then GUICtrlSetState($aGuiCtrlID, $GUI_UNCHECKED)
			Return False
		EndIf
	Else
		IniWrite($iniFullPath, $s_pcons, $aIniKey, False)
		If GUICtrlRead($aGuiCtrlID) == $GUI_CHECKED Then GUICtrlSetState($aGuiCtrlID, $GUI_UNCHECKED)
		Return False
	EndIf
EndFunc

Func pconsFind($aModelID)
	For $lBag=1 To 4
		For $lSlot=1 To $BAG_SLOTS[$lBag]
			If DllStructGetData(GetItemBySlot($lBag, $lSlot), "ModelID") == $aModelID Then
				Return True
			EndIf
		Next
	Next
	Return False
EndFunc

Func pconsScanInventory()
	Local $lAlcohol=0, $lConsEssence=0, $lConsGrail=0, $lConsArmor=0, $lRedrock=0, $lBluerock=0, $lGreenrock=0
	Local $lPies=0, $lCupcakes=0, $lApples=0, $lCorns=0, $lEggs=0, $lKabobs=0, $lWarsupplies=0, $lLunars=0, $lResscrolls=0
	Local $lSkalesoup=0, $lMobstoppers=0, $lPahnai=0, $lCity=0, $lMorale=0
	Local $lItem, $lQuantity
	For $lBag=1 To 4 Step 1
		For $lSlot = 1 To $BAG_SLOTS[$lBag]
			$lItem = GetItemBySlot($lBag, $lSlot)
			$lQuantity = DllStructGetData($lItem, "Quantity")
			Switch DllStructGetData($lItem, "ModelID")
				Case 0
					ContinueLoop
				Case $ITEM_ID_CONS_ESSENCE
					$lConsEssence += $lQuantity
				Case $ITEM_ID_CONS_GRAIL
					$lConsGrail += $lQuantity
				Case $ITEM_ID_CONS_ARMOR
					$lConsArmor += $lQuantity
				Case $ITEM_ID_ALCOHOL_1[1], $ITEM_ID_ALCOHOL_1[2], $ITEM_ID_ALCOHOL_1[3], $ITEM_ID_ALCOHOL_1[4], $ITEM_ID_ALCOHOL_1[5], $ITEM_ID_ALCOHOL_1[6], $ITEM_ID_ALCOHOL_1[7], $ITEM_ID_ALCOHOL_1[8]
					$lAlcohol += $lQuantity
				Case $ITEM_ID_ALCOHOL_5[1], $ITEM_ID_ALCOHOL_5[2], $ITEM_ID_ALCOHOL_5[3], $ITEM_ID_ALCOHOL_5[4], $ITEM_ID_ALCOHOL_5[5], $ITEM_ID_ALCOHOL_5[6], $ITEM_ID_ALCOHOL_5[7]
					$lAlcohol += 5*$lQuantity
				Case $ITEM_ID_RRC
					$lRedrock += $lQuantity
				Case $ITEM_ID_BRC
					$lBluerock += $lQuantity
				Case $ITEM_ID_GRC
					$lGreenrock += $lQuantity
				Case $ITEM_ID_PIES
					$lPies += $lQuantity
				Case $ITEM_ID_CUPCAKES
					$lCupcakes += $lQuantity
				Case $ITEM_ID_APPLES
					$lApples += $lQuantity
				Case $ITEM_ID_CORNS
					$lCorns += $lQuantity
				Case $ITEM_ID_EGGS
					$lEggs += $lQuantity
				Case $ITEM_ID_KABOBS
					$lKabobs += $lQuantity
				Case $ITEM_ID_WARSUPPLIES
					$lWarsupplies += $lQuantity
				Case $ITEM_ID_LUNARS_DRAGON, $ITEM_ID_LUNARS_SNAKE, $ITEM_ID_LUNARS_HORSE, $ITEM_ID_LUNARS_RABBIT, $ITEM_ID_LUNARS_SHEEP
					$lLunars += $lQuantity
				Case $ITEM_ID_SKALEFIN_SOUP
					$lSkalesoup += $lQuantity
				Case $ITEM_ID_MOBSTOPPER
					$lMobstoppers += $lQuantity
			    Case $ITEM_ID_PUMPKIN_COOKIE
				    $lMorale += $lQuantity
				Case $pconsCityModels[1], $pconsCityModels[2], $pconsCityModels[3], $pconsCityModels[4], $pconsCityModels[5]
					$lCity += $lQuantity
			EndSwitch
		Next
	Next
	Local $lCons = _Min($lConsEssence, _Min($lConsArmor, $lConsGrail))
	$pconsConsActive = $pconsConsActive And $lCons > 0
	$pconsEssenceActive = $pconsEssenceActive And $lConsEssence > 0
	$pconsRRCActive = $pconsRRCActive And $lRedrock > 0
	$pconsBRCActive = $pconsBRCActive And $lBluerock > 0
	$pconsGRCActive = $pconsGRCActive And $lGreenrock > 0
	$pconsAlcoholActive = $pconsAlcoholActive And $lAlcohol > 0
	$pconsPieActive = $pconsPieActive And $lPies > 0
	$pconsCupcakeActive = $pconsCupcakeActive And $lCupcakes > 0
	$pconsAppleActive = $pconsAppleActive And $lApples > 0
	$pconsCornActive = $pconsCornActive And $lCorns > 0
	$pconsEggActive = $pconsEggActive And $lEggs > 0
	$pconsKabobActive = $pconsKabobActive And $lKabobs > 0
	$pconsWarSupplyActive = $pconsWarSupplyActive And $lWarsupplies > 0
	$pconsLunarsActive = $pconsLunarsActive And $lLunars > 0
	$pconsSkaleSoupActive = $pconsSkaleSoupActive And $lSkalesoup > 0
	$pconsMobstoppersActive = $pconsMobstoppersActive And $lMobstoppers > 0
	$pconsMoraleActive = $pconsMoraleActive And $lMorale > 0
	$pconsCityActive = $pconsCityActive And $lCity > 0

	GUICtrlSetState($pconsConsCheckbox, $pconsConsActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsEssenceCheckbox, $pconsEssenceActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsRRCCheckbox, $pconsRRCActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsBRCCheckbox, $pconsBRCActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsGRCCheckbox, $pconsGRCActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsAlcoholCheckbox, $pconsAlcoholActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsPieCheckbox, $pconsPieActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsCupcakeCheckbox, $pconsCupcakeActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsAppleCheckbox, $pconsAppleActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsCornCheckbox, $pconsCornActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsEggCheckbox, $pconsEggActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsKabobCheckbox, $pconsKabobActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsWarSupplyCheckbox, $pconsWarSupplyActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsLunarsCheckbox, $pconsLunarsActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsSkaleSoupCheckbox, $pconsSkaleSoupActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsMobstoppersCheckbox, $pconsMobstoppersActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsMoraleCheckbox, $pconsMoraleActive ? $GUI_CHECKED : $GUI_UNCHECKED)
	GUICtrlSetState($pconsCityCheckbox, $pconsCityActive ? $GUI_CHECKED : $GUI_UNCHECKED)

	GUICtrlSetColor($pconsConsCheckbox, 	$lCons 			> 0 ? ($lCons > 5 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsEssenceCheckbox, 	$lConsEssence	> 0 ? ($lConsEssence > 5 ?	$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsRRCCheckbox, 		$lRedrock 		> 0 ? ($lRedrock > 5 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsBRCCheckbox, 		$lBluerock 		> 0 ? ($lBluerock > 10 ? 	$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsGRCCheckbox, 		$lGreenrock 	> 0 ? ($lGreenrock > 15 ? 	$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsAlcoholCheckbox, 	$lAlcohol 		> 0 ? ($lAlcohol > 30 ? 	$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsPieCheckbox,		$lPies 			> 0 ? ($lPies > 10 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsCupcakeCheckbox, 	$lCupcakes 		> 0 ? ($lCupcakes > 10 ? 	$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsAppleCheckbox, 	$lApples 		> 0 ? ($lApples > 10 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsCornCheckbox, 	$lCorns 		> 0 ? ($lCorns > 10 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsEggCheckbox, 		$lEggs 			> 0 ? ($lEggs > 20 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsKabobCheckbox, 	$lKabobs 		> 0 ? ($lKabobs > 10 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsWarSupplyCheckbox,$lWarsupplies 	> 0 ? ($lWarsupplies > 20 ? $COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsLunarsCheckbox, 	$lLunars 		> 0 ? ($lLunars > 30 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsSkaleSoupCheckbox,$lSkalesoup 	> 0 ? ($lSkalesoup > 10 ? 	$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsMobstoppersCheckbox,$lMobstoppers > 0 ? ($lMobstoppers > 5 ? 	$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsMoraleCheckbox, 	$lMorale 		> 0 ? ($lPahnai > 10 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)
	GUICtrlSetColor($pconsCityCheckbox, 	$lCity 			> 0 ? ($lCity > 30 ? 		$COLOR_GREEN : $COLOR_YELLOW) : $COLOR_RED)

	GUICtrlSetData($pconsConsCheckbox, "("&$lCons&") Conset")
	GUICtrlSetData($pconsEssenceCheckbox, "("&$lConsEssence&") BU")
	GUICtrlSetData($pconsRRCCheckbox, "("&$lRedrock&") Red Rock Candy")
	GUICtrlSetData($pconsBRCCheckbox, "("&$lBluerock&") Blue Rock Candy")
	GUICtrlSetData($pconsGRCCheckbox, "("&$lGreenrock&") Green Rock Candy")
	GUICtrlSetData($pconsAlcoholCheckbox, "("&$lAlcohol&"min) Alcohol")
	GUICtrlSetData($pconsPieCheckbox, "("&$lPies&") Slice of Pumpkin Pie")
	GUICtrlSetData($pconsCupcakeCheckbox, "("&$lCupcakes&") Birthday Cupcake")
	GUICtrlSetData($pconsAppleCheckbox, "("&$lApples&") Candy Apple")
	GUICtrlSetData($pconsCornCheckbox, "("&$lCorns&") Candy Corn")
	GUICtrlSetData($pconsEggCheckbox, "("&$lEggs&") Golden Egg")
	GUICtrlSetData($pconsKabobCheckbox, "("&$lKabobs&") Drake Kabob")
	GUICtrlSetData($pconsWarSupplyCheckbox, "("&$lWarsupplies&") War Supplies")
	GUICtrlSetData($pconsLunarsCheckbox, "("&$lLunars&") Lunar Fortune")
	GUICtrlSetData($pconsSkaleSoupCheckbox, "("&$lSkalesoup&") Skalefin Soup")
	GUICtrlSetData($pconsMobstoppersCheckbox, "("&$lMobstoppers&") Mobstoppers")
	GUICtrlSetData($pconsMoraleCheckbox, "("&$lMorale&") Pumpkin Cookies")
	GUICtrlSetData($pconsCityCheckbox, "("&$lCity&") City Speedboosts")
EndFunc

Func presetLoad()
	Local $lPreset = IniRead($iniFullPath, "pconsPresets", GUICtrlRead(@GUI_CtrlId), 18*"0")
	Local $lArr = StringSplit($lPreset, "")
	If $lArr[0] <> $pconsEffects[0]+1 Then
		WriteChat("Error loading template. Unfortunately presets made before the update will not work.", $GWToolbox)
		Return
	EndIf

	$pconsConsActive = $lArr[1] == "1"
	$pconsRRCActive = $lArr[2] == "1"
	$pconsBRCActive = $lArr[3] == "1"
	$pconsGRCActive = $lArr[4] == "1"
	$pconsAlcoholActive = $lArr[5] == "1"
	$pconsPieActive = $lArr[6] == "1"
	$pconsCupcakeActive = $lArr[7] == "1"
	$pconsSkaleSoupActive = $lArr[8] == "1"
	$pconsAppleActive = $lArr[9] == "1"
	$pconsCornActive = $lArr[10] == "1"
	$pconsEggActive = $lArr[11] == "1"
	$pconsKabobActive = $lArr[12] == "1"
	$pconsWarSupplyActive = $lArr[13] == "1"
	$pconsLunarsActive = $lArr[14] == "1"
	$pconsEssenceActive = $lArr[15] == "1"
	$pconsMobstoppersActive = $lArr[16] == "1"
	$pconsMoraleActive = $lArr[17] = "1"
	$pconsCityActive = $lArr[18] = "1"

	pconsScanInventory()
EndFunc

Func presetSave()
	Local $lName = ""
	Local $lString = ""
	$lString &= $pconsConsActive ? "1" : "0"
	$lString &= $pconsRRCActive ? "1" : "0"
	$lString &= $pconsBRCActive ? "1" : "0"
	$lString &= $pconsGRCActive ? "1" : "0"
	$lString &= $pconsAlcoholActive ? "1" : "0"
	$lString &= $pconsPieActive ? "1" : "0"
	$lString &= $pconsCupcakeActive ? "1" : "0"
	$lString &= $pconsSkaleSoupActive ? "1" : "0"
	$lString &= $pconsAppleActive ? "1" : "0"
	$lString &= $pconsCornActive ? "1" : "0"
	$lString &= $pconsEggActive ? "1" : "0"
	$lString &= $pconsKabobActive ? "1" : "0"
	$lString &= $pconsWarSupplyActive ? "1" : "0"
	$lString &= $pconsLunarsActive ? "1" : "0"
	$lString &= $pconsEssenceActive ? "1" : "0"
	$lString &= $pconsMobstoppersActive ? "1" : "0"
	$lString &= $pconsMoraleActive ? "1" : "0"
	$lString &= $pconsCityActive ? "1" : "0"

	GUISetState(@SW_DISABLE, $mainGui)
	Opt("GUIOnEventMode", False)
	Local $lGui = GUICreate("Save Preset", 160, 150, Default, Default, $WS_POPUP, Default, $mainGui)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_WHITE)
		WinSetTrans($lGui, "", $Transparency)
	GUICtrlCreateLabel("Save Preset...", 0, 0, 160, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	Local $lDefault = "Enter name..."
	If GUICtrlRead($pconsPresetList) <> $pconsPresetDefault Then $lDefault = GUICtrlRead($pconsPresetList)
	Local $lInput = GUICtrlCreateInput($lDefault, 20, 60, 120, 23)
	Local $lCancel = MyGuiCtrlCreateButton("Cancel", 20, 100, 50, 20)
	Local $lOk = MyGuiCtrlCreateButton("Save", 90, 100, 50, 20)

	Local $iESC = GUICtrlCreateDummy()
	Local $AccelKeys[1][2] = [["{ESC}", $iESC]]; Set accelerators
	GUISetAccelerators($AccelKeys)

	GUISetState(@SW_SHOW)

	While 1
		Local $msg = GUIGetMsg()
		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $lCancel, $iESC
				ExitLoop
			Case $lOk, $lInput
				$lName = GUICtrlRead($lInput)
				If $lName <> "" And $lName <> $pconsPresetDefault Then IniWrite($iniFullPath, "pconsPresets", $lName, $lString)
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($lGui)
	GUISetState(@SW_ENABLE, $mainGui)
	WinActivate($mainGui)
	Opt("GUIOnEventMode", True)
	If (($lName <> "") And ($lName <> $pconsPresetDefault) And (_GUICtrlComboBox_FindString($pconsPresetList, $lName)==-1)) Then _GUICtrlComboBox_AddString($pconsPresetList, $lName)
EndFunc

Func presetDelete()
	Local $lName = ""
	GUISetState(@SW_DISABLE, $mainGui)
	Opt("GUIOnEventMode", False)
	Local $lGui = GUICreate("Delete Preset", 160, 150, Default, Default, $WS_POPUP, Default, $mainGui)
		GUISetBkColor($COLOR_BLACK)
		GUICtrlSetDefBkColor($COLOR_BLACK)
		GUICtrlSetDefColor($COLOR_WHITE)
		WinSetTrans($lGui, "", $Transparency)
	GUICtrlCreateLabel("Delete Preset...", 0, 0, 160, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	Local $lCombo = GUICtrlCreateCombo("", 20, 60, 120, 23, $CBS_DROPDOWNLIST)
		GUICtrlSetColor(-1, $COLOR_WHITE)
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		SetPresetCombo($lCombo)
	Local $lCancel = MyGuiCtrlCreateButton("Cancel", 20, 100, 50, 20)
	Local $lDelete = MyGuiCtrlCreateButton("Delete", 90, 100, 50, 20)

	Local $iENTER = GUICtrlCreateDummy()
	Local $iESC = GUICtrlCreateDummy()
	Local $AccelKeys[2][2] = [["{ENTER}", $iENTER], ["{ESC}", $iESC]]; Set accelerators
	GUISetAccelerators($AccelKeys)

	GUISetState(@SW_SHOW)

	While 1
		Local $msg = GUIGetMsg()
		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $lCancel, $iESC
				ExitLoop
			Case $lDelete, $iENTER
				$lName = GUICtrlRead($lCombo)
				If $lName <> $pconsPresetDefault Then
					IniDelete($iniFullPath, "pconsPresets", $lName)
				EndIf
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($lGui)
	GUISetState(@SW_ENABLE, $mainGui)
	WinActivate($mainGui)
	Opt("GUIOnEventMode", True)
	If (($lName <> "") And ($lName <> $pconsPresetDefault)) Then _GUICtrlComboBox_DeleteString($pconsPresetList, _GUICtrlComboBox_FindString($pconsPresetList, $lName))
	GUICtrlSetData($pconsPresetList, $pconsPresetDefault)
EndFunc

Func SetPresetCombo($aGuiCtrlID, $aDefault = $pconsPresetDefault)
	Local $lPresets = IniReadSection($iniFullPath, "pconsPresets")
	Local $lPresetsString = $aDefault
	If Not @error Then
		For $i=1 To $lPresets[0][0]
			$lPresetsString = $lPresetsString&"|"&$lPresets[$i][0]
		Next
	EndIf
	GUICtrlSetData($aGuiCtrlID, $lPresetsString)
	GUICtrlSetData($aGuiCtrlID, $aDefault)
EndFunc
#endregion pcons

Func sfmacroToggleSkill()
	For $i = 0 To 7 Step 1
		If $sfmacroCheckBoxes[$i] == @GUI_CtrlId Then
			$sfmacroSkillsToUse[$i] = Not $sfmacroSkillsToUse[$i]
			IniWrite($iniFullPath, $s_sfmacro, $i + 1, $sfmacroSkillsToUse[$i])
			Return
		EndIf
	Next
EndFunc   ;==>sfmacroToggleSkill

#region fastTravel
Func fastTravel()
	Local $lMapId = 0
	Local $lDistrict
	Local $lDistrictNo = 0
	Local $lRegion[12] = 	[-2, 0, 2, 2, 2, 2, 2, 2,  2, 1, 3, 4]
	Local $lLanguage[12] = 	[0,  0, 0, 2, 3, 4, 5, 9, 10, 0, 0, 0]
	Local $lGuiDistrict = GUICtrlRead($travelDistrict)
	Switch @GUI_CtrlId
		Case $travelDoA
			$lMapId = $MAP_ID_DOA
		Case $travelEmbark
			$lMapId = $MAP_ID_EMBARK
		Case $travelKamadan
			$lMapId = $MAP_ID_KAMADAN
		Case $travelToA
			$lMapId = $MAP_ID_TOA
		Case $travelVlox
			$lMapId = $MAP_ID_VLOX
		Case $travelOther
			#region gui
			GUISetState(@SW_DISABLE, $mainGui)
			Opt("GUIOnEventMode", False)
			Local $fastTravelGui = GUICreate("Fast Travel To...", 300, 150, Default, Default, $WS_POPUP, Default, $mainGui)
			GUISetBkColor($COLOR_BLACK)
			GUICtrlSetDefBkColor($COLOR_BLACK)
			GUICtrlSetDefColor($COLOR_WHITE)
			WinSetTrans($fastTravelGui, "", $Transparency)
			Local $lDestnations = ""
			For $i=1 To $MAP_ID[0]
				If IsString($MAP_ID[$i]) And $MAP_ID[$i] <> "" Then
					$lDestnations = $lDestnations & "|"&$MAP_ID[$i]
				EndIf
			Next
			GUICtrlCreateLabel("Fast Travel To...", 0, 0, 300, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
				GUICtrlSetFont(-1, 14)
			Local $lTravelDestination = GUICtrlCreateCombo("Select Destination", 20, 50, 260, 25, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
				GUICtrlSetData(-1, $lDestnations)
				GUICtrlSetBkColor(-1, $COLOR_BLACK)
			Local $lTravelDistrict = GUICtrlCreateCombo("Current District", 20, 80, 260, 25, $CBS_DROPDOWNLIST)
				GUICtrlSetData(-1, "International|American|American District 1|Europe English|Europe French|Europe German|Europe Italian|Europe Spanish|Europe Polish|Europe Russian|Asian Korean|Asia Chinese|Asia Japanese")
				GUICtrlSetBkColor(-1, $COLOR_BLACK)
				GUICtrlSetData(-1, $lGuiDistrict)
			Local $travelButton = MyGuiCtrlCreateButton("Travel", 20, 110, 110, 25)
			Local $cancelButton = MyGuiCtrlCreateButton("Cancel", 170, 110, 110, 25)

			Local $iENTER = GUICtrlCreateDummy()
			Local $iESC = GUICtrlCreateDummy()
			Local $AccelKeys[2][2] = [["{ENTER}", $iENTER], ["{ESC}", $iESC]]; Set accelerators
			GUISetAccelerators($AccelKeys)

			GUISetState(@SW_SHOW)

			While 1
				Local $msg = GUIGetMsg()
				Switch $msg
					Case 0
						ContinueLoop
					Case $GUI_EVENT_CLOSE, $cancelButton, $iESC
						ExitLoop
					Case $travelButton, $iENTER
						Local $lDestination = GUICtrlRead($lTravelDestination)
						For $i=1 To $MAP_ID[0]
							If $MAP_ID[$i] == $lDestination Then
								$lMapId = $i
								$lGuiDistrict = GUICtrlRead($lTravelDistrict)
								ExitLoop 2
							EndIf
						Next
				EndSwitch
			WEnd

			GUIDelete($fastTravelGui)
			GUISetState(@SW_ENABLE, $mainGui)
			WinActivate($mainGui)
			Opt("GUIOnEventMode", True)
			If $lMapId == 0 Then Return
			#endregion gui
	EndSwitch
	If $lGuiDistrict == "Current District" Then
		If getMapID() == $lMapId Then Return
		MoveMap($lMapId, GetRegion(), 0, GetLanguage())
		Return
	Else
		Switch $lGuiDistrict
			Case "International"
				$lDistrict = 0
			Case "American"
				$lDistrict = 1
			Case "American District 1"
				$lDistrict = 1
				$lDistrictNo = 1
			Case "Europe English"
				$lDistrict = 2
			Case "Europe French"
				$lDistrict = 3
			Case "Europe German"
				$lDistrict = 4
			Case "Europe Italian"
				$lDistrict = 5
			Case "Europe Spanish"
				$lDistrict = 6
			Case "Europe Polish"
				$lDistrict = 7
			Case "Europe Russian"
				$lDistrict = 8
			Case "Asian Korean"
				$lDistrict = 9
			Case "Asia Chinese"
				$lDistrict = 10
			Case "Asia Japanese"
				$lDistrict = 11
		EndSwitch
		If (getMapID()==$lMapId) And (getRegion()==$lRegion[$lDistrict]) And (getLanguage()==$lLanguage[$lDistrict]) Then Return
		MoveMap($lMapId, $lRegion[$lDistrict], $lDistrictNo, $lLanguage[$lDistrict])
	EndIf
EndFunc
#endregion

#region MaterialsBuyer
Func MaterialsBuyerGui()
	GUISetState(@SW_DISABLE, $mainGui)
	Opt("GUIOnEventMode", False)
	Local $matsBuyerGui = GUICreate("Materials Buyer", 300, 340, Default, Default, $WS_POPUP, Default, $mainGui)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)
	WinSetTrans($matsBuyerGui, "", $Transparency)

	GUICtrlCreateLabel("Materials Buyer", 0, 0, 300, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)

	Local $y = 50
	Local $labelX = 25
	Local $inputX = 35
	Local $comboX = 70
	Local $buttonX = 230
	GUICtrlCreateGroup("Manual", 10, $y, 280, 50)
		GUICtrlSetFont(-1, 10)
		GUICtrlCreateLabel("#", $labelX, $y+23)
		Local Const $matsInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
		Local Const $lMatsCombo = GUICtrlCreateCombo("", $comboX, $y+19, 150, 24, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		Global $matsString = ""
		For $i=1 To $MATS_NAME[0]
			$matsString = $matsString & "|" & $MATS_NAME[$i]
		Next
		GUICtrlSetData(-1, $matsString)
		Local Const $matsButton = MyGuiCtrlCreateButton("Buy", $buttonX, $y+20, 50, 20)

	$y+=60
	GUICtrlCreateGroup("Consets", 10, $y, 280, 50)
		GUICtrlSetFont(-1, 10)
		GUICtrlCreateLabel("#", $labelX, $y+23)
		Local Const $consInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
		Local Const $consLabel = GUICtrlCreateLabel("computing... 0/8", $comboX, $y+23, 150, 20, $SS_CENTER)
		Local Const $consButton = MyGuiCtrlCreateButton("Buy mats", $buttonX, $y+20, 50, 20)

	$y+=60
	GUICtrlCreateGroup("Res Scrolls", 10, $y, 280, 50)
		GUICtrlSetFont(-1, 10)
		GUICtrlCreateLabel("#", $labelX, $y+23)
		Local Const $scrollsInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
		Local Const $scrollsLabel = GUICtrlCreateLabel("computing... 0/10", $comboX, $y+23, 150, 20, $SS_CENTER)
		Local Const $scrollsButton = MyGuiCtrlCreateButton("Buy mats", $buttonX, $y+20, 50, 20)

	$y+=60
	GUICtrlCreateGroup("Powerstones", 10, $y, 280, 50)
		GUICtrlSetFont(-1, 10)
		GUICtrlCreateLabel("#", $labelX, $y+23)
		Local Const $pstonesInput = GUICtrlCreateInput("1", $inputX, $y+20, 30, 20, BitOR($ES_NUMBER, $ES_RIGHT))
		Local Const $pstonesLabel = GUICtrlCreateLabel("computing... 0/12", $comboX, $y+23, 150, 20, $SS_CENTER)
		Local Const $pstonesButton = MyGuiCtrlCreateButton("Buy mats", $buttonX, $y+20, 50, 20)

	$y+=70
	Local Const $logLabel = GUICtrlCreateLabel("computing costs...", $labelX, $y+5, 160, 20)
	Local Const $doneButton = MyGuiCtrlCreateButton("Close", 190, $y, 100, 25)

	GUISetState(@SW_SHOW)

	Local $lIronCost, $lDustCost, $lBonesCost, $lFeathersCost, $lFibersCost, $lGraniteCost
	Local $lPriceCheckTimer = TimerInit()
	Local $lPriceCheckCounter = 0
	Local $lLastTraderCostID = 0

	While 1
		Local $msg = GUIGetMsg()
		If GetMapLoading() == $INSTANCETYPE_EXPLORABLE Then ExitLoop
		Switch $msg
			Case $GUI_EVENT_CLOSE, $doneButton
				ExitLoop
			Case $matsButton
				Local $lMatID = getMatIDByName(GUICtrlRead($lMatsCombo))
				Local $lMatQuantity = Int(GUICtrlRead($matsInput))
				If $lMatID == -1 Then
					GUICtrlSetData($logLabel, "Select a material first!")
				ElseIf $lMatQuantity == 0 Then
					GUICtrlSetData($logLabel, "Input a number greater than 0!")
				ElseIf $lMatQuantity < 0 Then
					GUICtrlSetData($logLabel, "Input a positive number!")
				Else
					GUICtrlSetState($matsButton, $GUI_DISABLE)
					GUICtrlSetState($consButton, $GUI_DISABLE)
					GUICtrlSetState($scrollsButton, $GUI_DISABLE)
					GUICtrlSetState($pstonesButton, $GUI_DISABLE)
					GUICtrlSetData($doneButton, "Cancel")
					$lPriceCheckCounter = 13

					BuyMaterialsEx($doneButton, $logLabel, $lMatID, $lMatQuantity)

					GUICtrlSetState($matsButton, $GUI_ENABLE)
					GUICtrlSetState($consButton, $GUI_ENABLE)
					GUICtrlSetState($scrollsButton, $GUI_ENABLE)
					GUICtrlSetState($pstonesButton, $GUI_ENABLE)
					GUICtrlSetData($doneButton, "Close")
				EndIf
			Case $consButton
				Local $lMatQuantity = Int(GUICtrlRead($consInput))
				If $lMatQuantity == 0 Then
					GUICtrlSetData($logLabel, "Input a number greater than 0!")
				ElseIf $lMatQuantity < 0 Then
					GUICtrlSetData($logLabel, "Input a positive number!")
				Else
					GUICtrlSetState($matsButton, $GUI_DISABLE)
					GUICtrlSetState($consButton, $GUI_DISABLE)
					GUICtrlSetState($scrollsButton, $GUI_DISABLE)
					GUICtrlSetState($pstonesButton, $GUI_DISABLE)
					GUICtrlSetData($doneButton, "Cancel")
					$lPriceCheckCounter = 13

					For $i=1 To $lMatQuantity ; for each cons
						If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_IRON, 10) Then ExitLoop
						If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_DUST, 10) Then ExitLoop
						If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_BONES, 5) Then ExitLoop
						If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_FEATHERS, 5) Then ExitLoop
					Next

					GUICtrlSetState($matsButton, $GUI_ENABLE)
					GUICtrlSetState($consButton, $GUI_ENABLE)
					GUICtrlSetState($scrollsButton, $GUI_ENABLE)
					GUICtrlSetState($pstonesButton, $GUI_ENABLE)
					GUICtrlSetData($doneButton, "Close")
				EndIf
			Case $scrollsButton
				Local $lMatQuantity = Int(GUICtrlRead($scrollsInput))
				If $lMatQuantity == 0 Then
					GUICtrlSetData($logLabel, "Input a number greater than 0!")
				ElseIf $lMatQuantity < 0 Then
					GUICtrlSetData($logLabel, "Input a positive number!")
				Else
					GUICtrlSetState($matsButton, $GUI_DISABLE)
					GUICtrlSetState($consButton, $GUI_DISABLE)
					GUICtrlSetState($scrollsButton, $GUI_DISABLE)
					GUICtrlSetState($pstonesButton, $GUI_DISABLE)
					GUICtrlSetData($doneButton, "Cancel")
					$lPriceCheckCounter = 13

					For $i=1 To $lMatQuantity ; for each cons
						If Mod($i, 2) == 1 Then
							If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_FIBERS, 3) Then ExitLoop
							If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_BONES, 3) Then ExitLoop
						Else
							If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_FIBERS, 2) Then ExitLoop
							If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_BONES, 2) Then ExitLoop
						EndIf
					Next

					GUICtrlSetState($matsButton, $GUI_ENABLE)
					GUICtrlSetState($consButton, $GUI_ENABLE)
					GUICtrlSetState($scrollsButton, $GUI_ENABLE)
					GUICtrlSetState($pstonesButton, $GUI_ENABLE)
					GUICtrlSetData($doneButton, "Close")
				EndIf
			Case $pstonesButton
				Local $lMatQuantity = Int(GUICtrlRead($pstonesInput))
				If $lMatQuantity == 0 Then
					GUICtrlSetData($logLabel, "Input a number greater than 0!")
				ElseIf $lMatQuantity < 0 Then
					GUICtrlSetData($logLabel, "Input a positive number!")
				Else
					GUICtrlSetState($matsButton, $GUI_DISABLE)
					GUICtrlSetState($consButton, $GUI_DISABLE)
					GUICtrlSetState($scrollsButton, $GUI_DISABLE)
					GUICtrlSetState($pstonesButton, $GUI_DISABLE)
					GUICtrlSetData($doneButton, "Cancel")
					$lPriceCheckCounter = 13

					For $i=1 To $lMatQuantity ; for each cons
						If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_GRANITE, 10) Then ExitLoop
						If Not BuyMaterialsEx($doneButton, $logLabel, $MODELID_DUST, 10) Then ExitLoop
					Next

					GUICtrlSetState($matsButton, $GUI_ENABLE)
					GUICtrlSetState($consButton, $GUI_ENABLE)
					GUICtrlSetState($scrollsButton, $GUI_ENABLE)
					GUICtrlSetState($pstonesButton, $GUI_ENABLE)
					GUICtrlSetData($doneButton, "Close")
				EndIf
		EndSwitch

		If TimerDiff($lPriceCheckTimer) > 500 Then
			$lPriceCheckCounter += 1
			Switch $lPriceCheckCounter
				Case 1
					TraderRequest($MODELID_IRON)
				Case 2
					$lIronCost = GetTraderCostValue()
				Case 3
					TraderRequest($MODELID_DUST)
				Case 4
					$lDustCost = GetTraderCostValue()
				Case 5
					TraderRequest($MODELID_BONES)
				Case 6
					$lBonesCost = GetTraderCostValue()
				Case 7
					TraderRequest($MODELID_FEATHERS)
				Case 8
					$lFeathersCost = GetTraderCostValue()
					GUICtrlSetData($consLabel, "Cost: " & ($lDustCost*10 + $lIronCost*10 + $lBonesCost*5 + $lFeathersCost*5 + 750) / 1000 & " k")
				Case 9
					TraderRequest($MODELID_FIBERS)
				Case 10
					$lFibersCost = GetTraderCostValue()
					GUICtrlSetData($scrollsLabel, "Cost: " & ($lFibersCost * 2.5 + $lBonesCost * 2.5 + 250) / 1000 & " k")
				Case 11
					TraderRequest($MODELID_GRANITE)
				Case 12
					$lGraniteCost = GetTraderCostValue()
					GUICtrlSetData($pstonesLabel, "Cost: " & ($lGraniteCost * 10 + $lDustCost * 10 + 1000) / 1000 & " k")
					GUICtrlSetData($logLabel, "Waiting on user input")
			EndSwitch
			If $lPriceCheckCounter < 13 And Mod($lPriceCheckCounter, 2) == 0 Then
				Local $lTraderCostValue = GetTraderCostValue()
				Local $lTraderCostID = GetTraderCostID()
				If $lTraderCostID == 0 Or $lTraderCostValue == 0 Or $lTraderCostID == $lLastTraderCostID Then
					GUICtrlSetData($logLabel, "Waiting on user input")
					GUICtrlSetData($consLabel, "Error computing cost")
					GUICtrlSetData($scrollsLabel, "Error computing cost")
					GUICtrlSetData($pstonesLabel, "Error computing cost")
					$lPriceCheckCounter = 12
				Else
					$lLastTraderCostID = $lTraderCostID
				EndIf
			EndIf
			If $lPriceCheckCounter < 8 Then GUICtrlSetData($consLabel, "computing... "&$lPriceCheckCounter&"/8")
			If $lPriceCheckCounter < 10 Then GUICtrlSetData($scrollsLabel, "computing... "&$lPriceCheckCounter&"/10")
			If $lPriceCheckCounter < 12 Then GUICtrlSetData($pstonesLabel, "computing... "&$lPriceCheckCounter&"/12")
			$lPriceCheckTimer = TimerInit()
		EndIf
	WEnd

	GUIDelete($matsBuyerGui)
	GUISetState(@SW_ENABLE, $mainGui)
	WinActivate($mainGui)
	Opt("GUIOnEventMode", True)
EndFunc

;~ Description: Will buy the material $lMatID with number of trades $lMatsQuantity, it will also log into $logLabel, and it will exit if $exitButton is pressed.
;~ Returns: True if successful, False if failed.
Func BuyMaterialsEx($exitButton, $logLabel, $lMatID, $lMatsQuantity)
	Local $lTimer = TimerInit()
	Local $lMatsRequested = False
	Local $lMatsIteration = 0
	GUICtrlSetData($logLabel, "Buying... "&$lMatsIteration&"/"&$lMatsQuantity)
	While 1
		If GUIGetMsg() == $exitButton Then
			GUICtrlSetData($logLabel, "Buying... "&$lMatsIteration&"/"&$lMatsQuantity&" ...Cancelled")
			Return False
		EndIf

		If TimerDiff($lTimer) > 500 Then
			$lTimer = TimerInit()

			If Not $lMatsRequested Then ; if mats are not requested then do it
				Local $lRet = TraderRequest($lMatID)
				If Not $lRet Then ; if error notify user and return
					GUICtrlSetData($logLabel, "Buying... "&$lMatsIteration&"/"&$lMatsQuantity&" ...Error")
					Return False
				Else ; else everything is fine, mats were requested
					$lMatsRequested = True
				EndIf
			Else ; else we can buy
				If GetGoldCharacter() < GetTraderCostValue() Then ; check if we have enough money
					GUICtrlSetData($logLabel, "Buying... "&$lMatsIteration&"/"&$lMatsQuantity&" ...Not enough money!")
					Return False
				Else ; go buy
					TraderBuy()
					$lMatsRequested = False
					$lMatsIteration += 1
					GUICtrlSetData($logLabel, "Buying... "&$lMatsIteration&"/"&$lMatsQuantity)
					If $lMatsIteration >= $lMatsQuantity Then ; check if we are done.
						GUICtrlSetData($logLabel, "Buying... "&$lMatsIteration&"/"&$lMatsQuantity&" ...done")
						Return True
					EndIf
				EndIf
			EndIf
		EndIf
	WEnd
EndFunc

Func getMatIDByName($lMatName)
	For $i=1 To $MATS_NAME[0]
		If $lMatName == $MATS_NAME[$i] Then
			Return $MATS_ID[$i]
		EndIf
	Next
	Return -1
EndFunc
#endregion

#region customDialogs
Func customDialogGui()
	GUISetState(@SW_DISABLE, $mainGui)
	Opt("GUIOnEventMode", False)
	Local $customDialogsGui = GUICreate("Custom Dialogs", 300, 150, Default, Default, $WS_POPUP, Default, $mainGui)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)
	WinSetTrans($customDialogsGui, "", $Transparency)

	GUICtrlCreateLabel("Custom Dialogs", 0, 0, 300, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)

	GUICtrlCreateLabel("Please do not use this if you don't know what you are doing", 10, 50, 300, 20, Default, $GUI_WS_EX_PARENTDRAG)

	Local $lInput = GUICtrlCreateInput("", 20, 70, 170, 25, $ES_RIGHT)
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
		GUICtrlSetFont(-1, 12)

	Local $sendButton = MyGuiCtrlCreateButton("Send", 210, 70, 50, 25)

	Local $doneButton = MyGuiCtrlCreateButton("Close", 170, 110, 110, 25)

	Local $iENTER = GUICtrlCreateDummy()
	Local $iESC = GUICtrlCreateDummy()
	Local $AccelKeys[2][2] = [["{ENTER}", $iENTER], ["{ESC}", $iESC]]; Set accelerators
	GUISetAccelerators($AccelKeys)

	GUISetState(@SW_SHOW)

	While 1
		Local $msg = GUIGetMsg()
		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $doneButton, $iESC
				ExitLoop
			Case $iENTER, $sendButton
				Local $lDialogID = GUICtrlRead($lInput)
				Dialog($lDialogID)
				WriteChat("Sent dialog "&$lDialogID, $GWToolbox)
		EndSwitch
	WEnd

	GUIDelete($customDialogsGui)
	GUISetState(@SW_ENABLE, $mainGui)
	WinActivate($mainGui)
	Opt("GUIOnEventMode", True)
EndFunc
#endregion customDialogs

#region helper_funcs
Func UseItemByModelID($aModelID)
	Local $lItem
	For $lBag=1 To 4
		For $lSlot=1 To $BAG_SLOTS[$lBag]
			$lItem = GetItemBySlot($lBag, $lSlot)
			If DllStructGetData($lItem, "ModelID") == $aModelID Then Return SendPacket(0x8, 0x77, DllStructGetData($lItem, "ID"))
		Next
	Next
	Return False
EndFunc

Func pingSleep($additionalTime=500, $random=100)
	Sleep(GetPing()+$additionalTime+Random(0, $random, 1))
EndFunc

Func getSkillPosition($skillID, $skillBar = 0)
	If $skillBar == 0 Then $skillBar = getSkillBar()
	For $i = 1 To 8 Step 1
		If DllStructGetData($skillBar, "ID" & $i) == $skillID Then
			Return $i
		EndIf
	Next
	Return 0
EndFunc   ;==>getSkillPosition
#endregion helper_funcs

#region class change
Func initiateClassChange()
   Local $lClass = GUICtrlRead($DialogSelectClassToChangeToCombo)
   If $lClass == "Assassin" Then
		ChangeToAssassin()
   ElseIf $lClass == "Dervish" Then
		ChangeToDervish()
   ElseIf $lClass == "Elementalist" Then
		ChangeToElementalist()
   ElseIf $lClass == "Mesmer" Then
		ChangeToMesmer()
   ElseIf $lClass == "Monk" Then
		ChangeToMonk()
   ElseIf $lClass == "Necromancer" Then
		ChangeToNecromancer()
   ElseIf $lClass == "Paragon" Then
		ChangeToParagon()
   ElseIf $lClass == "Ranger" Then
		ChangeToRanger()
   ElseIf $lClass == "Ritualist" Then
		ChangeToRitualist()
   ElseIf $lClass == "Warrior" Then
		ChangeToWarrior()
   EndIf
EndFunc

Func ChangeToWarrior()
	Sleep(GetPing()+500)
	Dialog(0x184)
EndFunc
Func ChangeToRanger()
	Sleep(GetPing()+500)
	Dialog(0x284)
EndFunc
Func ChangeToMonk()
	Sleep(GetPing()+500)
	Dialog(0x384)
EndFunc
Func ChangeToNecromancer()
	Sleep(GetPing()+500)
	Dialog(0x484)
EndFunc
Func ChangeToMesmer()
	Sleep(GetPing()+500)
	Dialog(0x584)
EndFunc
Func ChangeToElementalist()
	Sleep(GetPing()+500)
	Dialog(0x684)
EndFunc
Func ChangeToAssassin()
	Sleep(GetPing()+500)
	Dialog(0x784)
EndFunc
Func ChangeToRitualist()
	Sleep(GetPing()+500)
	Dialog(0x884)
EndFunc
Func ChangeToParagon()
	Sleep(GetPing()+500)
	Dialog(0x984)
EndFunc
Func ChangeToDervish()
	Sleep(GetPing()+500)
	Dialog(0xA84)
EndFunc
#endregion

#region ferry
;~ Kamadan to Consulate Docks (Assistant Hahnna in Consulate)
Func ConsulateDocksFerry()
	Local $AssistantHahnna = "Assistant Hahnna"
	Local $NPCAssistantHahnna = GetAgentByName($AssistantHahnna)
	If GetMapID() = 429 Then
		GoToNpc($NPCAssistantHahnna)
		Sleep(GetPing()+500)
		Dialog(0x85)
	EndIf
EndFunc
;~ Kamadan to Lions Arch (Mhenlo in Colsulate Docks)
Func LionsArchFerry()
	Local $Mhenlo = "Mhenlo"
	Local $NPCMhenlo = GetAgentByName($Mhenlo)
	If GetMapID() = 493 Then
		GoToNpc($NPCMhenlo)
		Sleep(GetPing()+500)
		Dialog(0x89)
	EndIf
EndFunc
;~ Opens the gate to Lion's Arch
Func LionsGateFerry()
	Local $LionguardNeiro = "Lionguard Neiro"
	Local $NPCLionguardNeiro = GetAgentByName($LionguardNeiro)
	If GetMapID() = 415 Then
		MoveTo(242, -1405)
    	GoToNpc($NPCLionguardNeiro)
		Sleep(GetPing()+500)
		Dialog(0x85)
	EndIf
EndFunc
;~ Kamadan to Kaineng (Mhenlo in Colsulate Docks)
Func KainengFerry()
	Local $Mhenlo = "Mhenlo"
	Local $NPCMhenlo = GetAgentByName($Mhenlo)
	If GetMapID() = 493 Then
	    GoToNpc($NPCMhenlo)
		Sleep(GetPing()+500)
		Dialog(0x88)
	EndIf
EndFunc
#endregion

#region GuiEventHandlers
Func tabClickHandler()
	For $i=0 To UBound($tabButtons)-1
		If @GUI_CtrlId = $tabButtons[$i] Then
			changeTab($i)
			ExitLoop
		EndIf
	Next
EndFunc
Func changeTab($lNewActiveTab)
	If $lNewActiveTab < 0 Or $lNewActiveTab >= UBound($tabButtons) Then Return
	Local $lOldActiveTab = _GUICtrlTab_GetCurFocus($hTab)
	GUICtrlSetBkColor($tabButtons[$lOldActiveTab], $COLOR_BLACK)
	GUICtrlSetBkColor($tabButtons[$lNewActiveTab], $COLOR_GREY)
	GUICtrlSendMsg($hTab, $TCM_SETCURFOCUS, $lNewActiveTab, 0)
	If $lNewActiveTab==0 Then GUICtrlSetState($mainlabel, $GUI_FOCUS)
EndFunc

Func guiEventHandler()
	Switch @GUI_CtrlId
		Case $OnTopCheckbox
			WinSetOnTop($mainGui, "", GUICtrlRead($OnTopCheckbox)==$GUI_CHECKED)
			WinSetOnTop($timerGui, "", True)
			WinSetOnTop($healthGui, "", True)
			WinSetOnTop($distanceGui, "", True)
			IniWrite($iniFullPath, "display", "ontop", GUICtrlRead($OnTopCheckbox)==$GUI_CHECKED)
		Case $TransparencySlider
			IniWrite($iniFullPath, "display", "transparency", GUICtrlRead($TransparencySlider))
		Case $dragButton
			Local $pos = WinGetPos($mainGui)
			IniWrite($iniFullPath, "display", "x",$pos[0])
			IniWrite($iniFullPath, "display", "y", $pos[1])
		Case $DummyLeft
			If _GUICtrlTab_GetCurFocus($hTab) > 0 Then
				changeTab(_GUICtrlTab_GetCurFocus($hTab)-1)
			EndIf
		Case $DummyRight
			changeTab(_GUICtrlTab_GetCurFocus($hTab)+1)
		Case $infoTimerColorPicker
			Local $lColor = _GuiColorPicker_GetColor($infoTimerColorPicker)
			IniWrite($iniFullPath, $s_timer, "color", $lColor)
			If $timer Then GUICtrlSetColor($timerGuiLabel, $lColor)
			$COLOR_TIMER = $lColor
			GUICtrlSetBkColor($infoTimerColorPicker, $lColor)
		Case $infoHealthColorPicker1
			Local $lColor = _GuiColorPicker_GetColor($infoHealthColorPicker1)
			IniWrite($iniFullPath, $s_health, "color1", $lColor)
			$COLOR_HEALTH_HIGHT = $lColor
			GUICtrlSetBkColor($infoHealthColorPicker1, $lColor)
		Case $infoHealthColorPicker2
			Local $lColor = _GuiColorPicker_GetColor($infoHealthColorPicker2)
			IniWrite($iniFullPath, $s_health, "color2", $lColor)
			$COLOR_HEALTH_MIDDLE = $lColor
			GUICtrlSetBkColor($infoHealthColorPicker2, $lColor)
		Case $infoHealthColorPicker3
			Local $lColor = _GuiColorPicker_GetColor($infoHealthColorPicker3)
			IniWrite($iniFullPath, $s_health, "color3", $lColor)
			$COLOR_HEALTH_LOW = $lColor
			GUICtrlSetBkColor($infoHealthColorPicker3, $lColor)
		Case $infoDistanceColorPicker
			Local $lColor = _GuiColorPicker_GetColor($infoDistanceColorPicker)
			If $distance Then distanceUpdateColor($lColor)
			IniWrite($iniFullPath, $s_distance, "color3", $lColor)
			$COLOR_DISTANCE = $lColor
			GUICtrlSetBkColor($infoDistanceColorPicker, $lColor)
		Case $infoPartyColorPicker
			Local $lColor = _GUIColorPicker_GetColor($infoPartyColorPicker)
			IniWrite($iniFullPath, $s_party, "color", $lColor)
			$COLOR_PARTY = $lColor
			If $party Then
				For $i=1 To 8
					GUICtrlSetColor($partyLabels[$i], $lColor)
				Next
			EndIf
			GUICtrlSetBkColor($infoPartyColorPicker, $lColor)
		Case $zoomSlider
			ChangeMaxZoom(GUICtrlRead($zoomSlider))
		Case $settingsButton
			ShellExecute($DataFolder)
		Case $websiteButton
			ShellExecute($Host)
		Case Else
			MyGuiMsgBox(0, "error", "not yet implemented"&@CRLF&"Please tell someone about it")
	EndSwitch
EndFunc

Func toggleActive()
	Local $active = (GUICtrlRead(@GUI_CtrlId) == $GUI_CHECKED)
	Switch @GUI_CtrlId
		Case $sfmacroActive
			$sfmacro = $active
			IniWrite($iniFullPath, $s_sfmacro, "active", $active)
		Case $stuckActive
			$stuck = $active
			IniWrite($iniFullPath, $s_stuck, "active", $active)
		Case $recallActive
			$recall = $active
			IniWrite($iniFullPath, $s_recall, "active", $active)
		Case $uaActive
			$ua = $active
			IniWrite($iniFullPath, $s_ua, "active", $active)
		Case $hidegwActive
			$hidegw = $active
			IniWrite($iniFullPath, $s_hidegw, "active", $active)
		Case $clickerActive
			$clicker = $active
			IniWrite($iniFullPath, $s_clicker, "active", $active)
		Case $resignActive
			$resign = $active
			IniWrite($iniFullPath, $s_resign, "active", $active)
		Case $resign2Active
			$teamResign = $active
			IniWrite($iniFullPath, $s_teamresign, "active", $active)
		Case $pconsHotkeyActive
			$pconsHotkey = $active
			IniWrite($iniFullPath, $s_pcons, "hkActive", $active)
		Case $resActive
			$res = $active
			IniWrite($iniFullPath, $s_res, "active", $active)
		Case $ageActive
			$age = $active
			IniWrite($iniFullPath, $s_age, "active", $active)
		Case $agepmActive
			$agepm = $active
			IniWrite($iniFullPath, $s_agepm, "active", $active)
		Case $pstoneActive
			$pstone = $active
			IniWrite($iniFullPath, $s_pstone, "active", $active)
		Case $focusActive
			$focus = $active
			IniWrite($iniFullPath, $s_focus, "active", $active)
		Case $looterActive
			$looter = $active
			IniWrite($iniFullPath, $s_looter, "active", $active)
		Case $identifierActive
			$identifier = $active
			IniWrite($iniFullPath, $s_identifier, "active", $active)
		Case $ghostpopActive
			$ghostpop = $active
			IniWrite($iniFullPath, $s_ghostpop, "active", $active)
		Case $ghosttargetActive
			$ghosttarget = $active
			IniWrite($iniFullPath, $s_ghosttarget, "active", $active)
		Case $gstonepopActive
			$gstonepop = $active
			IniWrite($iniFullPath, $s_gstonepop, "active", $active)
		Case $legiopopActive
			$legiopop = $active
			IniWrite($iniFullPath, $s_legiopop, "active", $active)
		Case $rainbowuseActive
			$rainbowuse = $active
			IniWrite($iniFullPath, $s_rainbowuse, "active", $active)
		Case $DialogHK1Active
			$DialogHK1 = $active
			IniWrite($iniFullPath, "DialogHK"&"1", "active", $active)
		Case $DialogHK2Active
			$DialogHK2 = $active
			IniWrite($iniFullPath, "DialogHK"&"2", "active", $active)
		Case $DialogHK3Active
			$DialogHK3 = $active
			IniWrite($iniFullPath, "DialogHK"&"3", "active", $active)
		Case $DialogHK4Active
			$DialogHK4 = $active
			IniWrite($iniFullPath, "DialogHK"&"4", "active", $active)
		Case $infoBuffsDropCheckbox
			$buffsDrop = $active
			IniWrite($iniFullPath, "buffsdrop", "active", $active)
		Case $pconsToggle
			$pcons = Not $pcons
			IniWrite($iniFullPath, $s_pcons, "active", $active)
			GUICtrlSetData($pconsStatusLabel, $pcons ? "Active" : "Disabled")
			GUICtrlSetColor($pconsStatusLabel, $pcons ? $COLOR_GREEN : $COLOR_RED)
			If ($pcons) Then
				Switch GetMapLoading()
					Case $INSTANCETYPE_EXPLORABLE
						$pconsRetArray = GetHasEffects($pconsEffects)
						$pconsRetArrayVerify = GetHasEffects($pconsEffects)
						For $i=1 To $pconsRetArray[0]
							$pconsRetArray[$i] = _Max($pconsRetArray[$i], $pconsRetArrayVerify[$i])
						Next

					Case $INSTANCETYPE_OUTPOST
						$pconsCityRetArray = GetHasEffects($pconsCityEffects)
						$pconsCityRetArrayVerify = GetHasEffects($pconsCityEffects)
						For $i=1 To $pconsCityRetArray[0]
							$pconsCityRetArray[$i] = _Max($pconsCityRetArray[$i], $pconsCityRetArrayVerify[$i])
						Next
				EndSwitch
			EndIf
		Case Else
			MyGuiMsgBox(0, "toggleActive", "not implemented!")
	EndSwitch
EndFunc   ;==>toggleActive

Func setHotkey()
	GUISetState(@SW_DISABLE, $mainGui)
	Local $hDLL = DllOpen("user32.dll")

	Local $hotkeyGui = GUICreate("", 140, 100, -1, -1, 0x80880000, Default, $mainGui)
	GUISetBkColor(0x00FF00, $hotkeyGui)
	Local $label = GUICtrlCreateLabel("Press any key", 0, 30, 140, 70, $SS_CENTER)
	GUICtrlSetFont($label, 16)
	GUISetState(@SW_SHOW, $hotkeyGui)

	While True
		For $i = 4 To 221 Step 1
			If $i == 7 Then ContinueLoop
			If _IsPressed(Hex($i), $hDLL) Then
				Local $keyDEC = $i
				Local $keyHEX = StringRight(Hex($keyDEC), 2)
				Local $keyString = IniRead($keysIniFullPath, "idToKey", $keyHEX, "not found")

				; update GUI
				If $keyString == "not found" Then
					GUICtrlSetData($label, "Key not found" & @CRLF & "key code=" & $keyHEX)
				Else
					GUICtrlSetData($label, $keyString)
				EndIf

				; wait until the key is released
				While _IsPressed($keyHEX, $hDLL)
					Sleep(10)
				WEnd

				; set stuff
				GUICtrlSetData(@GUI_CtrlId, $keyString)
				Switch @GUI_CtrlId
					Case $stuckInput
						$stuckHotkey = $keyHEX
						IniWrite($iniFullPath, $s_stuck, "hotkey", $keyHEX)
					Case $recallInput
						$recallHotkey = $keyHEX
						IniWrite($iniFullPath, $s_recall, "hotkey", $keyHEX)
					Case $uaInput
						$uaHotkey = $keyHEX
						IniWrite($iniFullPath, $s_ua, "hotkey", $keyHEX)
					Case $hidegwInput
						$hidegwHotkey = $keyHEX
						IniWrite($iniFullPath, $s_hidegw, "hotkey", $keyHEX)
					Case $clickerInput
						$clickerHotkey = $keyHEX
						IniWrite($iniFullPath, $s_clicker, "hotkey", $keyHEX)
					Case $resignInput
						$resignHotkey = $keyHEX
						IniWrite($iniFullPath, $s_resign, "hotkey", $keyHEX)
					Case $resign2Input
						$teamResignHotkey = $keyHEX
						IniWrite($iniFullPath, $s_teamresign, "hotkey", $keyHEX)
					Case $resInput
						$resHotkey = $keyHEX
						IniWrite($iniFullPath, $s_res, "hotkey", $keyHEX)
					Case $ageInput
						$ageHotkey = $keyHEX
						IniWrite($iniFullPath, $s_age, "hotkey", $keyHEX)
					Case $agepmInput
						$agepmHotkey = $keyHEX
						IniWrite($iniFullPath, $s_agepm, "hotkey", $keyHEX)
					Case $pstoneInput
						$pstoneHotkey = $keyHEX
						IniWrite($iniFullPath, $s_pstone, "hotkey", $keyHEX)
					Case $focusInput
						$focusHotkey = $keyHEX
						IniWrite($iniFullPath, $s_focus, "hotkey", $keyHEX)
					Case $looterInput
						$looterHotkey = $keyHEX
						IniWrite($iniFullPath, $s_looter, "hotkey", $keyHEX)
					Case $identifierInput
						$identifierHotkey = $keyHEX
						IniWrite($iniFullPath, $s_identifier, "hotkey", $keyHEX)
					Case $ghostpopInput
						$ghostpopHotkey = $keyHEX
						IniWrite($iniFullPath, $s_ghostpop, "hotkey", $keyHEX)
					Case $ghosttargetInput
						$ghosttargetHotkey = $keyHEX
						IniWrite($iniFullPath, $s_ghosttarget, "hotkey", $keyHEX)
					Case $gstonepopInput
						$gstonepopHotkey = $keyHEX
						IniWrite($iniFullPath, $s_gstonepop, "hotkey", $keyHEX)
					Case $legiopopInput
						$legiopopHotkey = $keyHEX
						IniWrite($iniFullPath, $s_legiopop, "hotkey", $keyHEX)
					Case $rainbowuseInput
						$rainbowuseHotkey = $keyHEX
						IniWrite($iniFullPath, $s_rainbowuse, "hotkey", $keyHEX)
					Case $pconsHotkeyInput
						$pconsHotkeyHotkey = $keyHEX
						IniWrite($iniFullPath, $s_pcons, "hotkey", $keyHEX)
					Case $DialogHK1Input
						$DialogHK1Hotkey = $keyHEX
						IniWrite($iniFullPath, "DialogHK"&"1", "hotkey", $keyHEX)
					Case $DialogHK2Input
						$DialogHK2Hotkey = $keyHEX
						IniWrite($iniFullPath, "DialogHK"&"2", "hotkey", $keyHEX)
					Case $DialogHK3Input
						$DialogHK3Hotkey = $keyHEX
						IniWrite($iniFullPath, "DialogHK"&"3", "hotkey", $keyHEX)
					Case $DialogHK4Input
						$DialogHK4Hotkey = $keyHEX
						IniWrite($iniFullPath, "DialogHK"&"4", "hotkey", $keyHEX)
					Case Else
						MyGuiMsgBox(0, "setHotkey", "not implemented!")
				EndSwitch

				; we dont want to stay in this function forever, do we?
				GUISetState(@SW_ENABLE, $mainGui)
				GUISetState(@SW_HIDE, $hotkeyGui)
				GUIDelete($hotkeyGui)
				DllClose($hDLL)
				Return
			EndIf
		Next
		Sleep(10)
	WEnd
EndFunc   ;==>setHotkey

Func guiDialogsEventHandler()
	Switch @GUI_CtrlId
	    Case $dialogsYes
			Dialog(0x84)
	    Case $dialogsNo
			Dialog(0x85)
	    Case $dialogsFerryConsulate
			ConsulateDocksFerry()
	    Case $dialogsFerryLionsArch
			LionsArchFerry()
	    Case $dialogsFerryLionsGate
			LionsGateFerry()
	    Case $dialogsFerryKaineng
			KainengFerry()
		Case $dialogsTake4H
			AcceptQuest($QUEST_ID_UW_PLAINS)
		Case $dialogsTakeDemonAss
			AcceptQuest($QUEST_ID_UW_MNT)
		Case $dialogsTakeToS
			AcceptQuest($QUEST_ID_FOW_TOS)
		Case $dialogsTakeFury
			QuestReward($QUEST_ID_DOA_FOUNDRY_BREAKOUT)
	EndSwitch
EndFunc

Func setDialogID()
	Local $lDialogString = GUICtrlRead(@GUI_CtrlId)
	Local $lDialogNumber = 0
	For $i=1 To $DIALOGS_NAME[0]
		If $lDialogString == $DIALOGS_NAME[$i] Then
			$lDialogNumber = $i
			ExitLoop
		EndIf
	Next
	Switch @GUI_CtrlId
		Case $DialogHK1Combo
			$DialogHK1Number = $lDialogNumber
			IniWrite($iniFullPath, "DialogHK"&"1", "number", $lDialogNumber)
		Case $DialogHK2Combo
			$DialogHK2Number = $lDialogNumber
			IniWrite($iniFullPath, "DialogHK"&"2", "number", $lDialogNumber)
		Case $DialogHK3Combo
			$DialogHK3Number = $lDialogNumber
			IniWrite($iniFullPath, "DialogHK"&"3", "number", $lDialogNumber)
		Case $DialogHK4Combo
			$DialogHK4Number = $lDialogNumber
			IniWrite($iniFullPath, "DialogHK"&"4", "number", $lDialogNumber)
	EndSwitch
EndFunc
#endregion GuiEventHandlers

#region GuiCreationFunctions
Func MyGuiCtrlCreateButton($sText, $iX, $iY, $iW, $iH, $iColor = 0xFFFFFF, $iBgColor = 0x222222, $iPenSize = 1, $iStyle = -1, $iStyleEx = 0)
	Return GuiCtrlCreateBorderLabel($sText, $iX, $iY, $iW, $iH, $iColor, $iBgColor, $iPenSize, ($iStyle = -1 ? BitOR($SS_CENTER, $SS_CENTERIMAGE) : $iStyle), $iStyleEx)
EndFunc

Func MyGuiMsgBox($iFlag, $sTitle, $sText, $hParent = 0, $iWidth = 300, $iHeight = 150, $bCenteredText = False)
	If $hParent Then GUISetState(@SW_DISABLE, $hParent)
	Opt("GUIOnEventMode", False)
	Local $lGui = GUICreate($sTitle, $iWidth, $iHeight, Default, Default, $WS_POPUP, Default, $hParent)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)
	GUICtrlCreateLabel($sTitle, 0, 0, $iWidth, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	GUICtrlCreateLabel($sText, 20, 50, $iWidth-40, $iHeight-100, ($bCenteredText ? -1: $SS_CENTER), $GUI_WS_EX_PARENTDRAG)
	If IsDeclared($s_Transparency) Then WinSetTrans($lGui, "", $Transparency)

	Local $lOkButton = -1
	Local $lCancelButton = -1
	Local $lYesButton = -1
	Local $lNoButton = -1
	Local $lRet = -1

	Switch $iFlag
		Case 0
			$lOkButton = MyGuiCtrlCreateButton("Ok", $iWidth - 130, $iHeight - 40, 110, 25)
		Case 1
			$lCancelButton = MyGuiCtrlCreateButton("Cancel", $iWidth - 200, $iHeight - 40, 80, 25)
			$lOkButton = MyGuiCtrlCreateButton("Ok", $iWidth - 100, $iHeight - 40, 80, 25)
		Case 4
			$lNoButton = MyGuiCtrlCreateButton("No", $iWidth - 200, $iHeight - 40, 80, 25)
			$lYesButton = MyGuiCtrlCreateButton("Yes", $iWidth - 100, $iHeight - 40, 80, 25)
		Case Else
			Return
	EndSwitch

	GUISetState(@SW_SHOW)

	While True
		Switch GUIGetMsg()
			Case $GUI_EVENT_CLOSE, $lCancelButton
				$lRet = 2
				ExitLoop
			Case $lOkButton
				$lRet = 1
				ExitLoop
			Case $lNoButton
				$lRet = 7
				ExitLoop
			Case $lYesButton
				$lRet = 6
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($lGui)
	if $hParent Then GUISetState(@SW_ENABLE, $hParent)
	if $hParent Then WinActivate($hParent)
	Opt("GUIOnEventMode", True)
	Return $lRet
EndFunc

Func _GUICtrlTab_SetBkColor($hWnd, $hSysTab32, $sBkColor)
    Local $aTabPos = ControlGetPos($hWnd, "", $hSysTab32)
    Local $aTab_Rect = _GUICtrlTab_GetItemRect($hSysTab32, -1)
    GUICtrlCreateLabel("", $aTabPos[0]+2, $aTabPos[1]+$aTab_Rect[3]+4, $aTabPos[2]-6, $aTabPos[3]-$aTab_Rect[3]-7)
    GUICtrlSetBkColor(-1, $sBkColor)
    GUICtrlSetState(-1, $GUI_DISABLE)
EndFunc

Func GuiCtrlCreateBorderLabel($sText, $iX, $iY, $iW, $iH, $iColor, $iBgColor, $iPenSize = 1, $iStyle = -1, $iStyleEx = 0)
    GUICtrlCreateLabel("", $iX - $iPenSize, $iY - $iPenSize, $iW + 2 * $iPenSize, $iH + 2 * $iPenSize, 0)
	GUICtrlSetState(-1, $GUI_DISABLE)
    GUICtrlSetBkColor(-1, $iColor)
    Local $nID = GUICtrlCreateLabel($sText, $iX, $iY, $iW, $iH, $iStyle, $iStyleEx)
	GUICtrlSetBkColor(-1, $iBgColor)
    Return $nID
EndFunc   ;==>CreateBorderLabel

Func _GuiRoundCorners($h_win, $iSize)
	Local $XS_pos, $XS_ret
	$XS_pos = WinGetPos($h_win)
	$XS_ret = DllCall("gdi32.dll", "long", "CreateRoundRectRgn", "long", 0, "long", 0, "long", $XS_pos[2]+1, "long", $XS_pos[3]+1, "long", $iSize, "long", $iSize)
	If $XS_ret[0] Then
		DllCall("user32.dll", "long", "SetWindowRgn", "hwnd", $h_win, "long", $XS_ret[0], "int", 1)
	EndIf
EndFunc   ;==>_GuiRoundCorners

Func GuiCtrlCreateRect($x, $y, $width, $height, $color = $COLOR_WHITE)
	GUICtrlCreateLabel("", $x, $y, $width, $height)
	GUICtrlSetBkColor(-1, $color)
	GUICtrlSetState(-1, $GUI_DISABLE)
EndFunc

Func GuiCtrlUpdateData($hCtrl, $data)
	If GUICtrlRead($hCtrl) <> $data Then GUICtrlSetData($hCtrl, $data)
EndFunc
#endregion GuiCreationFunctions

#region initialization
Func MyInitialize()
	Local $lGwProcList = ProcessList("gw.exe")
	Local $lToolboxProcList = ProcessList($GWToolbox&".exe")
	Local $lInitParameter

	; exit conditions:
	If $lGwProcList[0][0] == 0 Then
		MyGuiMsgBox(0, $GWToolbox, "Error: Guild Wars is not running")
		exitProgram()
	EndIf
	If $lGwProcList[0][0] == 1 And $lToolboxProcList[0][0] == 2 Then
		MyGuiMsgBox(0, $GWToolbox, "Error: GWToolbox is already running")
		exitProgram()
	EndIf

	Local $lActiveChars = ScanGW()

	Switch $lActiveChars[0]
		Case 0
			MyGuiMsgBox(0, $GWToolbox, "Error: You are not logged in any gw character. Please log in with a character")
			exitProgram()
		Case 1
			$lInitParameter = $lActiveChars[1]
		Case Else
			$lInitParameter = selectClient($lActiveChars)
	EndSwitch

	Local $lInitRet = Initialize($lInitParameter, False, False, False)

	If $lInitRet == 0 Then
		MyGuiMsgBox(0, $GWToolbox, "Error: Unable to attach GWToolbox to Guild Wars")
		exitProgram()
	EndIf
EndFunc

Func selectClient($chars)
	Opt("GUIOnEventMode", False)
	Local $clientSelectionGui = GUICreate("GWToolbox", 300, 150, Default, Default, $WS_POPUP)
	GUISetBkColor($COLOR_BLACK)
	GUICtrlSetDefBkColor($COLOR_BLACK)
	GUICtrlSetDefColor($COLOR_WHITE)

	Local $lCharString = ""
	For $i=1 To $chars[0]
		$lCharString = $lCharString & "|" & $chars[$i]
	Next

	GUICtrlCreateLabel("Select Client...", 0, 0, 300, 50, BitOR($SS_CENTER, $SS_CENTERIMAGE), $GUI_WS_EX_PARENTDRAG)
		GUICtrlSetFont(-1, 14)
	Local $lCombo = GUICtrlCreateCombo("Select GW Client", 20, 50, 260, 25, BitOR($CBS_DROPDOWNLIST, $WS_VSCROLL, $CBS_SORT))
		GUICtrlSetData(-1, $lCharString)
		GUICtrlSetBkColor(-1, $COLOR_BLACK)
	Local $okButton = MyGuiCtrlCreateButton("Ok", 20, 110, 110, 25)
	Local $cancelButton = MyGuiCtrlCreateButton("Cancel", 170, 110, 110, 25)

	GUISetState(@SW_SHOW)

	Local $lSelectedChar = ""

	While 1
		Local $msg = GUIGetMsg()
		Switch $msg
			Case 0
				ContinueLoop
			Case $GUI_EVENT_CLOSE, $cancelButton
				Exit
			Case $okButton
				$lSelectedChar = GUICtrlRead($lCombo)
				ExitLoop
		EndSwitch
	WEnd

	GUIDelete($clientSelectionGui)
	Opt("GUIOnEventMode", True)
	Return $lSelectedChar
EndFunc
#endregion initialization

Func ArrayGetCommonElements($aArrA, $aArrB)
	Local $lReturnArray[1] = [0]
	For $i=1 To $aArrA[0]
		For $j=1 To $aArrB[0]
			If $aArrA[$i] == $aArrB[$j] Then
				$lReturnArray[0] += 1
				ReDim $lReturnArray[$lReturnArray[0] + 1]
				$lReturnArray[$lReturnArray[0]] = $aArrA[$i]
			EndIf
		Next
	Next
	Return $lReturnArray
EndFunc


Func minimizeProgram()
	GUISetState(@SW_MINIMIZE, $mainGui)
EndFunc

; Actually I don't care about the parameter, it's just a hack to allow Exit(somefunc())
; I know, this is horrible, I dont give an exit code and I don't close stuff. But windows doesn't care about the exit message, and AutoIt closes automatically all open resources... in theory.
Func exitProgram($param = 0)
	Exit 0
EndFunc

mainLoop()
