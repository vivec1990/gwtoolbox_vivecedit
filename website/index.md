# Guild Wars Toolbox
A set of tools for Guild Wars speed-clearers

_by Has_

---------------
### Download
[GWToolbox.exe](http://fbgmguild.com/GWToolbox/GWToolbox.exe)
Current version is 2.14

### Features
* Fast travel to **any** outpost
* Check price for consets, res scrolls and powerstones, automatically buy materials for any amount of those
* Ability to set **hotkeys** for typing /stuck and /resign, using or cancelling recall and UA, using res scrolls and powerstones and more!
* Automatically **maintains** selected pcons
* Send team builds into chat
* Take or accept quests quickly with hotkeys, and be able to do so while NPCs are in combat
* Show instance timer, target health or distance, maintained E/Mo bonds

### Source code
GWToolbox is completely open source and the repository is available [here](https://bitbucket.org/ggori/gwtoolbox) on BitBucket

Feel free to download or fork the repository and play with it. You can message me on BitBucket or make a pull request if you make something cool!

### FAQs

__Will I get banned for using GWToolbox?__
I can't say. GWToolbox is not a bot as it it does not play the game for you, however it does use the same technology that bots use. What can I say, though, is that as far as I know noone has ever been banned for it since it was created 2 years ago.

__It's not showing on top of Guild Wars!__
If you are running windowed, make sure you tick "Always on top". Sometimes you may have to switch focus a couple of times to refresh it and then it will stay on top.

If you are running full-screen, then no, it won't show.

__It's generally not working, can you help?__

* Run **both** Guild Wars and GWToolbox as administrator.
* Add GWToolbox.exe to your antivirus whitelist
* In Windows 8 (and above?) add GWToolbox.exe to the operating system whitelist
* Make sure the Guild Wars client process is called "gw.exe" (non case sensitive)

__It crashed my game!__
Again, running both Guild Wars and GWToolbox as administrator helps with crashing issues.

In some Eye of the North dungeons, GWToolbox **will** crash the game if you zone with pcons usage active. This is a known issue I was not able to solve, the simple workaround is to disable pcons before zoning and re-enable them after.

### Detailed features list and description
GWToolbox uses a tabbed interface. You can use the top buttons to open the various tabs.

The three top-right buttons are use to `-`minimize, `o`move or `x`close GWToolbox. 
#### General
* **Fast Travel** - Use the drop-down list to select the district, or leave "Current District", then click one of the 5 fast access town buttons. Alternatively you can click "Other" and select any town in the game. Note that while selecting the list you can type the outpost name to choose it quickly.
* **Skills Clicker** - Simple "dumb" clicker. It will use the selected skills on recharge. If the E/Mo mode check-box is ticked then it will use Ether Renewal, Spirit Bond, Burning speed on recharge. E/Mo mode is good for going afk as emo, but not for actual play.
* **Display** - You can choose to display GWToolbox on top of (windowed) Guild Wars and select the transparency.
* **Materials Buyer** - Price check and buy materials for cons, res scrolls, powerstones. Make sure to read the help dialog.
* **Max zoom** - Increase the maximum zoom. Mostly for fun but can be useful to check hos-jumps. Note that mobs are shown on screen if they are on radar distance from you, regardless of camera position.

#### Hotkeys
All hotkeys have an "Hotkey" button that will allow you to select which key to use, and a "Active" checkbox that will allow you to enable or disable the hotkey.

* **/stuck** types `/stuck` in chat
* **recall** cancels recall if it's on, otherwise casts on current target
* **UA** cancels UA if it's on, otherwise casts it
* **Hide GW** completely hides Guild Wars window and creates a GWToolbox system tray icon to restore it. Angry parent aggro anyone?
* **/resign** types `/resign` in chat
* **[/resign;xx]** types `[/resign;xx]` in chat
* **Clicker** toggles on/off a clicker that will spam left mouse click
* **Res Scrolls** uses a res scroll
* **/age** types `/age` in chat
* **Age pm** will send you a pm with the current instance time. Useful for full-screen users
* **Pstones** will use a powerstone
* **Focus** will switch focus between the Guild Wars Client and GWToolbox. Useful for non-full screen users who keep GWToolbox not on top (small monitors).
* **Target Boo** will target the current *Boo*, the ghost created by Ghost-in-the-Box
* **Pop Ghost** will use a Ghost-in-the-Box
* **Looter** will pickup nearby items. Mostly useful to pick up chest drops.
* **Identifier** will identify all items in inventory
* **Pop Ghastly** will use a Ghastly Summoning Stone
* **Pop Legion** will use a Legionnaire Summoning Crystal
* **Use Rainbow** will use red, blue, green rock candies. It will not use one if it is already active.

#### Cons
Tick the desired consumables to use, then click "Toggle Active". When status is "Enabled", GWToolbox will maintain those consumables in explorable areas.

You can alternatively set up a hotkey instead of pressing the "Toggle Active" button. 

You can save or load consumable presets.

Note that GWToolbox will scan your inventory for pcons everytime you zone into a new map. If you wish to update the scan, for example after taking consumable from the Xunlai chest, simply click "Update".

#### Builds
You can send into Team Chat your saved team builds.

First of all, press Edit in any row, and write or paste your team build, then press "Send" or use the hotkey to send it to chat.

#### Dialogs
When you use any of those dialogs in this tab, you need to target the appropriate npc, then use the button or hotkey.

You can use the four buttons to take those four quests, otherwise you can set up your own hotkeys for most quests.

With UW Teleport you can teleport your team to any area, even if its reaper is not up yet. Teleport works like normal teleport, meaning party-wide before dhuum spawns, and solo teleport after.

Custom Dialog allows you to use any dialog ID. Do not use if not confident with those.

#### Info
* **Timer** shows the current instance timer. In Urgoz it will also show door timing.
* **Target Health** will show target health both as percentage and absolute value. You can only see the absolute value if you directly dealt damage or healed your target.
* **Party Danger** will show the number of enemies targeting each party member. *Note that this feature requires toolbox to always scan the whole radar and thus it may use a lot of CPU power*
* **Target Distance** will show the distance from the target in percentage of in-game standard distances
* **E/Mo Bonds Monitor** will show which bond is being active on each party member. You will have to align the window with your party. Also note that this feature ignores heroes and henchmen.

#### Settings
GWToolbox stores all your current settings into the `GWToolbox.ini` file that you can find by pressing the "Open Settings Folder" button. When upgrading your pc or reformatting, you can backup and then restore this file to keep your settings.